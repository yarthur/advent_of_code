import { defineConfig } from "vitest/config";

export default defineConfig({
    test: {
        coverage: {
            cleanOnRerun: false,
            enabled: true,
            provider: 'istanbul',
            reportOnFailure: true,
            reporter: [ 'html', 'cobertura', 'text', 'text-summary' ],
            skipFull: true,
        },
    },
});
