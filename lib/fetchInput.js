import 'dotenv/config';
import { dirname } from "node:path";
import { fileURLToPath } from "node:url";
import { readFile, writeFile } from "node:fs/promises";

const __dirname = dirname(fileURLToPath(import.meta.url));
const projectRoot = `${__dirname}/..`;

const TOO_EARLY_REQUEST_TEXT =
  "please don't repeatedly request this endpoint before it unlocks";
const UNAUTHENTICATED_INPUT_TEXT = 'please log in to get your puzzle input';
const INTERNAL_SERVER_ERROR_TEXT = 'internal server error';
const HTML_RESPONSE_TEXT = '!doctype html';

/**
 * @function fetchInput
 * @summary Attempts to fetch the input for a given puzzle from the service.
 * @param { string } year - The year that the puzzle comes from.
 * @param { string } day - The day of the puzzle.
 * @returns { string } The puzzle input.
 */
export default async (year, day) => {
    if (process.env.AOC_SESSION_KEY === undefined) {
        throw new Error("$AOC_SESSION_KEY Environment Variable not defined.");
    }

    const uri = `https://adventofcode.com/${year}/day/${parseInt(day, 10)}/input`;
    const pkgStr = await readFile(`${projectRoot}/package.json`, "utf8");
    const pkg = JSON.parse(pkgStr);
    const USER_AGENT = `node/${process.version} yarthur/${pkg.name}/${pkg.version}`;

    const options = {
        method: 'get',
        headers: {
            'Content-Type': 'text/plain',
            Cookie: `session=${process.env.AOC_SESSION_KEY}`,
            'User-Agent': USER_AGENT,
        },
    };

    const fetchResponse = await (await fetch(uri, options)).text();

  if (fetchResponse.toLowerCase().includes(UNAUTHENTICATED_INPUT_TEXT)) {
    return Promise.reject(
      new Error(
        'You must log in to get your puzzle input, please provide a valid token'
      )
    );
  }

  if (fetchResponse.toLowerCase().includes(TOO_EARLY_REQUEST_TEXT)) {
    return Promise.reject(
      new Error(
        'This puzzle has not opened yet, please wait until the puzzle unlocks!'
      )
    );
  }

  if (fetchResponse.toLowerCase().includes(INTERNAL_SERVER_ERROR_TEXT)) {
    return Promise.reject(
      new Error(
        'An unexpected error occurred while fetching the input, internal server error.'
      )
    );
  }

  if (fetchResponse.toLowerCase().includes(HTML_RESPONSE_TEXT)) {
    return Promise.reject(
      new Error(
        'An error occurred while fetching the input. Are you authenticated correctly?'
      )
    );
  }

    await writeFile(
        `${projectRoot}/inputs/${year}-${day}.txt`,
        fetchResponse.trim()
    );

    return fetchResponse.trim();
};

