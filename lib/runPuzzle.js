/**
 * @typedef {Array} Puzzle
 * @property {string} Puzzle[0]
 * @property {string} Puzzle[1]
 * @property {Function} Puzzle[2]
 * @property {Function} Puzzle[3]
 * @property {string} Puzzle[4]
 */

import { promises as fsPromises } from "fs";
import getInput from "./getInput.js";

/**
 * @type {Function}
 */
const readdir = fsPromises.readdir;

/**
 * TODO: when jest works a bit better, this would allow for a more stable implementation.
 * Instead of assuming the script is being triggered at project root, it would allow us to account for the location of the script itself (`__dirname` in CJS).
 *
 * For now, only "execute" this script from the project root.
 */
// import { dirname } from "path";
// import { fileURLToPath } from "url";

// const __dirname = dirname(fileURLToPath(import.meta.url));
// const projectRoot = `${__dirname}/..`;

/**
 * @type {string}
 */
const projectRoot = process.cwd();

/**
 * @param {string=} year
 * @param {string=} day
 * @param allowFetch
 */
const fetchPuzzles = async (year, day, allowFetch = false) => {
    const solutionsPath = `${projectRoot}/solutions`;

    if (year === undefined) {
        /**
         * @type {object[]}
         */
        const years = await readdir(solutionsPath, {
            withFileTypes: true,
        });

        return years.reduce(
            /**
             * @param {Promise<Puzzle[]>} puzzles
             * @param {object} dirEnt
             * @returns {Promise<Puzzle[]>}
             */
            async (puzzles, dirEnt) => {
                let newPuzzles = [];

                if (dirEnt.isDirectory() === true && isNaN(parseInt(dirEnt.name, 10)) === false) {
                    newPuzzles = await fetchPuzzles(dirEnt.name);
                }

                return [...(await puzzles), ...newPuzzles];
            },
            []
        );
    }

    if (day === undefined) {
        /**
         * @type {object[]}
         */
        const days = await readdir(`${solutionsPath}/${year}`, {
            withFileTypes: true,
        });

        return await days.reduce(
            /**
             * @param {Promise<Puzzle[]>} puzzles
             * @param {object} dirEnt
             * @returns {Promise<Puzzle[]>}
             */
            async (puzzles, dirEnt) => {
                if (dirEnt.isDirectory() === true) {
                    (await puzzles).push(
                        ...(await fetchPuzzles(year, dirEnt.name))
                    );
                }

                return puzzles;
                // return [...(await puzzles), ...newPuzzles];
            },
            []
        );
    }

    try {
        /**
         * @param {object} puzzleParts
         * @param {Function} puzzleParts.part1
         * @param {Function} puzzleParts.part2
         */
        const { part1, part2 } = await import(
            `${solutionsPath}/${year}/${day}/index.js`
        );

        /**
         * @type {string}
         */
        const input = await getInput([year, day], allowFetch);

        return [[year, day, part1, part2, input]];
    }
    catch (e) {
        console.info(e);
        return [];
    }
};

export default async (year, day) => {
    await fetchPuzzles(year, day, true).then((puzzles) => {
        puzzles.forEach(([year, day, part1, part2, input]) => {
            if (input) {
                if (part1 !== undefined) {
                    console.log(`${year} day ${day}, part1: ${part1(input)}`);
                }

                if (part2 !== undefined && part2(input) !== false) {
                    console.log(`${year} day ${day}, part2: ${part2(input)}`);
                }
            }
        });
    });
};
