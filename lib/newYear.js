import scaffoldDirectory from "./scaffoldDirectory.js";

export default async (year) => {
    const newYearDirectory = `${process.cwd()}/solutions/${year}`;

    await scaffoldDirectory("year", newYearDirectory, { year });
};
