import newYear from "./newYear.js";
import scaffoldDirectory from "./scaffoldDirectory.js";
import fetchInput from "./fetchInput.js";

export default async (year, day, summary) => {
    try {
        await newYear(year);
    } catch (e) {
        console.info(e);
    }

    const newDayDirectory = `${process.cwd()}/solutions/${year}/${day}`;

    await scaffoldDirectory("day", newDayDirectory, { year, day, summary });
    await fetchInput(year, day);
};
