import { describe, afterEach, vi, it, expect } from "vitest";
import getInput from "../getInput.js";

const expected = "This is fake input.";

describe("getInputs", async () => {
    afterEach(() => {
        vi.restoreAllMocks();
    })

    it("returns the contents of the input file as a text string", async () => {
        vi.mock("fs/promises", async() => {
            return {
                readFile: async () => {
                    return expected;
                },
            };
        });

        expect(await getInput(["lib", "__tests__"])).toEqual(expected);
    });
});
