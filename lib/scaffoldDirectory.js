import { readdir, mkdir, writeFile, copyFile } from "node:fs/promises";

/**
 * @type {string}
 */
const templateRootDirectory = `${process.cwd()}/templates`;

/**
 * @type { string }
 */
const templateKey = ".template.js";

/**
 * @function scaffoldDirectory
 * @summary Writes boilerplate/scaffold files to a specified directory.
 * @param { string } templateDirectory - The directory in which the scaffold templates exist.
 * @param { string } destination - The directory being scaffolded.
 * @param { object } templateParams - A collection of parameters to be passed to the templates.
 * @returns { Promise<void> }
 */
const scaffoldDirectory = async (templateDirectory, destination, templateParams) => {
    await mkdir(destination).then(async () => {

        const templatePath = `${templateRootDirectory}/${templateDirectory}`;

        const templateFiles = await readdir(
            `${templateRootDirectory}/${templateDirectory}`,
            { withFileTypes: true }
        );

        templateFiles.forEach(async (dirEnt) => {
            const fileName = dirEnt.name;

            if (dirEnt.isDirectory() === true) {
                let newPath = `${destination}/${fileName}`;


                return await scaffoldDirectory(`${templateDirectory}/${fileName}`, newPath, templateParams);
            }

            if (fileName.includes(templateKey) === true) {
                const content = await import(`${templatePath}/${fileName}`).then(
                    ({ default: template }) => {
                        return template(templateParams);
                    }
                );

                return await writeFile(
                    `${destination}/${fileName.split(templateKey)[0]}`,
                    content
                );
            }

            return await copyFile(
                `${templatePath}/${fileName}`,
                `${destination}/${fileName}`
            );
        });
    }).catch(() => {
        console.log(`${destination} already exists.`);
    });
};

export default scaffoldDirectory;
