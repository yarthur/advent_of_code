import 'dotenv/config';
import { dirname } from "node:path";
import { fileURLToPath } from "node:url";
import { readFile } from "node:fs/promises";
import fetchInput from "./fetchInput.js";

const __dirname = dirname(fileURLToPath(import.meta.url));
const projectRoot = `${__dirname}/..`;

/**
 * @function getInput
 * @summary Fetches the Input for a given puzzle.
 * @param {string[]} path - The Path to the puzzle.
 * @param { boolean } [allowFetch = true] - A flag indicating whether or not we want to try and fetch the input from the AOC server.
 * @returns {Promise<string>} - The contents of the puzzle's input file.
 */
export default async (path, allowFetch = true) => {
    const filePath = `${projectRoot}/inputs/${path.join("-")}.txt`;
    try {
        const fileContents = await readFile(filePath, "utf8")

        return fileContents.trim();
    } catch {
        console.info(`${filePath} does not exist.`);
        if (allowFetch) {
            console.log("Need to try to fetch the input.");
            return await fetchInput(...path);
        }

        console.info("Fetching not enabled. Returning `null`.")
        return null
    }
};
