import runPuzzle from "../lib/runPuzzle.js";

const [year, day] = process.argv.slice(2);

await runPuzzle(year, day);
