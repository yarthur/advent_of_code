import newDay from "../lib/newDay.js";

const [year, day, summary] = process.argv.slice(2);

await newDay(year, day, summary);
