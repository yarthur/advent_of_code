import newYear from "../lib/newYear.js";

const [year] = process.argv.slice(2);

await newYear(year);
