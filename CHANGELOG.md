# advent-of-code

## 2.0.0

### Major Changes

- 544695b: Removes inputs from the repository -- they're now cached in
  `./inputs`, but not checked in.

  - When I first built this out, I didn't realize the terms requested the inputs
    not be checked into other public repos. This change corrects that oversight.
  - Updating the project should "just work", though you'll likely not have any
    inputs for the puzzle solutions to run.
  - I've also written a script to fetch the puzzle inputs, which should run when
    running the `newDay` script and/or when running a single Puzzle.

- cb3caa1: Removes individual day's README files

  - When I first built this out, I didn't realize the terms requested the puzzle
    descriptions not be checked into other public repos. This change corrects
    that oversight.
  - I'm marking this a "major"/"breaking" change, but everything should work as
    expected with regards to this particular change (see the change re: inputs).

## 1.5.0

### Minor Changes

- Solve for [2023, day 4](https://adventofcode.com/2023/day/4).

## 1.4.0

### Minor Changes

- 7679b5d: Solves for [2023, day 3](https://adventofcode.com/2023/day/3).

## 1.3.0

### Minor Changes

- e1d0173: Solve for [2023, day2](https://adventofcode.com/2023/day/2).

## 1.2.0

### Minor Changes

- Solved [2023, day 1](https://adventofcode.com/2023/day/1).

## 1.1.0

### Minor Changes

- Adding Changeset!

### Patch Changes

- Cleaning up some files we aren't really using.
- Tweaking the templates.
