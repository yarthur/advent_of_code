/**
 * @namespace CommonFunctions
 * @summary A collection of common functions.
 */

import * as math from "./math.js";

export {
    math,
};
