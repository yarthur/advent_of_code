/**
 * @namespace Math
 * @summary A collection of common Math-related functions.
 * @memberof CommonFunctions
 */

/**
 * @function add
 * @summary Returns the Sum of two Numbers.
 * @memberof CommonFunctions.Math
 * @param { number } a - The first of two numbers.
 * @param { number } b - The second of two numbers.
 * @returns { number } The Sum of the two numbers.
 */
export const add = (a, b) => {
    return a + b;
};

/**
 * @function multiply
 * @summary Returns the product of two Numbers.
 * @memberof CommonFunctions.Math
 * @param { number } a - The first of two numbers.
 * @param { number } b - The second of two numbers.
 * @returns { number } The Product of the two numbers.
 */
export const multiply = (a, b) => {
    return a * b;
};

/**
 * @function greatestCommonDenominator
 * @summary Returns the Greatest Common Denominator of two numbers.
 * @memberof CommonFunctions.Math
 * @param { number } a - The first of two numbers.
 * @param { number } b - The second of two numbers.
 * @returns { number } The Greatest Common Denominator of the two numbers.
 */
export const greatestCommonDenominator = (a, b) => {
    return a ? greatestCommonDenominator(b % a, a) : b;
}

/**
 * @function leastCommonMultiple
 * @summary Returns the Least Common Multiple of two numbers.
 * @memberof CommonFunctions.Math
 * @param { number } a - The first of two numbers.
 * @param { number } b - The second of two numbers.
 * @returns { number } The Least Common Multiple of the two numbers.
 */
export const leastCommonMultiple = (a, b) => {
    return a * b / greatestCommonDenominator(a, b);
};

