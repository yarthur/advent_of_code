import { describe, it, expect } from "vitest";
import * as math from "../math.js";

describe("commonFunctions.math", () => {
    describe("add", () => {
        it("returns the Sum of two numbers", () => {
            expect(math.add(5, 7)).toBe(12);
            expect(math.add(6, 3)).toBe(9);
            expect(math.add(3, 6)).toBe(9);
        })
    });

    describe("multiply", () => {
        it("returns the product of two numbers", () => {
            expect(math.multiply(5, 7)).toBe(35);
            expect(math.multiply(6, 3)).toBe(18);
            expect(math.multiply(3, 6)).toBe(18);
        })
    });

    describe("greatestCommonDenominator", () => {
        it("derives the greatest common denominator of two numbers", () => {
            expect(math.greatestCommonDenominator(5, 7)).toBe(1);
            expect(math.greatestCommonDenominator(6, 3)).toBe(3);
            expect(math.greatestCommonDenominator(3, 6)).toBe(3);
        });
    });

    describe("leastCommonMultiple", () => {
        it("returns the least common multiple of two numbers", () => {
            expect(math.leastCommonMultiple(5, 7)).toBe(35);
            expect(math.leastCommonMultiple(6, 3)).toBe(6);
            expect(math.leastCommonMultiple(3, 6)).toBe(6);
        });
    });
});
