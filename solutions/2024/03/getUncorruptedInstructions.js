/**
 * @function getUncorruptedInstructions
 * @summary Pulls uncorrupted Instructions from the Input String.
 * @memberof 2024.03
 * @param { string } input - The puzzle's Input String.
 * @param { boolean } withConditionals = false - Flag to enable parsing of conditionals in the string.
 * @returns { Array< Array< number, number > > } A collection of uncorrupted Instructions.
 */
export default (input, withConditionals = false) => {
    const uncorruptedPattern = /(mul\((\d+),(\d+)\)|(do|don't)\(\))/g;

    let dont = false;

    return [...input.matchAll(uncorruptedPattern)]
        .reduce((instructions, match) => {
            if (match[0].startsWith("do")) {
                if (withConditionals) {
                    dont = match[4] === "don't";
                }

                return instructions;
            }

            // Don't
            if (withConditionals && dont) {
                return instructions;
            }

            return [
                ...instructions,
                [
                    parseInt(match[2], 10),
                    parseInt(match[3], 10),
                ],
            ];
        }, []);
};
