import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input, inputWithConditionals } from "./testInput.js";

describe("2024, day 3", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(161);
    });

    it("solves for part 2", () => {
        expect(part2(inputWithConditionals)).toBe(48);
    });
});
