import { describe, it, expect } from "vitest";
import getUncorruptedInstructions from "../getUncorruptedInstructions.js";
import { input, inputWithConditionals, instructions, instructionsWithConditionals } from "./testInput.js";

describe("getUncorruptedInstructions", () => {
    it("pulls uncorrupted instructions from the input string", () => {
        expect(getUncorruptedInstructions(input)).toStrictEqual(instructions);
    });

    it("parses conditionals before pulling uncorrupted instructions from the input string", () => {
        expect(getUncorruptedInstructions(inputWithConditionals, true))
            .toStrictEqual(instructionsWithConditionals);
    });
})
