/**
 * My solutions for the
 * [2024 edition of Advent of Code, day 3](https://adventofcode.com/2024/day/3/).
 * @namespace 03
 * @summary Mull It Over
 * @memberof 2024
 */

import getUncorruptedInstructions from "./getUncorruptedInstructions.js";
import { add, multiply } from "../../common-functions/math.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2024.03
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all uncorrupted multiples.
 */
export const part1 = (input) => {
    const instructions = getUncorruptedInstructions(input);
    return instructions.map((instruction) => {
        return instruction.reduce(multiply);
    }).reduce(add, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2024.03
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all _real_ and uncorrupted multiples.
 */
export const part2 = (input) => {
    const instructions = getUncorruptedInstructions(input, true);

    return instructions.map((instruction) => {
        return instruction.reduce(multiply);
    }).reduce(add, 0);
};
