/**
 * @function getLists
 * @summary Parses the Input String into two lists.
 * @memberof 2023.01
 * @param { string } input - The Input String.
 * @returns { Array< Array< number >, Array< number > >} The two lists.
 */
export default (input) => {
    return input.split("\n")
        .reduce(([firstList, secondList], row) => {
            const[one, two] = row.split(/\s+/);

            firstList.push(parseInt(one, 10));
            secondList.push(parseInt(two, 10));

            return [firstList, secondList];
        }, [[], []]);
}
