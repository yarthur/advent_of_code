import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input } from "./testInput.js";

describe("2024, day 1", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(11);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toBe(31);
    });
});
