import { describe, it, expect } from "vitest";
import getLists from "../getLists.js";
import { input, lists } from "./testInput.js";

describe("getLists", () =>{
    it("parses the Input String into two lists", () => {
        expect(getLists(input)).toStrictEqual(lists);
    })
})
