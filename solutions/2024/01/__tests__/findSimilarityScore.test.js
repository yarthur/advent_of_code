import { describe, it, expect } from "vitest";
import findSimilarityScore from "../findSimilarityScore.js";
import { lists } from "./testInput.js";

describe("findSimilarityScore", () => {
    it("calculates the similarity score of two lists", () => {
        expect(findSimilarityScore(lists)).toBe(31);
    });
});
