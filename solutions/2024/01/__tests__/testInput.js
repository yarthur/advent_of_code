/**
 * @type {string}
 */
export const input = `3   4
4   3
2   5
1   3
3   9
3   3`;

export const lists = [
    [ 3, 4, 2, 1, 3, 3 ],
    [ 4, 3, 5, 3, 9, 3 ],
];
