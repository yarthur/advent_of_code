import { describe, it, expect } from "vitest";
import findTotalDistance from "../findTotalDistance.js";
import { lists } from "./testInput.js";

describe("findTotalDistance", () => {
    it("finds the total distance between two lists", () => {
        expect(findTotalDistance(lists)).toBe(11);
    });
});
