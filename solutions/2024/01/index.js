/**
 * My solutions for the
 * [2024 edition of Advent of Code, day 1](https://adventofcode.com/2024/day/1/).
 * @namespace 01
 * @summary undefined
 * @memberof 2024
 */

import getLists from "./getLists.js";
import findTotalDistance from "./findTotalDistance.js";
import findSimilarityScore from "./findSimilarityScore.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2024.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The total distance between the two lists.
 */
export const part1 = (input) => {
    const lists = getLists(input);

    return findTotalDistance(lists);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2024.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The Similarity Score of the two lists.
 */
export const part2 = (input) => {
    const lists = getLists(input);

    return findSimilarityScore(lists);
};
