/**
 * @function findTotalDistance
 * @summary Finds the Total distance between two lists.
 * @memberof 2024.01
 * @param { Array< Array< number >, Array< number > >} lists - The two lists of points we're finding the distance from.
 * @returns { number } - The total distance between the Lists.
 */
export default (lists) => {
    const secondList = lists[1].sort();

    return lists[0].sort()
        .reduce((distance, pointA, index) => {
            const pointB = secondList[index];
            
            return distance + Math.abs(pointA - pointB);
        }, 0);
};
