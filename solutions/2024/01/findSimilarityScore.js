/**
 * @function findSimilarityScore
 * @summary Calculates the Similarity Score of two lists.
 * @memberof 2024.01
 * @param { Array< Array< number >, Array< number > > } lists - The two Lists.
 * @returns { number } The resulting Similarity Score.
 */
export default (lists) => {
    const valueCounts = new Map();

    lists[1].forEach((value) => {
        let count = valueCounts.get(value);

        if (count === undefined) {
            count = 0;
        }

        valueCounts.set(value, count + 1);
    });

    return lists[0].reduce((score, value) => {
        let count = valueCounts.get(value);
        if (count === undefined) {
            return score;
        }

        return score + (value * count);
    }, 0);
};
