# 2024 — ⭐️: 4

My solutions for the [2024 edition of Advent of Code](https://adventofcode.com/2024/)

## Solutions

- [Day 1](/solutions/2024/01/index.js) — ⭐️⭐️
- [Day 3](/solutions/2024/03/index.js) — ⭐️⭐️
