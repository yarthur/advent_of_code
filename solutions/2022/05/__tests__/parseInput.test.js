import { describe, it, expect } from "vitest";
import parseInput from "../parseInput.js";
import { input, startingStacks, instructions } from "./testInput.js";

describe("parseInput", () => {
    it("parses the input into stacks and move instructions.", () => {
        expect(parseInput(input)).toStrictEqual([startingStacks, instructions]);
    });
});
