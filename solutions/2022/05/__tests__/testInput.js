/**
 * @typedef {import("../types").StackMap} StackMap
 * @typedef {import("../types").Instruction} Instruction
 */

/**
 * @type {string}
 */
export const input = `    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`;

/**
 * @type {StackMap}
 */
export const startingStacks = new Map([
    ["1", ["Z", "N"]],
    ["2", ["M", "C", "D"]],
    ["3", ["P"]],
]);

/**
 * @type {Instruction[]}
 */
export const instructions = [
    {
        qty: 1,
        origin: "2",
        destination: "1",
    },
    {
        qty: 3,
        origin: "1",
        destination: "3",
    },
    {
        qty: 2,
        origin: "2",
        destination: "1",
    },
    {
        qty: 1,
        origin: "1",
        destination: "2",
    },
];

/**
 * @type {object} restacked
 * @property {StackMap} restacked.oneAtATime
 * @property {StackMap} restacked.allAtOnce
 */
export const restacked = {
    oneAtATime: new Map([
        ["1", ["C"]],
        ["2", ["M"]],
        ["3", ["P", "D", "N", "Z"]],
    ]),
    allAtOnce: new Map([
        ["1", ["M"]],
        ["2", ["C"]],
        ["3", ["P", "Z", "N", "D"]],
    ]),
};
