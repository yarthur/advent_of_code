import { describe, it, expect } from "vitest";
import moveStacks from "../moveStacks.js";
import { startingStacks, instructions, restacked } from "./testInput.js";

describe("moveStacks", () => {
    it("rearranges the crates correctly by moving them one at a time.", () => {
        expect(moveStacks(startingStacks, instructions, 9000)).toStrictEqual(
            restacked.oneAtATime
        );
    });

    it("rearranges the crates correctly and all at once.", () => {
        expect(moveStacks(startingStacks, instructions, 9001)).toStrictEqual(
            restacked.allAtOnce
        );
    });
});
