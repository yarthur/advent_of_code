/**
 * @function moveStacks
 * @summary Moves crates around the stacks.
 * @memberof 2022.05
 * @param { 2022.05.StackMap } startingStacks - The stacks of crates before rearranging..
 * @param { Array<2022.05.Instruction> } instructions - The rearrangement instructions.
 * @param {number} [cmModelNumber] - The CrateMover model number.
 * @returns { 2022.05.StackMap } The stacks of crates at the end of rearranging.
 */
export default (startingStacks, instructions, cmModelNumber = 9000) => {
    return instructions.reduce((stacks, instruction) => {
        const { qty, origin, destination } = instruction;
        const originStack = stacks.get(origin);
        const destinationStack = stacks.get(destination);

        let xferStack = originStack.slice(qty * -1);

        // Reverse the transfer stack if the crane can't move more than 1 at a time.
        if ([9001].includes(cmModelNumber) === false) {
            xferStack.reverse();
        }

        stacks.set(origin, originStack.slice(0, qty * -1));
        stacks.set(destination, destinationStack.concat(xferStack));

        return stacks;
    }, new Map([...startingStacks]));
};
