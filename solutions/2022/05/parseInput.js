/**
 * @function parseStacks
 * @summary Parses the input string into a Stack Map.
 * @memberof 2022.05
 * @private
 * @param { string } stackInput - The puzzle input string.
 * @returns { 2022.05.StackMap }
 */
const parseStacks = (stackInput) => {
    const stackRows = stackInput.split("\n");

    const stackLabels = stackRows.pop().trim().split("   ");

    const stacks = new Map();

    stackLabels.forEach((label, index) => {
        const stackLocation = index * 4 + 1;

        stackRows.forEach((row) => {
            const crate = row.charAt(stackLocation);

            if (crate !== " ") {
                const stack = stacks.get(label) || [];
                stack.unshift(crate);
                stacks.set(label, stack);
            }
        });
    });

    return stacks;
};

/**
 * @function parseInstructions
 * @summary Parses the instructions from the input string.
 * @memberof 2022.05
 * @private
 * @param {string} instructionsInput - The input string for the Instructions.
 * @returns { Array<2022.05.Instruction> } An array of rearrangement Instructions.
 */
const parseInstructions = (instructionsInput) => {
    const instructions = instructionsInput.split("\n");

    return instructions.map((instruction) => {
        const [qty, origin, destination] = [...instruction.match(/\d+/g)];

        return {
            qty: Number.parseInt(qty, 10),
            origin,
            destination,
        };
    });
};

/**
 * @function parseInput
 * @summary Parses the input string into a map of Stacks, and Parsing Instructions.
 * @memberof 2022.05
 * @param { string } input - The puzzle's input.
 * @returns { Array<2022.05.StackMap, Array<2022.05.Instruction>> } A StackMap and collection of Instructions.
 */
export default (input) => {
    const [stackInput, instructionsInput] = input.split("\n\n");

    return [parseStacks(stackInput), parseInstructions(instructionsInput)];
};
