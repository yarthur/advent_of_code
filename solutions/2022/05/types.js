/**
 * @typedef { Map<string, Array<string>> } StackMap
 * @summary The map of stacks.
 * @memberof 2022.05
 */

/**
 * @typedef { object } Instruction
 * @summary A step in the rearrangement procedure.
 * @memberof 2022.05
 * @property { number } qty - The quantity of crates that should be moved.
 * @property { string } origin - The stack the crate(s) are moving from.
 * @property { string } destination - The stack the crate(s) are moving to.
 */
