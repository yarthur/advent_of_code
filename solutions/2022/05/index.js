/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 5](https://adventofcode.com/2022/05/).
 * @namespace 05
 * @summary Supply Stacks
 * @memberof 2022
 */

import parseInput from "./parseInput.js";
import moveStacks from "./moveStacks.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.05
 * @param { string } input - The input string.
 * @returns { number } The crate ends up on top of each stack after the rearrangement procedure completes.
 */
export const part1 = (input) => {
    const [startingStacks, instructions] = parseInput(input);

    const finalStacks = moveStacks(startingStacks, instructions, 9000);

    const finalCrates = Array.from(finalStacks.values()).map((stack) => {
        return stack.pop();
    });

    return finalCrates.join("");
};


/**
 * @function part2
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.05
 * @param { string } input - The input string.
 * @returns { number } The crate ends up on top of each stack after the rearrangement procedure completes.
 */
export const part2 = (input) => {
    const [startingStacks, instructions] = parseInput(input);

    const finalStacks = moveStacks(startingStacks, instructions, 9001);

    const finalCrates = Array.from(finalStacks.values()).map((stack) => {
        return stack.pop();
    });

    return finalCrates.join("");
};
