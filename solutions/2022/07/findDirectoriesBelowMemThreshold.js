/**
 * @function findDirectoriesBelowMemThreshold
 * @summary Find directories in a file system that are below a given memory size.
 * @memberof 2022.07
 * @param { 2022.07.FileSystem } fs - The filesystem being scoured.
 * @param { number } threshold - The size threshold to be under.
 * @returns { 2022.07.FileSystem }
 */
export default (fs, threshold) => {
    const filteredDirs = new Map();

    fs.forEach((dir, name) => {
        if (dir.size <= threshold) {
            filteredDirs.set(name, dir);
        }
    });

    return filteredDirs;
};
