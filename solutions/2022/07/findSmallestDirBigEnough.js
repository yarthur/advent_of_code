/**
 * @function findSmallestDirBigEnough
 * @summary Find the smallest directory that, if deleted, would free up enough space.
 * @memberof 2022.07
 * @param { 2022.07.FileSystem } fs - The filesystem being
 * searched.
 * @param { number } targetSize - The amount of free space needed.
 * @param { number } seedSize - A starting size to check against and ensure we're getting the _smallest_ directory needed.
 * @returns { 2022.07.SmallestDirBigEnough } The smallest directory that, if deleted, would free up enough space on the filesystem.
 */
export default (fs, targetSize, seedSize) => {
    const smallestDirBigEnough = {
        name: "",
        size: seedSize,
    };

    fs.forEach((dir, name) => {
        if (dir.size >= targetSize && dir.size < smallestDirBigEnough.size) {
            smallestDirBigEnough.name = name;
            smallestDirBigEnough.size = dir.size;
        }
    });

    return smallestDirBigEnough;
};
