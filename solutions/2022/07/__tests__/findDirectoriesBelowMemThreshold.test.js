import { describe, it, expect } from "vitest";
import findDirectoriesBelowMemThreshold from "../findDirectoriesBelowMemThreshold.js";
import { fileSystem, smallishDirs } from "./testInput.js";

describe("findDirectoriesBelowMemThreshold", () => {
    it("finds directories below a given memory threshold.", () => {
        expect(
            findDirectoriesBelowMemThreshold(fileSystem, 100000)
        ).toStrictEqual(smallishDirs);
    });
});
