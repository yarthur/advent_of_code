import { describe, it, expect } from "vitest";
import mapFileSystem from "../mapFileSystem.js";
import { input, fileSystem } from "./testInput.js";

describe("mapFileSystem", () => {
    it("generates a map of the file system from the input.", () => {
        expect(mapFileSystem(input)).toStrictEqual(fileSystem);
    });
});
