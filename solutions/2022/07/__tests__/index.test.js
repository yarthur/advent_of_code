import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2022, day 07", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(95437);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toBe(24933642);
    });
});
