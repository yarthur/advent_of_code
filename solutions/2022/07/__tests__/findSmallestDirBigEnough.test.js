import { describe, it, expect } from "vitest";
import findSmallestDirBigEnough from "../findSmallestDirBigEnough.js";
import { fileSystem, smallestDirBigEnough } from "./testInput.js";

describe("findSmallestDirBigEnough", () => {
    it("finds the smallest directory that would free up enough space to update the system.", () => {
        expect(
            findSmallestDirBigEnough(fileSystem, 8381165, 70000000)
        ).toStrictEqual(smallestDirBigEnough);
    });
});
