/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 7](https://adventofcode.com/2022/07/).
 * @namespace 07
 * @summary No Space Left On Device
 * @memberof 2022
 */

import mapFileSystem from "./mapFileSystem.js";
import findDirectoriesBelowMemThreshold from "./findDirectoriesBelowMemThreshold.js";
import findSmallestDirBigEnough from "./findSmallestDirBigEnough.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.07
 * @param { string } input - The input string.
 * @returns { number } The sum of the total sizes of all directories with a size > 100000.
 */
export const part1 = (input) => {
    const fs = mapFileSystem(input);
    const smallishDirs = findDirectoriesBelowMemThreshold(fs, 100000);

    return Array.from(smallishDirs.values()).reduce((total, { size }) => {
        return total + size;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.07
 * @param { string } input - The input string.
 * @returns { number } The total size of smallest directory that, if deleted, would free up enough space on the filesystem.
 */
export const part2 = (input) => {
    const fs = mapFileSystem(input);

    const currentAvailableSpace = 70000000 - fs.get("~").size;
    const targetSize = 30000000 - currentAvailableSpace;

    const primeTarget = findSmallestDirBigEnough(fs, targetSize, 70000000);
    return primeTarget.size;
};
