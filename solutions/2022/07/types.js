/**
 * @typedef { object } Directory
 * @summary A representation of a directory within a filesystem.
 * @memberof 2022.07
 * @property { Map<string, number> } files - A map of files in the directory
 * @property { Set<string> } directories - A set of  names of the child directories
 * @property { number  } size - The cumulative size of the directory's children.
 */

/**
 * @typedef { Map<string, Directory> } FileSystem - a
 * @summary A Map representing a filesystem.
 * @memberof 2022.07
 */

/**
 * @typedef { object } SmallestDirBigEnough
 * @summary Information on the smallest directory that is big enough for our needs.
 * @memberof 2022.07
 * @property { string } name - The name of the  directory.
 * @property { number } size - The size of the  directory.
 */
