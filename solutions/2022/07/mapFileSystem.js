/**
 * @function mapFileSystem
 * @summary Takes the puzzle in put, and returns a Map of the filesystem with cumulative sizes calculated.
 * @memberof 2022.07
 * @param { string } input - The raw puzzle input string.
 * @returns { 2022.07.FileSystem } The filesystem map, with  cumulative sizes calculated on the directories.
 */
export default (input) => {
    const fs = drawMap(input);

    return calculateSizes(fs);
};

/**
 * @function directoryTemplate
 * @summary Generates a generic Directory object.
 * @memberof 2022.07
 * @private
 * @returns { 2022.07.Directory } The boilerplate structure for a Directory.
 */
const directoryTemplate = () => {
    return {
        files: new Map(),
        directories: new Set(),
        size: 0,
    };
};

/**
 * @function drawMap
 * @summary Parses the puzzle input string into a Filesystem.
 * @memberof 2022.07
 * @private
 * @param { string } input - The raw puzzle input string.
 * @returns { 2022.07.FileSystem } The file system map.
 */
const drawMap = (input) => {
    const commands = input.split("$ ");

    // Because `$ ` will be the very start of the string, index
    // 0 will be "". So let's get rid of that.
    commands.shift();

    const fileSystem = commands.reduce(
        ({ fs, currentPath }, command) => {
            const [cmdIn, ...cmdOut] = command.split("\n");
            const [verb, ...args] = cmdIn.split(" ");

            if (verb === "ls") {
                const pwd = directoryTemplate();

                cmdOut.forEach((child) => {
                    const [meta, name] = child.split(" ");

                    if (isNaN(Number.parseInt(meta, 10)) === false) {
                        pwd.files.set(name, Number.parseInt(meta, 10));
                    }

                    if (meta === "dir") {
                        pwd.directories.add(name);
                    }
                });

                fs.set(currentPath.join("/"), pwd);
            }

            if (verb === "cd") {
                if (args[0] === "..") {
                    currentPath.pop();
                } else if (args[0] === "/") {
                    currentPath = ["~"];
                } else {
                    currentPath.push(args[0]);
                }
            }

            return { fs, currentPath };
        },
        {
            fs: new Map(),
            currentPath: ["~"],
        }
    );

    return fileSystem.fs;
};

/**
 * @function calculateSizes
 * @summary Calculates the (cumulative) size of the directories on the filesystem.
 * @memberof 2022.07
 * @private
 * @param { 2022.07.FileSystem } fs - The filesystem map.
 * @param { Array<string> } currentPath - The current path being calculated.
 * @returns { 2022.07.FileSystem } The filesystem map with cumulative file sizes on the directories.
 */
const calculateSizes = (fs, currentPath = ["~"]) => {
    let pwd = fs.get(currentPath.join("/"));

    pwd.files.forEach((fileSize) => {
        pwd.size += fileSize;
    });

    pwd.directories.forEach((name) => {
        const childPath = [...currentPath, name];
        calculateSizes(fs, childPath);

        pwd.size += fs.get(childPath.join("/")).size;
    });

    return fs;
};
