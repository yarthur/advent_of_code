/**
 * @typedef { Array } Inspection
 * @summary The Inspection routine for a Monkey.
 * @memberof 2022.11
 * @property { "+" | "-" | "*" | "/" } 0 - The
 * operator used for the routine.
 * @property { number | "old" } 1 - The value the worryLevel is modified to by the Inspection.
 */

/**
 * @typedef { Array } Test
 * @summary The Test routine for a Monkey.
 * @memberof 2022.11
 * @property { number } 0 - The number used to test if it is an exact multiplier of the item's worry level.
 * @property { Map<boolean, number> } 1 - A Map of destinations, keyed off the true/false result of the test.
 */

/**
 * @typedef { object } Monkey
 * @summary A data representation of a Monkey
 * @memberof 2022.11
 * @property { number[] } items - The items in the Monkey's possession.
 * @property { Inspection } inspection - The Inspection routine for the Monkey.
 * @property { Test } test - The Test routine for the Monkey.
 * @property { number } inspectedCount - The running count of items inspected by  the Monkey.
 */
