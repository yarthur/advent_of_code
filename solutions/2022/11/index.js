/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 11](https://adventofcode.com/2022/11/).
 * @namespace 2022.11
 * @summary Monkey in the Middle
 * @memberof 2022
 */

import getMonkeys from "./getMonkeys.js";
import keepAway from "./keepAway.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.11
 * @param { string } input - The input string.
 * @returns { number } The level of monkey business after 20 rounds of stuff-slinging simian shenanigans.
 */
export const part1 = (input) => {
    const monkeys = getMonkeys(input);

    const monkeyBusiness = keepAway(monkeys, 20);

    const twoMostActive = Array.from(monkeyBusiness.values()).reduce(
        (mostActive, { inspectedCount }) => {
            return [...mostActive, inspectedCount]
                .sort((a, b) => {
                    return b - a;
                })
                .splice(0, 2);
        },
        [0, 0]
    );

    return twoMostActive[0] * twoMostActive[1];
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.11
 * @param { string } input - The input string.
 * @returns {boolean} The level of monkey business after 10000 rounds.
 */
export const part2 = (input) => {
    const monkeys = getMonkeys(input);

    const monkeyBusiness = keepAway(monkeys, 10000, false);

    const twoMostActive = Array.from(monkeyBusiness.values()).reduce(
        (mostActive, { inspectedCount }) => {
            return [...mostActive, inspectedCount]
                .sort((a, b) => {
                    return b - a;
                })
                .splice(0, 2);
        },
        [0, 0]
    );

    return twoMostActive[0] * twoMostActive[1];
};
