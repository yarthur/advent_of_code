/**
 * @function keepAway
 * @summary Run through the game of Keep Away
 * @memberof 2022.11
 * @param {Map<number, Monkey>} monkeys - Data associated with the monkeys.
 * @param {number} rounds - Counter variable for the number of rounds.
 * @param {boolean} managedSimply - Flag to determine if our worry is being managed simply (part 1) or not (part 2).
 * @returns {Map<number, Monkey>} - The Monkey data resulting from this round.
 */
export default (monkeys, rounds = 1, managedSimply = true) => {
    const worryManagement = getWorryManagement(monkeys, managedSimply);
    for (let round = 0; round < rounds; round += 1) {
        monkeys.forEach(({ items, inspection, test, inspectedCount }, name) => {
            if (items.length === 0) {
                return;
            }

            inspectedCount += items.length;

            items.forEach((item) => {
                const inspectedItem = inspectItem(
                    item,
                    inspection,
                    worryManagement
                );

                const testResults = inspectedItem % test[0] === 0;

                const destination = test[1].get(testResults);

                const destinationData = monkeys.get(destination);

                destinationData.items.push(inspectedItem);
            });

            monkeys.set(name, { items: [], inspection, test, inspectedCount });
        });
    }

    return monkeys;
};

/**
 * @function getWorryManagement
 * @summary Function to get the worry management modifier routine.
 * @memberof 2022.11
 * @private
 * @param {Map<number, Monkey>} monkeys - Data associated with the monkeys.
 * @param {boolean} managedSimply = true - Flag to determine if our worry is being managed simply (part 1) or not (part 2).
 * @returns {Function} - The function representing how we are managing our worry.
 */
const getWorryManagement = (monkeys, managedSimply) => {
    if (managedSimply) {
        /**
         * Function to modify our worry stat in a simpler manner.
         * @param {number} worry - Numerical representation of our worry stat.
         * @returns {number} - The modified value of our worry stat after simple management.
         */
        return (worry) => {
            return Math.floor(worry / 3);
        };
    }

    const commonMultiple = Array.from(monkeys.values()).reduce(
        (multiple, { test }) => {
            return multiple * test[0];
        },
        1
    );

    /**
     * Function to modify our worry stat in a more complex manner.
     * @param {number} worry - Numerical representation of our worry stat.
     * @returns {number} - The modified value of our worry stat after more complex management.
     */
    return (worry) => {
        return worry % commonMultiple;
    };
};

/**
 * @function inspectItem
 * @summary Function that dictates what happens when an item is inspected.
 * @memberof 2022.11
 * @private
 * @param {number} item - The item being inspected.
 * @param {Inspection} modifier - The inspection parameters for the monkey.
 * @param { "+" | "-" | "*" | "/" } modifier.modifierOperator - The type of modifier of the inpsection.
 * @param { number | "old" } modifier.modifierValue - The modification of the item from inspection.
 * @param {Function} worryManagement - The worry management strategy.
 * @returns {number} - The resultant worry level from inspection.
 */
const inspectItem = (
    item,
    [modifierOperator, modifierValue],
    worryManagement
) => {
    let inspectedItem = item;

    if (modifierValue === "old") {
        modifierValue = item;
    }

    switch (modifierOperator) {
        case "+":
            inspectedItem += modifierValue;
            break;

        case "-":
            inspectedItem -= modifierValue;
            break;

        case "*":
            inspectedItem *= modifierValue;
            break;

        case "/":
            inspectedItem /= modifierValue;
            break;
    }

    return worryManagement(inspectedItem);
};
