import { describe, it, expect } from "vitest";
import getMonkeys from "../getMonkeys.js";
import { input, monkeys } from "./testInput.js";

describe("getMonkeys", () => {
    it("gets the info for the monkeys from the input.", () => {
        expect(getMonkeys(input)).toStrictEqual(monkeys());
    });
});
