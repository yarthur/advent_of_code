/**
 * @type {string}
 */
export const input = `Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1`;

export const monkeys = () => {
    return new Map([
        [
            0,
            {
                items: [79, 98],
                inspection: ["*", 19],
                test: [
                    23,
                    new Map([
                        [true, 2],
                        [false, 3],
                    ]),
                ],
                inspectedCount: 0,
            },
        ],
        [
            1,
            {
                items: [54, 65, 75, 74],
                inspection: ["+", 6],
                test: [
                    19,
                    new Map([
                        [true, 2],
                        [false, 0],
                    ]),
                ],
                inspectedCount: 0,
            },
        ],
        [
            2,
            {
                items: [79, 60, 97],
                inspection: ["*", "old"],
                test: [
                    13,
                    new Map([
                        [true, 1],
                        [false, 3],
                    ]),
                ],
                inspectedCount: 0,
            },
        ],
        [
            3,
            {
                items: [74],
                inspection: ["+", 3],
                test: [
                    17,
                    new Map([
                        [true, 0],
                        [false, 1],
                    ]),
                ],
                inspectedCount: 0,
            },
        ],
    ]);
};

export const keepAwayRounds = new Map([
    [1, [[20, 23, 27, 26], [2080, 25, 167, 207, 401, 1046], [], []]],
    [2, [[695, 10, 71, 135, 350], [43, 49, 58, 55, 362], [], []]],
    [3, [[16, 18, 21, 20, 122], [1468, 22, 150, 286, 739], [], []]],
    [4, [[491, 9, 52, 97, 248, 34], [39, 45, 43, 258], [], []]],
    [5, [[15, 17, 16, 88, 1037], [20, 110, 205, 524, 72], [], []]],
    [6, [[8, 70, 176, 26, 34], [481, 32, 36, 186, 2190], [], []]],
    [7, [[162, 12, 14, 64, 732, 17], [148, 372, 55, 72], [], []]],
    [8, [[51, 126, 20, 26, 136], [343, 26, 30, 1546, 36], [], []]],
    [9, [[116, 10, 12, 517, 14], [108, 267, 43, 55, 288], [], []]],
    [10, [[91, 16, 20, 98], [481, 245, 22, 26, 1092, 30], [], []]],
    [15, [[83, 44, 8, 184, 9, 20, 26, 102], [110, 36], [], []]],
    [20, [[10, 12, 14, 26, 34], [245, 93, 53, 199, 115], [], []]],
]);

export const inspectedCounts = {
    managed: [101, 95, 7, 105],
    unmanaged: new Map([
        [1, [2, 4, 3, 6]],
        [20, [99, 97, 8, 103]],
        [1000, [5204, 4792, 199, 5192]],
        [2000, [10419, 9577, 392, 10391]],
        [3000, [15638, 14358, 587, 15593]],
        [4000, [20858, 19138, 780, 20797]],
        [5000, [26075, 23921, 974, 26000]],
        [6000, [31294, 28702, 1165, 31204]],
        [7000, [36508, 33488, 1360, 36400]],
        [8000, [41728, 38268, 1553, 41606]],
        [9000, [46945, 43051, 1746, 46807]],
        [10000, [52166, 47830, 1938, 52013]],
    ]),
};
