import { describe, it, expect } from "vitest";
import keepAway from "../keepAway.js";
import { monkeys, keepAwayRounds, inspectedCounts } from "./testInput.js";

describe("keepAway", () => {
    it("plays out the results of keep-away for a given number of rounds while your worry is managed.", () => {
        Array.from(keepAwayRounds.entries()).forEach(
            ([testRound, expectedMonkeys]) => {
                const actualMonkeys = keepAway(monkeys(), testRound);

                expectedMonkeys.forEach((expectedItems, monkey) => {
                    expect(actualMonkeys.get(monkey).items).toStrictEqual(
                        expectedItems
                    );
                });
            }
        );

        const actualMonkeys = keepAway(monkeys(), 20);

        inspectedCounts.managed.forEach((expectedCount, monkey) => {
            expect(actualMonkeys.get(monkey).inspectedCount).toBe(
                expectedCount
            );
        });
    });

    it("plays out the results of keep-away for a given number of rounds while your worry is not managed.", () => {
        Array.from(inspectedCounts.unmanaged.entries()).forEach(
            ([testRound, counts]) => {
                const newMonkeys = keepAway(monkeys(), testRound, false);

                counts.forEach((count, monkey) => {
                    expect(newMonkeys.get(monkey).inspectedCount).toBe(count);
                });
            }
        );
    });
});
