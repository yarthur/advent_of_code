/**
 * @typedef { string } nameSubstring
 * @summary The substring to be stripped from the "name" line.
 * @memberof 2022.11
 * @private
 */
const nameSubstring = "Monkey ";

/**
 * @typedef { string } itemsSubstring
 * @summary The substring to be stripped from the "items" line.
 * @memberof 2022.11
 * @private
 */
const itemsSubstring = "  Starting items: ";

/**
 * @typedef { string } inspectSubstring
 * @summary The substring to be stripped from the "inspect" line.
 * @memberof 2022.11
 * @private
 */
const inspectSubstring = "  Operation: new = old ";

/**
 * @typedef { string } testSubstring
 * @summary The substring to be stripped from the "test" line.
 * @memberof 2022.11
 * @private
 */
const testSubstring = "  Test: divisible by ";

/**
 * @typedef { string } trueSubstring
 * @summary The substring to be stripped from the "true" line.
 * @memberof 2022.11
 * @private
 */
const trueSubstring = "    If true: throw to monkey ";

/**
 * @typedef { string } falseSubstring
 * @summary The substring to be stripped from the "false" line.
 * @memberof 2022.11
 * @private
 */
const falseSubstring = "    If false: throw to monkey ";


/**
 * @function getMonkeys
 * @summary Parses the input to define the monkeys.
 * @memberof 2022.11
 * @param { string } input - The puzzle's input string.
 * @returns { Map<number, 2022.11.Monkey> } A map of the monkeys involved
 */
export default (input) => {
    const monkeyStrings = input.split("\n\n");

    return monkeyStrings.reduce((monkeys, monkeyString) => {
        const [
            nameStr,
            itemsString,
            inspectString,
            testString,
            trueString,
            falseString,
        ] = monkeyString.split("\n");

        const name = nameStr.substring(
            nameSubstring.length,
            nameStr.length - 1
        );

        const items = getInventory(itemsString);
        const inspection = getInspection(inspectString);
        const test = getTest(testString, trueString, falseString);

        monkeys.set(Number.parseInt(name, 10), {
            items,
            inspection,
            test,
            inspectedCount: 0,
        });

        return monkeys;
    }, new Map());
};

/**
 * @function getInventory
 * @summary Parses the Inventory data for a Monkey.
 * @memberof 2022.11
 * @private
 * @param { string } itemsString - The raw data string.
 * @returns { Array<number> } - The starting inventory of the Monkey.
 */
const getInventory = (itemsString) => {
    return itemsString
        .substring(itemsSubstring.length)
        .split(", ")
        .map((inv) => {
            return Number.parseInt(inv, 10);
        });
};

/**
 * @function getInspection
 * @summary Parses the details around the Inspection routine of a Monkey.
 * @memberof 2022.11
 * @private
 * @param { string } inspectString - The raw inspection input string.
 * @returns { 2022.11.Inspection } - The Inspection data for the Monkey.
 */
const getInspection = (inspectString) => {
    const parts = inspectString.substring(inspectSubstring.length);

    const [operator, valueString] = parts.split(" ");

    const value =
        valueString === "old" ? valueString : Number.parseInt(valueString, 10);

    return [operator, value];
};

/**
 * Parses the Test routine for a Monkey.
 * @private
 * @param { string } testString - The raw input string for the test.
 * @param { string } trueString - The raw input string for the `result === true` scenario.
 * @param { string } falseString - The raw input string for the `result === false` scenario.
 * @returns { 2022.11.Test } The parsed Test data.
 */
const getTest = (testString, trueString, falseString) => {
    const test = testString.substring(testSubstring.length);
    const trueDestination = trueString.substring(trueSubstring.length);
    const falseDestination = falseString.substring(falseSubstring.length);

    return [
        Number.parseInt(test, 10),
        new Map([
            [true, Number.parseInt(trueDestination, 10)],
            [false, Number.parseInt(falseDestination, 10)],
        ]),
    ];
};
