# 2022 — ⭐️: 20

My solutions for the
[2022 edition of Advent of Code](https://adventofcode.com/2022/)

## Solutions

-   [01](./01/index.js): ⭐️⭐️
-   [02](./02/index.js): ⭐️⭐️
-   [03](./03/index.js): ⭐️⭐️
-   [04](./04/index.js): ⭐️⭐️
-   [05](./05/index.js): ⭐️⭐️
-   [06](./06/index.js): ⭐️⭐️
-   [07](./07/index.js): ⭐️⭐️
-   [08](./08/index.js): ⭐️⭐️
-   [10](./10/index.js): ⭐️⭐️
-   [11](./11/index.js): ⭐️⭐️
