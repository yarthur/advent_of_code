/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 6](https://adventofcode.com/2022/06/).
 * @namespace 06
 * @summary Tuning Trouble
 * @memberof 2022
 */
import getDataStream from "./getDataStream.js";
import findFirstMarker from "./findFirstMarker.js";

/**
 * @function part1
 * @summary Solves part 1 of th day's puzzle.
 * @memberof 2022.06
 * @param { string } input - The puzzle's input string.
 * @returns { number } The number of characters needed to be processed before the first start-of-packet marker is detected.
 */
export const part1 = (input) => {
    const dataStream = getDataStream(input);

    const firstMarker = findFirstMarker(dataStream, 4);

    return firstMarker.end;
};

/**
 * @function part2
 * @summary Solves part 2 of th day's puzzle.
 * @memberof 2022.06
 * @param { string } input - The puzzle's input string.
 * @returns { number } The number of characters needed to be processed before the first start-of-packet marker is detected.
 */
export const part2 = (input) => {
    const dataStream = input.split("");

    const startOfMessage = findFirstMarker(dataStream, 14);

    return startOfMessage.end;
};
