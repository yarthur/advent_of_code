/**
 * @function findFirstMarker
 * @summary Finds the first marker (1 marker length of unique characters) within the Datastream.
 * @memberof 2022.06
 * @param { Array<string> } dataStream - The Datastream buffer.
 * @param { number } [markerLength = 2] - The length of the marker string.
 * @returns { 2022.06.Marker } The Marker data for the first Marker packet.
 */
export default (dataStream, markerLength = 2) => {
    const lastPossibleIndex = dataStream.length - markerLength;
    let marker;
    let markerIndex = -1;
    for (
        let index = 0;
        index < lastPossibleIndex && markerIndex < 1;
        index += 1
    ) {
        const possibleMarker = new Set(
            dataStream.slice(index, index + markerLength)
        );

        // This will match
        if (possibleMarker.size === markerLength) {
            marker = [...possibleMarker];
            markerIndex = index;
        }
    }

    return {
        marker,
        start: markerIndex,
        end: markerIndex + markerLength,
    };
};
