import { describe, it, expect } from "vitest";
import findFirstMarker from "../findFirstMarker.js";
import testCases from "./testInput.js";

describe("findFirstMarker", () => {
    it("finds the first marker in the dataStream.", () => {
        testCases.forEach(({ dataStream, startOfPacket, startOfMessage }) => {
            expect(findFirstMarker(dataStream, 4)).toStrictEqual(startOfPacket);
            expect(findFirstMarker(dataStream, 14)).toStrictEqual(
                startOfMessage
            );
        });
    });
});
