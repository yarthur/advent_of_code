import { describe, it, expect } from "vitest";
import testCases from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2022, day 06", () => {
    it("solves for part 1", () => {
        testCases.forEach(({ input, startOfPacket }) => {
            expect(part1(input)).toBe(startOfPacket.end);
        });
    });

    it("solves for part 2", () => {
        testCases.forEach(({ input, startOfMessage }) => {
            expect(part2(input)).toBe(startOfMessage.end);
        });
    });
});
