import { describe, it, expect } from "vitest";
import getDataStream from "../getDataStream.js";
import testCases from "./testInput.js";

describe("getDataStream", () => {
    it("parses the input into a data stream.", () => {
        testCases.forEach(({ input, dataStream }) => {
            expect(getDataStream(input)).toStrictEqual(dataStream);
        });
    });
});
