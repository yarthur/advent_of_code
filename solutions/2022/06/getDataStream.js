/**
 * @function getDataStream
 * @summary Parses the puzzle input string into a Datastream.
 * @memberof 2022.06
 * @param {string} input - The puzzle's input string.
 * @returns {string[]} The Datastream buffer.
 */
export default (input) => {
    return input.split("");
};
