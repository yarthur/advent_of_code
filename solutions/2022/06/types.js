/**
 * @typedef { object } Marker
 * @summary Describes the packet that serves as a Marker in a Datastream.
 * @memberof 2022.06
 * @property { Array<string> } marker - The Marker string itself.
 * @property { number } start - The index of the start of the packet.
 * @property { number } end - The index of the end of the packet.
 */

/**
 * @typedef { object } TestCase
 * @summary Describes the data around a test case for 2022.06
 * @memberof 2022.06
 * @private
 * @property { string } input - The input string to test against.
 * @property { Array<string> } dataStream - The Datastream that comes from the input string.
 * @property { 2022.06.Marker } startOfPacket - The Marker that indicates the start of the packet.
 * @property { 2022.06.Marker } startOfMessage - The Marker that indicates the start of the message.
 */
