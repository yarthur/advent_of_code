import { moveScores, outcomes, moves, theirMoves } from "./constants.js";

/**
 * @constant myMoves
 * @summary Translates the Round's second data point into a Move.
 * @memberof 2022.02
 * @private
 * @type { {[Code in 2022.02.MyMoveCodes]: Moves} }
 */
const myMoves = {
    X: "Rock",
    Y: "Paper",
    Z: "Scissors",
};

/**
 * @function getMatchResult
 * @summary Determine the result of a round of Rock Paper Scisors.
 * @memberof 2022.02
 * @private
 * @param { 2022.02.MoveResults } moveData
 * @param { 2022.02.Moves } theirMove
 * @returns { 2022.02.Results }
 */
const getMatchResult = (moveData, theirMove) => {
    const resultData = Object.entries(moveData).filter(
        (datum) => {
            return datum[1] === theirMove;
        }
    );

    return resultData[0][0];
};

/**
 * @function scoreByMove
 * @summary Computes the result of a round of Rock Paper Scissors, assuming the second value of the Round is your move.
 * @memberof 2022.02
 * @param { Array<2022.02.Round> } rounds - The Rounds of Rock Paper Scissors being played.
 * @returns { Array<2022.02.Score> } The resulting scores for each round of Rock Paper Scissors.
 */
export default (rounds) => {
    return rounds.map(([them, me]) => {
        const theirMove = theirMoves[them];
        const myMove = myMoves[me];
        const myMoveData = moves[myMove];

        const result = getMatchResult(myMoveData, theirMove);

        const shape = moveScores[myMove];
        const outcome = outcomes[result];

        return {
            shape,
            outcome,
            score: shape + outcome,
        };
    });
};
