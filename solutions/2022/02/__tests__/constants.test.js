import { describe, it, expect } from "vitest";
import * as constants from "../constants.js";

describe("constants for 2022, day 02.", () => {
    it("matches snapshot.", () => {
        expect(constants).toMatchSnapshot();
    });
});
