import { describe, it, expect } from "vitest";
import scoreByOutcome from "../scoreByOutcome.js";
import { rounds, scoresByOutcome } from "./testInput.js";

describe("scoreByOutcome", () => {
    it("scores the rounds properly when the second value is assumed to be the round's outcome.", () => {
        expect(scoreByOutcome(rounds)).toStrictEqual(scoresByOutcome);
    });
});
