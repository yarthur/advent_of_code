// @ts-check

/**
 * @typedef {import("../types").Round} Round
 * @typedef {import("../types").Score} Score
 */

/**
 * @type {string}
 */
export const input = `A Y
B X
C Z`;

/**
 * @type {[Round]}
 */
export const rounds = [
    ["A", "Y"],
    ["B", "X"],
    ["C", "Z"],
];

/**
 * @type {[Score]}
 */
export const scoresByMove = [
    {
        shape: 2,
        outcome: 6,
        score: 8,
    },
    {
        shape: 1,
        outcome: 0,
        score: 1,
    },
    {
        shape: 3,
        outcome: 3,
        score: 6,
    },
];

/**
 * @type {[Score]}
 */
export const scoresByOutcome = [
    {
        shape: 1,
        outcome: 3,
        score: 4,
    },
    {
        shape: 1,
        outcome: 0,
        score: 1,
    },
    {
        shape: 1,
        outcome: 6,
        score: 7,
    },
];
