import { describe, it, expect } from "vitest";
import scoreByMove from "../scoreByMove.js";
import { rounds, scoresByMove } from "./testInput.js";

describe("scoreByMove", () => {
    it("scores the rounds properly when the second value is assumed to be your move.", () => {
        expect(scoreByMove(rounds)).toStrictEqual(scoresByMove);
    });
});
