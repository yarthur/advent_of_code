import { describe, it, expect } from "vitest";
import getRounds from "../getRounds.js";
import { input, rounds } from "./testInput.js";

describe("getRounds", () => {
    it("parses the outcome of rounds based on input.", () => {
        expect(getRounds(input)).toStrictEqual(rounds);
    });
});
