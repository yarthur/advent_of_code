/**
 * @typedef {"A" | "B" | "C"} TheirMoveCodes
 * @summary The strategy guide's codes for the opponent's move.
 * @memberof 2022.02
 */

/**
 * @typedef {"X" | "Y" | "Z"} MyMoveCodes
 * @summary The strategy guide's response to your opponent for the round.
 * @memberof 2022.02
 * @description 
 * This value has a couple interpretations. For Part 1, it is "your" move: `X`
 * for Rock, `Y` for Paper, and `Z` for Scissors. For Part 2, it is the outcome
 * of the match: `X` means you need to lose, `Y` means you need to draw, and `Z`
 * means you need to win.
 */

/**
 * @typedef { Array } Round
 * @summary The data involved in one round of Rock, Paper, Scissors.
 * @memberof 2022.02
 * @property { "A" | "B" | "C" } 0 - The opponent's move.
 * @property { MyMoveCodes } 1 - Your move (part 1), or the outcome of the match (part 2).
 * @description
 * The strategy guide's rounds.
 * The first value is the opponent's move: `A` for Rock, `B` for Paper, and `C`
 * for Scissors.
 * The second column has a couple interpretations. For Part 1, it is "your"
 * move: `X` for Rock, `Y` for Paper, and `Z` for Scissors. For Part 2, it is
 * the outcome of the match: `X` means you need to lose, `Y` means you need to
 * draw, and `Z` means you need to win.
 */

/**
 * @typedef { "Rock" | "Paper" | "Scissors" } Moves
 * @memberof 2022.02
 */

/**
 * @typedef { object } Score
 * @summary The score for a single round of Rock Paper Scissors.
 * @memberof 2022.02
 * @property { 1 | 2 | 3 } Score.shape - The shape you selected.
 * @property { 0 | 3 | 6} Score.outcome - The score for the outcome of the round.
 * @property { number } Score.score - The total score for the round.
 * @description
 * The score is made up of 2 parts, summed together:
 *     1. The score for the _shape you selected_ (1 for Rock, 2 for Paper, and
 *         3 for Scissors) 
 *     2. The score for the _outcome of the round_ (0 if you lost, 3 if the
 *        round was a draw, and 6 if you won).
 */
 
// export type Results = "win" | "loss" | "tie";
//
// export type MoveResults = {
//     [Result in Results]: Move;
// };
