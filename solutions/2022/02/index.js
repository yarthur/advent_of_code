/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 2](https://adventofcode.com/2022/02/).
 * @namespace 02
 * @summary Rock Paper Scissors
 * @memberof 2022
 */

import getRounds from "./getRounds.js";
import scoreByMove from "./scoreByMove.js";
import scoreByOutcome from "./scoreByOutcome.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.02
 * @param { string } input - The input string.
 * @returns {number} The total score be if everything goes exactly according to your strategy guide.
 */
export const part1 = (input) => {
    const rounds = getRounds(input);
    const scores = scoreByMove(rounds);

    return scores.reduce((total, { score }) => {
        return total + score;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.02
 * @param { string } input - The input string.
 * @returns {number} The total score be if everything goes exactly according to your strategy guide.
 */
export const part2 = (input) => {
    const rounds = getRounds(input);
    const scores = scoreByOutcome(rounds);

    return scores.reduce((total, { score }) => {
        return total + score;
    }, 0);
};
