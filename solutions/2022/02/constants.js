/**
 * Score point values associated with each shape.
 * @constant moveScores
 * @type {{[Move in 2022.02.Moves]: number}}
 * @memberof 2022.02
 */
export const moveScores = {
    Rock: 1,
    Paper: 2,
    Scissors: 3,
};

/**
 * Score point values associated with each outcome.
 * @constant outcomes
 * @type { {[Result in 2022.02.Results]: number} }
 * @memberof 2022.02
 */
export const outcomes = {
    win: 6,
    loss: 0,
    tie: 3,
};

/**
 * @constant moves
 * @summary A key of outcomes based on all move combinations.
 * @type { {[Move in 2022.02.Moves]: MoveResults} }
 * @memberof 2022.02
 * @type { object }
 * @property { 2022.02.MoveResults } Rock - The Move Results for the Rock move.
 * @property { 2022.02.MoveResults } Paper - The Move Results for the Paper move.
 * @property { 2022.02.MoveResults } Scissors - The Move Results for the Scissors move.
 */
export const moves = {
    Rock: {
        win: "Scissors",
        loss: "Paper",
        tie: "Rock",
    },
    Paper: {
        win: "Rock",
        loss: "Scissors",
        tie: "Paper",
    },
    Scissors: {
        win: "Paper",
        loss: "Rock",
        tie: "Scissors",
    },
};

/**
 * A key of outcomes for the match.``
 * @constant theirMoves
 * @type {{[Code in 2022.02.TheirMoveCodes]: Moves }}
 * @memberof 2022.02
 */
export const theirMoves = {
    A: "Rock",
    B: "Paper",
    C: "Scissors",
};
