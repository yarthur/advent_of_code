import { moveScores, outcomes, moves, theirMoves } from "./constants.js";

/**
 * @constant resultsKey
 * @summary Translate's the Round's second data point into a  result.
 * @memberof 2022.02
 * @private
 * @type {{[Code in 2022.02.MyMoveCodes]: Results}}
 */
const resultsKey = {
    X: "loss",
    Y: "tie",
    Z: "win",
};

/**
 * @function scoreByOutcome
 * @summary Computes the result of a round of Rock Paper Scissors, assuming the second value of the Round is outcome of the round.
 * @memberof 2022.02
 * @param { Array<2022.02.Round> } rounds - The Rounds of Rock Paper Scissors being played.
 * @returns { Array<2022.02.Score> } The resulting scores for each round of Rock Paper Scissors.
 */
export default (rounds) => {
    return rounds.map(([them, resultCode]) => {
        const theirMove = theirMoves[them];
        const result = resultsKey[resultCode];

        const myMove = Object.entries(moves).reduce((move, [key, data]) => {
            if (move === null) {
                move = data[result] === theirMove ? key : null;
            }

            return move;
        }, null);

        const shape = moveScores[myMove];
        const outcome = outcomes[result];

        return {
            shape,
            outcome,
            score: shape + outcome,
        };
    });
};
