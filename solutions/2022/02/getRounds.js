/**
 * @function getRounds
 * @summary Parses the puzzle input into Rounds.
 * @memberof 2022.02
 * @param { string } input The puzzle input string.
 * @returns { Array<2022.02.Round> } An array of Rounds.
 */
export default (input) => {
    const rounds = input.split("\n");

    return rounds.map((round) => {
        return round.split(" ");
    });
};
