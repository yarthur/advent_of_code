/**
 * Generates a sorted list of Total Calories carried by individual Elves.
 * @function getSortedTotals
 * @memberof 2022.01
 * @param { Array< 2022.01.Loadout > } loadouts - Information about the Calories carried by a group of Elves.
 * @returns {[number]} A sorted list of Total Calories carried by individual Elves.
 */
export default (loadouts) => {
    const totals = loadouts.map(({ total }) => {
        return total;
    });

    const sortedTotals = totals.sort((a, b) => {
        return b - a;
    });

    return sortedTotals;
};
