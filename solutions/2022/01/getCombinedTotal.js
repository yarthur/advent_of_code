/**
 * Gets the combined Total Calories carried by a given number of Elves.
 * @function getCombinedTotal
 * @memberof 2022.01
 * @param { Array< number > } totals - A list of Total Calories carried by individual Elves.
 * @param { number } numberToCombine - The number of Elves from the group to combine.
 * @returns {number} The combined Total Calories carried by the specified number of Elves.
 */
export default (totals, numberToCombine) => {
    let combinedTotal = 0;

    for (let index = 0; index < numberToCombine; index += 1) {
        combinedTotal += totals[index] || 0;
    }

    return combinedTotal;
};
