/**
 * Parses the puzzle's input string into the Loadout of a group of Elves.
 * @function getLoadouts
 * @memberof 2022.01
 * @param { string } input - The puzzle's input string.
 * @returns { Array< 2022.01.Loadout > } Information about the Calories carried by a group of Elves.
 */
export default (input) => {
    const loadouts = input.split("\n\n");

    const elfSnackPacks = loadouts.map((loadout) => {
        const snacks = loadout.split("\n");

        const individual = snacks.map((snack) => {
            return Number.parseInt(snack, 10);
        });

        const total = individual.reduce((total, snack) => {
            return total + snack;
        }, 0);

        return {
            individual,
            total,
        };
    });

    return elfSnackPacks;
};
