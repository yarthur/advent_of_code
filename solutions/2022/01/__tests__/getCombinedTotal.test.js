import { describe, it, expect } from "vitest";
import getCombinedTotal from "../getCombinedTotal.js";
import { sortedTotals } from "./testInput.js";

describe("getCombinedTotal", () => {
    it("gets the combined total of the first X number of records.", () => {
        expect(getCombinedTotal(sortedTotals, 3)).toBe(45000);
    });
});
