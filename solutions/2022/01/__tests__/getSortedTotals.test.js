import { describe, it, expect } from "vitest";
import getSortedTotals from "../getSortedTotals.js";
import { loadouts, sortedTotals } from "./testInput.js";

describe("getSortedTotals", () => {
    it("returns an array of totals sorted highest to lowest.", () => {
        expect(getSortedTotals(loadouts)).toStrictEqual(sortedTotals);
    });
});
