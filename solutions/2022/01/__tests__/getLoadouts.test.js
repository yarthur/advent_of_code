import { describe, it, expect } from "vitest";
import getLoadouts from "../getLoadouts.js";
import { input, loadouts } from "./testInput.js";

describe("getLoadout", () => {
    it("parses the input into loadout data.", () => {
        expect(getLoadouts(input)).toStrictEqual(loadouts);
    });
});
