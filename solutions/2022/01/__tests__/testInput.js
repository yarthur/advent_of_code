export const input = `1000
2000
3000

4000

5000
6000

7000
8000
9000

10000`;

export const loadouts = [
    {
        individual: [1000, 2000, 3000],
        total: 6000,
    },
    {
        individual: [4000],
        total: 4000,
    },
    {
        individual: [5000, 6000],
        total: 11000,
    },
    {
        individual: [7000, 8000, 9000],
        total: 24000,
    },
    {
        individual: [10000],
        total: 10000,
    },
];

export const sortedTotals = [24000, 11000, 10000, 6000, 4000];
