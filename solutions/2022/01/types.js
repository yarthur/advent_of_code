/**
 * @typedef { object } Loadout
 * @summary Information about the Calories carried by an individual Elf.
 * @memberof 2022.01
 * @property { Array< number > } individual - The Caloric value of individual meals, snacks, rations, etc. held by the Elf.
 * @property { number } total - The combined Caloric  value of the meals, snacks, rations, etc, held by the Elf.
 */
