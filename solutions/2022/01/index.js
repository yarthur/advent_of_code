/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 1](https://adventofcode.com/2022/01/).
 * @namespace 01
 * @summary Calorie Counting
 * @memberof 2022
 */
import getLoadouts from "./getLoadouts.js";
import getSortedTotals from "./getSortedTotals.js";
import getCombinedTotal from "./getCombinedTotal.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The highest number of calories carried by any single elf in the group.
 */
export const part1 = (input) => {
    const elfSnackPacks = getLoadouts(input);

    return elfSnackPacks.reduce((highest, { total }) => {
        return total > highest ? total : highest;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The highest number of calories carried by the three elves in the group carrying the most calories.
 */
export const part2 = (input) => {
    const loadouts = getLoadouts(input);

    const sortedTotals = getSortedTotals(loadouts);

    return getCombinedTotal(sortedTotals, 3);
};
