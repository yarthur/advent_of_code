/**
 * @function getTrees
 * @summary Parses the puzzle input and returns a 2-dimmensional representation of the forest.
 * @memberof 2022.08
 * @param { string } input - The puzzle input.
 * @returns { 2022.08.Forest } A representation of trees in a 2-dimmensional map of the forest.
 */
export default (input) => {
    const rows = input.split("\n");

    return rows.map((row) => {
        const trees = row.split("");

        return trees.map((tree) => {
            return Number.parseInt(tree, 10);
        });
    });
};
