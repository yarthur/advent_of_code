import { describe, it, expect } from "vitest";
import calculateScenicValues from "../calculateScenicValues.js";
import { trees } from "./testInput.js";

describe("calculateScenicValues", () => {
    it("calculates the scenic value of each tree in a forest.", () => {
        const scenicValues = calculateScenicValues(trees);

        expect(scenicValues.get("2,1").total).toBe(4);
        expect(scenicValues.get("2,3").total).toBe(8);
    });
});
