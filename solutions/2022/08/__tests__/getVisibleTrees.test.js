import { describe, it, expect } from "vitest";
import getVisibleTrees from "../getVisibleTrees.js";
import { trees, visibleTrees } from "./testInput.js";

describe("getVisibleTrees", () => {
    it("collects records of all the visible trees.", () => {
        expect(getVisibleTrees(trees)).toStrictEqual(visibleTrees);
    });
});
