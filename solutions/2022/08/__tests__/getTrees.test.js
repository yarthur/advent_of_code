import { describe, it, expect } from "vitest";
import getTrees from "../getTrees.js";
import { input, trees } from "./testInput.js";

describe("getTrees", () => {
    it("parses the input into a map of trees.", () => {
        expect(getTrees(input)).toStrictEqual(trees);
    });
});
