/**
 * @function calculateScenicValues
 * @summary Calculate the scenic value of a single position in the forest.
 * @memberof 2022.08
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @returns {Map<string, 2022.08.ScenicValue>} - A Map of the scenic values of each location within the forest.
 */
export default (forest) => {
    return forest.reduce((scenicValues, row, y) => {
        return row.reduce((sV, tree, x) => {
            const north = getNorthernValues(forest, tree, { x, y });
            const east = getEasternValues(forest, tree, { x, y });
            const south = getSouthernValues(forest, tree, { x, y });
            const west = getWesternValues(forest, tree, { x, y });

            sV.set([x, y].join(","), {
                north,
                east,
                south,
                west,
                total: getTotalValue([north, east, south, west]),
            });

            return sV;
        }, scenicValues);
    }, new Map());
};

/**
 * @function getNorthernValues
 * @summary Calculate the scenic value looking north from the location.
 * @memberof 2022.08
 * @private
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @param {number} ceiling - The tallest tree so far.
 * @param {{x: number, y: number}} coordinates - The X/Y coordinates currently being evaluated.
 * @returns {number} The scenic value looking north from the location.
 */
const getNorthernValues = (forest, ceiling, { x, y }) => {
    let tallest = -1;
    let visibleTrees = 0;

    for (let next = y - 1; next >= 0 && tallest < ceiling; next -= 1) {
        let nextHeight = forest[next][x];

        visibleTrees += 1;

        if (nextHeight >= tallest) {
            tallest = nextHeight;
        }
    }

    return visibleTrees;
};

/**
 * @function getEasternValues
 * @summary Calculate the scenic value looking east from the location.
 * @memberof 2022.08
 * @private
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @param {number} ceiling - The tallest tree so far.
 * @param {{x: number, y: number}} coordinates - The X/Y coordinates currently being evaluated.
 * @returns {number} The scenic value looking east from the location.
 */
const getEasternValues = (forest, ceiling, { x, y }) => {
    let tallest = -1;
    let visibleTrees = 0;

    for (
        let next = x + 1;
        next < forest[y].length && tallest < ceiling;
        next += 1
    ) {
        let nextHeight = forest[y][next];

        visibleTrees += 1;

        if (nextHeight >= tallest) {
            tallest = nextHeight;
        }
    }

    return visibleTrees;
};

/**
 * @function getSoutherValues
 * @summary Calculate the scenic value looking south from the location.
 * @memberof 2022.08
 * @private
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @param {number} ceiling - The tallest tree so far.
 * @param {{x: number, y: number}} coordinates - The X/Y coordinates currently being evaluated.
 * @returns {number} The scenic value looking south from the location.
 */
const getSouthernValues = (forest, ceiling, { x, y }) => {
    let tallest = -1;
    let visibleTrees = 0;

    for (
        let next = y + 1;
        next < forest.length && tallest < ceiling;
        next += 1
    ) {
        let nextHeight = forest[next][x];

        visibleTrees += 1;

        if (nextHeight >= tallest) {
            tallest = nextHeight;
        }
    }

    return visibleTrees;
};

/**
 * @function getWesternValues
 * @summary Calculate the scenic value looking west from the location.
 * @memberof 2022.08
 * @private
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @param {number} ceiling - The tallest tree so far.
 * @param {{x: number, y: number}} coordinates - The X/Y coordinates currently being evaluated.
 * @returns {number} The scenic value looking west from the location.
 */
const getWesternValues = (forest, ceiling, { x, y }) => {
    let tallest = -1;
    let visibleTrees = 0;

    for (let next = x - 1; next >= 0 && tallest < ceiling; next -= 1) {
        let nextHeight = forest[y][next];

        visibleTrees += 1;

        if (nextHeight >= tallest) {
            tallest = nextHeight;
        }
    }

    return visibleTrees;
};

/**
 * @function getTotalValue
 * @summary Calculate the total combined scenic value for the location.
 * @memberof 2022.08
 * @private
 * @param {number[]} directions - The scenic values for each cardinal direction.
 * @returns {number} The total combined scenic value for the location.
 */
const getTotalValue = (directions) => {
    return directions.reduce((val, direction) => {
        return val * direction;
    }, 1);
};
