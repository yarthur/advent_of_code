/**
 * @typedef { Array<Array<number>> } Forest
 * @summary A representation of trees in a 2-dimmensional map of a forest.
 * @memberof 2022.08
 */

/**
 * @typedef { object } ScenicValue
 * @summary The scenic value of a given location within a forest.
 * @memberof 2022.08
 * @property { number } north - The scenic value looking north from the location.
 * @property { number } east - The scenic value looking east from the location.
 * @property { number } south - The scenic value looking south from the location.
 * @property { number } west - The scenic value looking west from the location.
 * @property { number } total - The total combined scenic value for the location.
 */
