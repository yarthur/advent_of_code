/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 8](https://adventofcode.com/2022/08/).
 * @namespace 08
 * @summary Treetop Tree House
 * @memberof 2022
 */

import getTrees from "./getTrees.js";
import getVisibleTrees from "./getVisibleTrees.js";
import calculateScenicValues from "./calculateScenicValues.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.08
 * @param { string } input - The input string.
 * @returns { number } How many trees are visible from outside the grid.
 */
export const part1 = (input) => {
    const forest = getTrees(input);

    const visibleTrees = getVisibleTrees(forest);

    return visibleTrees.size;
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.08
 * @param { string } input - The input string.
 * @returns { number } The highest scenic score possible for any tree
 */
export const part2 = (input) => {
    const forest = getTrees(input);
    const scenicValues = calculateScenicValues(forest);

    return Array.from(scenicValues.values()).reduce((mostest, { total }) => {
        return Math.max(mostest, total);
    }, 0);
};
