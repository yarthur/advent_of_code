/**
 * @function getVisibleTrees
 * @summary Calculates how many trees are visible from outside the forest.
 * @memberof 2022.08
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @returns { Set<string> } A set of the number of visible tress for each row/column.
 */
export default (forest) => {
    return new Set([
        ...mapTallestTrees(forest),
        ...mapTallestTrees(forest, true),
    ]);
};

/**
 * @function mapTallestTrees
 * @summary Finds the tallest tress for each row/column.
 * @memberof 2022.08
 * @private
 * @param { 2022.08.Forest } forest - A representation of trees in a 2-dimmensional map of the forest.
 * @param { boolean } reversed - Flag to determine if we're checking the top/left sides or bottom/right sides.
 * @returns { string[] } The coordinates for all visible trees.
 */
const mapTallestTrees = (forest, reversed = false) => {
    const rows = reversed ? forest.reverse() : forest;

    const ceilings = {
        x: reversed ? rows[0].length - 1 : null,
        y: reversed ? rows.length - 1 : null,
    };

    const runningTallest = {
        x: new Array(rows[0].length).fill(-1),
        y: new Array(rows.length).fill(-1),
    };

    return rows.reduce(
        /**
         * Going row-by-row (and column-by-column for
         * the row), find the tallest trees.
         * @param {string[]} rowTallest - The tallest trees per row
         * @param {number[]} row - The row we're currently focusing on.
         * @param {number} y - The index/"name" of the row we're currently focusing on.
         * @returns {string[]} All the tallest trees found.
         */
        (rowTallest, row, y) => {
            const trees = reversed ? row.reverse() : row;

            return trees.reduce(
                /**
                 * Going column-by column, finding the tallest trees.
                 * @param {string[]} treeTallest - The tallest trees found so far.
                 * @param {number} tree - The tree we're currently looking at.
                 * @param {number} x - The index/"name" of the column we're currently focusing on.
                 * @returns {string[]} All the tallest trees found.
                 */
                (treeTallest, tree, x) => {
                    // reversed ? South : North
                    if (tree > runningTallest.x[x]) {
                        runningTallest.x[x] = tree;

                        treeTallest.push(
                            getCoordinates([x, y], ceilings).join(",")
                        );
                    }

                    // reversed ? East : West
                    if (tree > runningTallest.y[y]) {
                        runningTallest.y[y] = tree;
                        treeTallest.push(
                            getCoordinates([x, y], ceilings).join(",")
                        );
                    }

                    return treeTallest;
                },
                rowTallest
            );
        },
        []
    );
};

/**
 * @function getCoordinates
 * @summary Retrieve the coordinates requested.
 * @memberof 2022.08
 * @private
 * @param { [x: number, y: number] } coordinates - The raw coordinate received.
 * @param { {x: number | null, y: number | null} } ceilings - If a coordinate is "reversed", the supplied value will be the amount we need to "reverse" it.
 * @returns { Array<number> } The X/Y coordinate requested.
 */
const getCoordinates = ([x, y], ceilings) => {
    return [
        ceilings.x === null ? x : ceilings.x - x,
        ceilings.y === null ? y : ceilings.y - y,
    ];
};
