import { describe, it, expect } from "vitest";
import getProgram from "../getProgram.js";

const input = `noop
addx 3
addx -5`;

const program = [0, 1, 1, 1, 4, 4, -1];

describe("getProgram", () => {
    it("builds a record of registers based on the program input.", () => {
        expect(getProgram(input)).toStrictEqual(program);
    });
});
