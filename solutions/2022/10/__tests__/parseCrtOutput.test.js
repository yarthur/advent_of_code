import { describe, it, expect } from "vitest";
import parseCrtOutput from "../parseCrtOutput.js";
import { program, crtOutput } from "./testInput.js";

describe("parseCrtOutput", () => {
    it("parses the program into the CRT output pattern.", () => {
        expect(parseCrtOutput(program)).toStrictEqual(crtOutput);
    });
});
