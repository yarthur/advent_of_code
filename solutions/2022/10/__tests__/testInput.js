/**
 * @type {string}
 */
export const input = `addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop`;

/**
 * @type {number[]}
 */
export const program = [
    0, 1, 1, 16, 16, 5, 5, 11, 11, 8, 8, 13, 13, 12, 12, 4, 4, 17, 17, 21, 21,
    21, 20, 20, 25, 25, 24, 24, 29, 29, 28, 28, 33, 33, 32, 32, 37, 37, 36, 36,
    1, 1, 2, 2, 26, 26, 7, 7, 8, 8, 24, 24, 13, 13, 13, 13, 34, 34, 19, 19, 19,
    19, 16, 16, 25, 25, 26, 26, 23, 23, 31, 31, 32, 32, 37, 37, 37, 37, 37, 37,
    37, 1, 1, 1, 2, 2, 9, 9, 9, 9, 9, 11, 11, 17, 17, 17, 17, 17, 17, 17, 18,
    18, 18, 18, 25, 25, 26, 26, 26, 13, 13, 26, 26, 33, 33, 33, 34, 34, 1, 1, 1,
    1, 1, 3, 3, 3, 3, 3, 11, 11, 11, 10, 10, 12, 12, 13, 13, 13, 30, 30, 21, 21,
    22, 22, 23, 23, 20, 20, 31, 31, 31, 31, 32, 32, 32, 33, 33, 33, 33, 20, 20,
    1, 1, 2, 2, 5, 5, 31, 31, 1, 1, 13, 13, 12, 12, 15, 15, 16, 16, 16, 16, 16,
    7, 7, 25, 25, 26, 26, 28, 28, 28, 28, 37, 37, 37, 37, 37, 36, 36, 38, 38, 1,
    1, 2, 2, 5, 5, 5, 20, 20, -1, -1, 21, 21, 15, 15, 16, 16, 16, 18, 18, 19,
    19, 19, 9, 9, 9, 9, 29, 29, 30, 30, 32, 32, 34, 34, 28, 28, 17, 17, 17, 17,
];

/*
 * @type {string}
 */
export const crtOutput = `##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....`;
