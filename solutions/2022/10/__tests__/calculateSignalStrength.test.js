import { describe, it, expect } from "vitest";
import calculateSignalStrength from "../calculateSignalStrength.js";
import { program } from "./testInput.js";

const strengths = new Map([
    [20, 420],
    [60, 1140],
    [100, 1800],
    [140, 2940],
    [180, 2880],
    [220, 3960],
]);

describe("calculateSignalStrength", () => {
    it("calculates the signal strength during the program cycle.", () => {
        const signalStrengths = program.map(calculateSignalStrength);

        Array.from(strengths.entries).forEach(([cycle, strength]) => {
            expect(signalStrengths[cycle]).toBe(strength);
        });
    });
});
