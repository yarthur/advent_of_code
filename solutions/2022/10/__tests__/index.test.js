import { describe, it, expect } from "vitest";
import { input, crtOutput } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2022, day 10", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(13140);
    });

    it("solves for part 2.", () => {
        expect(part2(input)).toStrictEqual("\n" + crtOutput);
    });
});
