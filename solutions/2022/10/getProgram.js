/**
 * @function getProgram
 * @summary Parses the puzzle's input string into a Program for a Cathod Ray Tube.
 * @memberof 2022.10
 * @param { string } input - The puzzle's input string.
 * @returns { Array<number> } - The exeution steps for the program.
 */
export default (input) => {
    const instructions = input.split("\n");

    const registers = instructions.reduce(
        (program, instruction) => {
            let lastRegisterValue = [...program].pop();

            program.push(lastRegisterValue);

            const registerChange = instruction.match(/-?\d+/);

            if (registerChange !== null) {
                program.push(
                    lastRegisterValue + Number.parseInt(registerChange[0], 10)
                );
            }

            return program;
        },
        [1]
    );

    return [0, ...registers];
};
