/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 10](https://adventofcode.com/2022/10/).
 * @namespace 2022.10
 * @summary Cathode-Ray Tube
 * @memberof 2022
 */

import getProgram from "./getProgram.js";
import calculateSignalStrength from "./calculateSignalStrength.js";
import parseCrtOutput from "./parseCrtOutput.js";

/**
 * @function part1
 * @summary Solves part 1 of th day's puzzle.
 * @memberof 2022.10
 * @param { string } input - The puzzle's input string.
 * @returns { number } The sum of the signal strengths of cycles 20, 60, 100, 140, 180, and 220.
 */
export const part1 = (input) => {
    const program = getProgram(input);

    const signalValues = program.map(calculateSignalStrength);

    return (
        signalValues[20] +
        signalValues[60] +
        signalValues[100] +
        signalValues[140] +
        signalValues[180] +
        signalValues[220]
    );
};

/**
 * @function part2
 * @summary Solves part 1 of th day's puzzle.
 * @memberof 2022.10
 * @param { string } input - The puzzle's input string.
 * @returns {string} The resulting CRT image.
 */
export const part2 = (input) => {
    const program = getProgram(input);

    // The newline ensures the output
    // displays properly. Otherwise, the
    // `2022 day 10, part 2:` would mess up
    // the top line
    return "\n" + parseCrtOutput(program);
};
