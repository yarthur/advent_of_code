/**
 * @function parseCrtOutput
 * @summary Parses the CRT output based on the program instructions.
 * @memberof 2022.10
 * @param { Array<number> } program - The instructions for the CRT.
 * @returns {string} The resulting output.
 */
export default (program) => {
    const screen = new Array(6).fill("");
    let column = 0;
    let row = 0;

    // Get rid of "cycle 0"
    program.shift();

    program.forEach((value) => {
        if (row < 6) {
            const activeSignal = value <= column + 1 && value >= column - 1;
            screen[row] += activeSignal ? "#" : ".";

            column += 1;

            if (column >= 40) {
                column = 0;
                row += 1;
            }
        }
    });

    return screen.join("\n");
};
