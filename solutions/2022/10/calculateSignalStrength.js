/**
 * @function calculateSignalStrength
 * @summary Calculates the signal strength for a given cycle of a CRT image.
 * @memberof 2022.10
 * @param { number } register - The given cycle's register value.
 * @param { number } cycle - The given cycle's index within the program.
 * @returns { number } The Signal Strength for the given cycle.
 */
export default (register, cycle) => {
    return register * cycle;
};
