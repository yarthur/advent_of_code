import { describe, it, expect } from "vitest";
import getPairs from "../getPairs.js";
import { input, pairs } from "./testInput.js";

describe("getPairs", () => {
    it("parses the input into pairs of ranges.", () => {
        expect(getPairs(input)).toStrictEqual(pairs);
    });
});
