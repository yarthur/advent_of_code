import { describe, it, expect } from "vitest";
import isOverlapping from "../isOverlapping.js";
import { pairs, overlappingPairs } from "./testInput.js";

describe("isOverlapping", () => {
    it("identifies pairs that overlap at least one section.", () => {
        expect(pairs.filter(isOverlapping)).toStrictEqual(overlappingPairs);
    });
});
