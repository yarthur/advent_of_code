/**
 * @typedef {import("../types").Pair} Pair
 */

/**
 * @type {string}
 */
export const input = `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`;

/**
 * @type {Pair[]}
 */
export const pairs = [
    [
        [2, 4],
        [6, 8],
    ],
    [
        [2, 3],
        [4, 5],
    ],
    [
        [5, 7],
        [7, 9],
    ],
    [
        [2, 8],
        [3, 7],
    ],
    [
        [6, 6],
        [4, 6],
    ],
    [
        [2, 6],
        [4, 8],
    ],
];

/**
 * @type {Pair[]}
 */
export const fullyContainedPairs = [
    [
        [2, 8],
        [3, 7],
    ],
    [
        [6, 6],
        [4, 6],
    ],
];

/**
 * @type {Pair[]}
 */
export const overlappingPairs = [
    [
        [5, 7],
        [7, 9],
    ],
    [
        [2, 8],
        [3, 7],
    ],
    [
        [6, 6],
        [4, 6],
    ],
    [
        [2, 6],
        [4, 8],
    ],
];
