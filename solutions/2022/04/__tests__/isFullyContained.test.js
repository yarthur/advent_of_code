import { describe, it, expect } from "vitest";
import isFullyContained from "../isFullyContained.js";
import { pairs, fullyContainedPairs } from "./testInput.js";

describe("isFullyContained", () => {
    it("checks to see if one range in a pair is fully contained wihtin the other.", () => {
        expect(pairs.filter(isFullyContained)).toStrictEqual(
            fullyContainedPairs
        );
    });
});
