/**
 * @function isFullyContained
 * @summary Checks to see if one Range within the Pair is fully contained by the other.
 * @memberof 2022.04
 * @param { Pair } pair - The Pair of assigned Ranges to compare.
 * @returns { boolean } The result of the cmparison.
 */
export default ([range1, range2]) => {
    // range 1 is contained within range2
    if (range1[0] >= range2[0] && range1[1] <= range2[1]) {
        return true;
    }

    // range 2 is contained within range2
    if (range2[0] >= range1[0] && range2[1] <= range1[1]) {
        return true;
    }

    return false;
};
