/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 4](https://adventofcode.com/2022/04/).
 * @namespace 04
 * @summary Camp Cleanup 
 * @memberof 2022
 */
import getPairs from "./getPairs.js";
import isFullyContained from "./isFullyContained.js";
import isOverlapping from "./isOverlapping.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.04
 * @param {string} input - The puzzle's input string.
 * @returns {number} The number of assignment Pairs where one Range fully contain the other.
 */
export const part1 = (input) => {
    const pairs = getPairs(input);
    const fullyContained = pairs.filter(isFullyContained);
    return fullyContained.length;
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.04
 * @param {string} input - The puzzle's input string.
 * @returns {number} The number of assignment Pairs where one Range overlaps the other.
 */
export const part2 = (input) => {
    const pairs = getPairs(input);
    const overlappingPairs = pairs.filter(isOverlapping);
    return overlappingPairs.length;
};
