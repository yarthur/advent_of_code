/**
 * @typedef { Array< number, number > } Range
 * @summary A Range of Sections assigned to an Elf.
 * @memberof 2022.04
 */

/**
 * @typedef { Array< 2022.04.Range, 2022.04.Range > } Pair
 * @summary A par of Elves' Assigned Ranges
 * @memberof 2022.04
 */
