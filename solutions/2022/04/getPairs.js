/**
 * @function getPairs
 * @summary Parses the puzzle's input string into Pairs of assigned Ranges.
 * @memberof 2022.04
 * @param {string} input - The puzzle's input string.
 * @returns { Array< 2022.04.Pair > } All of the assigned Pairs.
 */
export default (input) => {
    const roughPairs = input.split("\n");

    return roughPairs.map(
        (pair) => {
            const splitPair = pair.split(",");

            return splitPair.map(
                (elf) => {
                    const range = elf.split("-");

                    return range.map(
                        (coordinate) => {
                            return Number.parseInt(coordinate, 10);
                        }
                    );
                }
            );
        }
    );
};
