/**
 * @function isOverlapping
 * @summary Checks to see if the two Ranges within a Pair overlap at all.
 * @memberof 2022.04
 * @param { 2022.04.Pair } pair - The Pair of assigned Ranges to compare.
 * @returns { boolean } The result of the comparison.
 */
export default ([range1, range2]) => {
    // range 1 is contained within range2
    if (range1[0] >= range2[0] && range1[0] <= range2[1]) {
        return true;
    }

    // range 2 is contained within range2
    if (range2[0] >= range1[0] && range2[0] <= range1[1]) {
        return true;
    }

    return false;
};
