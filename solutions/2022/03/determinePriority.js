/**
 * The common modifier to base priority values off character codes.
 * @constant { object } priorityModifier
 * @memberof 2022.03
 * @property { number } lower - The modifier for lower-case characters.
 * @property { number } upper - The modifier for upper-case characters.
 */
const priorityModifier = {
    lower: "a".charCodeAt(0) - 1,
    upper: "A".charCodeAt(0) - 27,
};

/**
 * Assigns a Priority value for an Item.
 * @function determinePriority
 * @memberof 2022.03
 * @param {string} item - The Item having an priority value assigned.
 * @returns {number} The priority value of the item.
 */
export default (item) => {
    const itemCode = item.charCodeAt(0);

    if (item.toUpperCase() === item) {
        return itemCode - priorityModifier.upper;
    }

    return itemCode - priorityModifier.lower;
};
