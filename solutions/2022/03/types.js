/**
 * @typedef { Array } Rucksack
 * @summary The contents within both compartments of an Elf's Rucksack.
 * @memberof 2022.03
 * @property { Array<string> } 0 - The first compartment of the Rucksack.
 * @property { Array<string> } 1 - The second compartment of the Rucksack.
 */

/**
 * @typedef { Array< 2022.03.Rucksack, 2022.03.Rucksack, 2022.03.Rucksack > } BadgeGroup
 * @summary A group of Elves (and their Rucksacks) with a common badge.
 * @memberof 2022.03
 */
