/**
 * Identifies the Badge Item within a small group of Elves and their Rucksacks.
 * @function getBadgeItem
 * @memberof 2022.03
 * @param { 2022.03.BadgeGroup } group - The Group of Elves (and their Rucksacks) being examined.
 * @returns {string} The common Badge Item amongst the Elves' Rucksacks.
 */
export default (group) => {
    const [leader, ...followers] = group.map((rucksack) => {
        return [...rucksack[0], ...rucksack[1]];
    });

    const badgeItem = leader.filter(
        (item) => {
            return followers.reduce(
                (isItem, follower) => {
                    if (isItem === true) {
                        isItem = follower.includes(item);
                    }

                    return isItem;
                },
                true
            );
        }
    );

    return badgeItem[0];
};
