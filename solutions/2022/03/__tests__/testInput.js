/**
 * @typedef {import("../types").Rucksack} Rucksack
 */

export const input = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`;

/**
 * @type {Rucksack[]}
 */
export const rucksacks = [
    [
        ["v", "J", "r", "w", "p", "W", "t", "w", "J", "g", "W", "r"],
        ["h", "c", "s", "F", "M", "M", "f", "F", "F", "h", "F", "p"],
    ],
    [
        [
            "j",
            "q",
            "H",
            "R",
            "N",
            "q",
            "R",
            "j",
            "q",
            "z",
            "j",
            "G",
            "D",
            "L",
            "G",
            "L",
        ],
        [
            "r",
            "s",
            "F",
            "M",
            "f",
            "F",
            "Z",
            "S",
            "r",
            "L",
            "r",
            "F",
            "Z",
            "s",
            "S",
            "L",
        ],
    ],
    [
        ["P", "m", "m", "d", "z", "q", "P", "r", "V"],
        ["v", "P", "w", "w", "T", "W", "B", "w", "g"],
    ],
    [
        [
            "w",
            "M",
            "q",
            "v",
            "L",
            "M",
            "Z",
            "H",
            "h",
            "H",
            "M",
            "v",
            "w",
            "L",
            "H",
        ],
        [
            "j",
            "b",
            "v",
            "c",
            "j",
            "n",
            "n",
            "S",
            "B",
            "n",
            "v",
            "T",
            "Q",
            "F",
            "n",
        ],
    ],
    [
        ["t", "t", "g", "J", "t", "R", "G", "J"],
        ["Q", "c", "t", "T", "Z", "t", "Z", "T"],
    ],
    [
        ["C", "r", "Z", "s", "J", "s", "P", "P", "Z", "s", "G", "z"],
        ["w", "w", "s", "L", "w", "L", "m", "p", "w", "M", "D", "w"],
    ],
];

/**
 * @type {string[]}
 */
export const misplacedItems = ["p", "L", "P", "v", "t", "s"];

/**
 * @type {number[]}
 */
export const itemPriorities = [16, 38, 42, 22, 20, 19];

/**
 * @type {Array<Rucksack[]>}
 */
export const groups = [
    [
        [
            ["v", "J", "r", "w", "p", "W", "t", "w", "J", "g", "W", "r"],
            ["h", "c", "s", "F", "M", "M", "f", "F", "F", "h", "F", "p"],
        ],
        [
            [
                "j",
                "q",
                "H",
                "R",
                "N",
                "q",
                "R",
                "j",
                "q",
                "z",
                "j",
                "G",
                "D",
                "L",
                "G",
                "L",
            ],
            [
                "r",
                "s",
                "F",
                "M",
                "f",
                "F",
                "Z",
                "S",
                "r",
                "L",
                "r",
                "F",
                "Z",
                "s",
                "S",
                "L",
            ],
        ],
        [
            ["P", "m", "m", "d", "z", "q", "P", "r", "V"],
            ["v", "P", "w", "w", "T", "W", "B", "w", "g"],
        ],
    ],
    [
        [
            [
                "w",
                "M",
                "q",
                "v",
                "L",
                "M",
                "Z",
                "H",
                "h",
                "H",
                "M",
                "v",
                "w",
                "L",
                "H",
            ],
            [
                "j",
                "b",
                "v",
                "c",
                "j",
                "n",
                "n",
                "S",
                "B",
                "n",
                "v",
                "T",
                "Q",
                "F",
                "n",
            ],
        ],
        [
            ["t", "t", "g", "J", "t", "R", "G", "J"],
            ["Q", "c", "t", "T", "Z", "t", "Z", "T"],
        ],
        [
            ["C", "r", "Z", "s", "J", "s", "P", "P", "Z", "s", "G", "z"],
            ["w", "w", "s", "L", "w", "L", "m", "p", "w", "M", "D", "w"],
        ],
    ],
];

/**
 * @type {string[]}
 */
export const badgeItems = ["r", "Z"];

/**
 * @type {number[]}
 */
export const badgePriorities = [18, 52];
