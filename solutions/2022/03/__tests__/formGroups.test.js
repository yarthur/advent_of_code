import { describe, it, expect } from "vitest";
import formGroups from "../formGroups.js";
import { rucksacks, groups } from "./testInput.js";

describe("formGroups", () => {
    it("forms groups of elves' rucksacks out of the master list.", () => {
        expect(formGroups(rucksacks, 3)).toStrictEqual(groups);
    });
});
