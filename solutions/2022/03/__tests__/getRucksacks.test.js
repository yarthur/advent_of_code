import { describe, it, expect } from "vitest";
import getRucksacks from "../getRucksacks.js";
import { input, rucksacks } from "./testInput.js";

describe("getRucksacks", () => {
    it("parses input into rucksack data.", () => {
        expect(getRucksacks(input)).toStrictEqual(rucksacks);
    });
});
