import { describe, it, expect } from "vitest";
import getBadgeItem from "../getBadgeItem.js";
import { groups, badgeItems } from "./testInput.js";

describe("getBadgeItem", () => {
    it("gets the common badge item amongst the group.", () => {
        groups.forEach((group, index) => {
            expect(getBadgeItem(group)).toBe(badgeItems[index]);
        });
    });
});
