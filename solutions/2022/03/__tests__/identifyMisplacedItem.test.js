import { describe, it, expect } from "vitest";
import identifyMisplacedItem from "../identifyMisplacedItem.js";
import { rucksacks, misplacedItems } from "./testInput.js";

describe("identifyMisplacedItem", () => {
    it("identifies the misplaced item in each rucksack.", () => {
        rucksacks.forEach((rucksack, index) => {
            expect(identifyMisplacedItem(rucksack)).toBe(misplacedItems[index]);
        });
    });
});
