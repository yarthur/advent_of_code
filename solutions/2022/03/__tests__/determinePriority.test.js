import { describe, it, expect } from "vitest";
import determinePriority from "../determinePriority.js";
import {
    misplacedItems,
    itemPriorities,
    badgeItems,
    badgePriorities,
} from "./testInput.js";

describe("determinePriority", () => {
    it("determines the priority of individual items", () => {
        misplacedItems.forEach((item, index) => {
            expect(determinePriority(item)).toBe(itemPriorities[index]);
        });

        badgeItems.forEach((item, index) => {
            expect(determinePriority(item)).toBe(badgePriorities[index]);
        });
    });
});
