/**
 * Parses the puzzle input string into a collection of Rucksacks.
 * @function getRucksacks
 * @memberof 2022.03
 * @param { string } input - The puzzle input string.
 * @returns { Array< 2022.03.Rucksack > } The individual Rucksacks.
 */
export default (input) => {
    const wholeRs = input.split("\n");

    return wholeRs.map((rs) => {
        const contents = rs.split("");
        const compartmentQty = contents.length / 2;

        return [
            contents.slice(0, compartmentQty),
            contents.slice(compartmentQty),
        ];
    });
};
