/**
 * My solutions for the
 * [2022 edition of Advent of Code, day 3](https://adventofcode.com/2022/03/).
 * @namespace 03
 * @summary Rucksack Reorganization
 * @memberof 2022
 */

import getRucksacks from "./getRucksacks.js";
import identifyMisplacedItem from "./identifyMisplacedItem.js";
import determinePriority from "./determinePriority.js";
import formGroups from "./formGroups.js";
import getBadgeItem from "./getBadgeItem.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2022.03
 * @param {string} input The puzzle's input string.
 * @returns {number} The sum of the priorities of the item types that appear in both compartments of each rucksack
 */
export const part1 = (input) => {
    const rucksacks = getRucksacks(input);
    const misplacedItems = rucksacks.map(identifyMisplacedItem);
    const priorities = misplacedItems.map(determinePriority);

    return priorities.reduce((total, priority) => {
        return total + priority;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2022.03
 * @param {string} input The puzzle's input string.
 * @returns {number} The sum of the priorities of the item types that correspond to the badges of each three-Elf group.
 */
export const part2 = (input) => {
    const rucksacks = getRucksacks(input);
    const groups = formGroups(rucksacks, 3);
    const badgeItems = groups.map(getBadgeItem);
    const priorities = badgeItems.map(determinePriority);
    return priorities.reduce((total, priority) => {
        return total + priority;
    }, 0);
};
