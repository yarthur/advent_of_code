/**
 * Finds the misplaced Item of a Rucksack
 * @function identifyMisplacedItem
 * @memberof 2022.03
 * @param { 2022.03.Rucksack } rucksack - The contents within both compartments of an Elf's Rucksack.
 * @returns {string} The misplaced Item.
 */
export default (rucksack) => {
    const item = rucksack[0].filter((item) => {
        return rucksack[1].includes(item);
    });

    return item[0];
};
