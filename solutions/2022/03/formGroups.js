/**
 * Divides the elves into groups of three.
 * @function formGroups
 * @memberof 2022.03
 * @param { Array< 2022.03.Rucksack > } rucksacks - All the Rucksacks (and by extension, Elves) to group up.
 * @param { number } groupSize - The size of the smaller groups.
 * @returns { Array< 2022.03.BadgeGroup > } All of the small groups.
 */
export default (rucksacks, groupSize) => {
    const groups = [];

    let group = [];

    rucksacks.forEach((rucksack) => {
        group.push(rucksack);

        if (group.length === groupSize) {
            groups.push([...group]);
            group = [];
        }
    });

    return groups;
};
