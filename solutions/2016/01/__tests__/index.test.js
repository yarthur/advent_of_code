import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2016, day 01", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(input);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toBe(false);
    });
});
