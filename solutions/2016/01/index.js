/**
 * My solutions for the
 * [2016 edition of Advent of Code, day 1](https://adventofcode.com/2016/01/).
 * @namespace 01
 * @summary No Time for a Taxicab
 * @memberof 2016
 */

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2016.01
 * @param {string} input - The input string.
 * @returns {string} The input string.
 */
export const part1 = (input) => {
    return input;
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2016.01
 * @param {string} input - The input string.
 * @returns {boolean} Something to test against, really.
 */
export const part2 = (input) => {
    return input === "";
};
