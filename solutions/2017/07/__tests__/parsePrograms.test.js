import { describe, it, expect } from "vitest";
import { inputs, programs } from "./sampleData.js";
import parsePrograms from "../parsePrograms.js";

describe("parsePrograms", () => {
    it("parses a string into an object of objects.", function () {
        expect(parsePrograms(inputs)).toEqual(programs);
    });
});
