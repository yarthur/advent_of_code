import { describe, it, expect } from "vitest";
import { programs, childParents } from "./sampleData.js";
import generateChildParents from "../generateChildParents.js";

describe("generateChildParents", () => {
    it.todo("creates an object, the properties of which detail the parent of each program (if any)", function () {
        expect(generateChildParents(programs)).toEqual(childParents);
    });
});
