import { describe, it, expect } from "vitest";
import { programs } from './sampleData';
import buildTowers from "../buildTowers.js";

const towers = [
    {
        key: "fwft",
        programWeight: 72,
        children: [
            {
                key: "ktlj",
                programWeight: 57,
                children: [],
            },
            {
                key: "cntj",
                programWeight: 57,
                children: [],
            },
            {
                key: "xhth",
                programWeight: 57,
                children: [],
            },
        ],
    },
];

describe("buildTowers", () => {
    it.todo("builds the towers out", function () {
        expect(buildTowers(programs)).toEqual(towers);
    });
});
