import { describe, it, expect } from "vitest";
import tieHash from "../tieHash.js";

describe("tieHash", () => {
    it("generates a sparse hash", () => {
        const list = [0, 1, 2, 3, 4];
        const knotLengths = [3, 4, 1, 5];
        expect(tieHash(list, knotLengths, 0)).toEqual([3, 4, 2, 1, 0]);
    });
});
