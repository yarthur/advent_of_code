import { describe, it, expect } from "vitest";
import generateKnotLengths from "../generateKnotLengths.js";

describe("generateKnotLengths", () => {
    it("generates a sequence of Knot Lengths from the input string", () => {
        expect(generateKnotLengths("3,4,1,5")).toEqual([3, 4, 1, 5]);
    });

    it("generates a sequence of Knot Lengths by treating the input string as ASCII character codes", () => {
        expect(generateKnotLengths("1,2,3", true)).toEqual([49, 44, 50, 44, 51]);
    })
})
