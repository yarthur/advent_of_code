import { describe, it, expect } from "vitest";
import generateList from "../generateList.js";

describe("generateNumberHash", () => {
    it("generates a number list of a given length, starting at 0", () => {
        expect(generateList(5)).toEqual([0, 1, 2, 3, 4]);
    });
});

