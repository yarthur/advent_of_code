import { describe, it, expect } from "vitest";
import generateHashHexValue from "../generateHashHexValue.js";

describe("generateHashHexValue", () => {
    it("generates a hexidecimal string from a decimal hash array", () => {
        expect(generateHashHexValue([64, 7, 255])).toEqual("4007ff");
    });
})
