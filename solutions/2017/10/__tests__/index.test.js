import { describe, afterEach, vi, it, expect } from "vitest";
import { part1, part2 } from "../index.js";

const OVERRIDE_NUMBER_HASH = "OVERRIDE_NUMBER_HASH";

vi.mock("../generateList.js", async () => {
    const original = await vi.importActual("../generateList.js");

    return {
        default: vi.fn((length) => {
            const overrideNumberHash = import.meta.env.OVERRIDE_NUMBER_HASH || false;

            if (overrideNumberHash) {
                return original.default(5);
            }

            return original.default(length);
        }),
    };
});

describe("2017, day 10", async () => {
    afterEach(() => {
        vi.unstubAllEnvs();
    });

    it("solves for part 1", async () => {
        vi.stubEnv(OVERRIDE_NUMBER_HASH, true);
        expect(part1("3,4,1,5")).toEqual(12);
    });

    it("solves for part2", async () => {
        expect(part2("")).toEqual("a2582a3a0e66e6e86e3812dcb672a272");

        expect(part2("AoC 2017")).toEqual("33efeb34ea91902bb2f59c9920caa6cd");
        expect(part2("1,2,3")).toEqual("3efbe78a8d82f29979031a4aa0b16a9d");
        expect(part2("1,2,4")).toEqual("63960835bcdc130f0b66d7ff4f6a5a8e");
    });
});
