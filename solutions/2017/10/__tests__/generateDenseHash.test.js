import { describe, it, expect } from "vitest";
import generateDenseHash from "../generateDenseHash.js";

describe("generateDenseHash", () => {
    it("generates a Dense Hash from a Sparse Hash", () => {
        const sparseHash = [65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22]
        expect(generateDenseHash(sparseHash, 16)).toEqual([64]);
    })
})
