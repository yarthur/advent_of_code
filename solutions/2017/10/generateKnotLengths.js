/**
 * @function generateKnotLengths
 * @summary Generates a sequence of Knot Lengths from the puzzle's input string.
 * @param { string } input - The puzzle's input string.
 * @param { boolean } [isAscii = false] - A flag to indicate whether or not the input should be treated as ASCII characters.
 * @returns { Array<number> } The list of knot lengths.
 */
export default (input, isAscii = false) => {
    if (isAscii) {
        // We include the commas in ASCII mode.
        return input.split("")
            .map((char) => {
                return char.charCodeAt(0);
            });
    }

    return input.split(",")
        .map(function (size) {
            return parseInt(size, 10);
        });
};
