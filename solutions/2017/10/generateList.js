/**
 * @function generateList
 * @summary Generates a number list of a given length, starting at 0.
 * @memberof 2017.10
 * @param { number } listLength - The desired length of the list.
 * @returns { Array<number> } The numeric list.
 */
export default (listLength) => {
    return [...Array(listLength).keys()];
};
