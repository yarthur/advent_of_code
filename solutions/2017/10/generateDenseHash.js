/**
 * @typedef { Array<number> } DenseHash
 * @summary A more compressed has generated from a Sparse Hash.
 * @memberof 2017.10
 */

/**
 * @function generateDenseHash
 * @summary Generates a Dense Hash from a Sparse Hash.
 * @memberof 2017.10
 * @param { Array<number> } sparseHash - The Sparse Hash to be converted to a Dense Hash.
 * @param { number } density - The number of Sparse Hash values to be compressed into one Dense Hash value.
 * @returns { 2017.10.DenseHash } The resulting DenseHash value.
 */
export default (sparseHash, density) => {
    const denseHash = [];
    let currentBlock = [];

    sparseHash.forEach((value) => {
        currentBlock.push(value);

        if (currentBlock.length === density) {
            const startingValue = currentBlock.shift();
            const denseValue = currentBlock.reduce((denseValue, currentValue) => {
                return denseValue ^ currentValue;
            }, startingValue);

            denseHash.push(denseValue);
            currentBlock = [];
        }
    });

    return denseHash;
};
