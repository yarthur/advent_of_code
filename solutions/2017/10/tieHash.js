var findPosition = function (currentPosition, skipDistance, stringLength) {
    currentPosition += skipDistance;

    if (currentPosition >= stringLength) {
        currentPosition -= stringLength;
    }

    return currentPosition;
};

var tieKnot = function (string, knotStart, knotLength, stringLength) {
    var knot = new Array(knotLength);

    for (let knotPosition = 0; knotPosition < knotLength; knotPosition += 1) {
        let stringPosition = findPosition(
            knotStart,
            knotPosition,
            stringLength
        );

        knot[knotPosition] = string[stringPosition];
    }

    knot.reverse();

    knot.map(function (value, knotPosition) {
        var stringPosition = findPosition(
            knotStart,
            knotPosition,
            stringLength
        );

        string[stringPosition] = value;
    });

    return string;
};

/**
 * @function tieHash
 * @summary Generates a Sparse Hash.
 * @memberof 2017.10
 * @param { Array<number> } hash - The hash getting tied.
 * @param { Array<number> } knotLengths - The list of lengths between "knots".
 * @param { number } currentPosition - The current position being tied.
 * @param { number } [rounds = 1] - The number of rounds of tieing to be completed.
 * @returns { Array<number> } The resulting Sparse Hash.
 */
export default (hash, knotLengths, currentPosition, rounds = 1) => {
    var hashLength = hash.length;

    var skipSize = 0;
    for(let round = 0; round < rounds; round += 1) {
        knotLengths.forEach(function (knotLength) {
            var skipDistance = knotLength + skipSize;

            hash = tieKnot(hash, currentPosition, knotLength, hashLength);

            currentPosition = findPosition(
                currentPosition,
                skipDistance % hashLength,
                hashLength
            );

            skipSize += 1;
        });
    }

    return hash;
};
