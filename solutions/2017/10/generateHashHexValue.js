/**
 * @function generateHashHexValue
 * @summary Generates a hexidecimal string from a decimal hash array.
 * @memberof 2017.10
 * @param { Array<number> } hash - The decimal hash array being converted.
 * @returns { string } The hexidecimal string form of the hash.
 */
export default (hash) => {
    return hash.reduce((hex, decimal) => {
        return hex + decimal.toString(16).padStart(2, "0");
    }, "");
};
