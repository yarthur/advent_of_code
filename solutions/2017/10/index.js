/**
 * My solutions for the
 * [2017 edition of Advent of Code, day 10](https://adventofcode.com/2017/10/).
 * @namespace 10
 * @summary Knot Hash
 * @memberof 2017
 */

import generateList from "./generateList.js";
import generateKnotLengths from "./generateKnotLengths.js";
import tieHash from "./tieHash.js";
import generateDenseHash from "./generateDenseHash.js";
import generateHashHexValue from "./generateHashHexValue.js";

/**
 * @function part1
 * @summary Solves for part 1 of the day's puzzle.
 * @memberof 2017.10
 * @param { string } input - The puzzle's input string.
 * @returns { number } The result of multiplying the first two numbers in the list.
 */
export const part1 = function (input) {
    const list = generateList(256);
    const knotLengths = generateKnotLengths(input);
    const knottedHash = tieHash(list, knotLengths, 0);

    return knottedHash[0] * knottedHash[1];
};

/**
 * @function part2
 * @summary Solves for part 2 of the day's puzzle.
 * @memberof 2017.10
 * @param { string } input - The puzzle's input string.
 * @returns { number } The result of multiplying the first two numbers in the list.
 */
export const part2 = (input) => {
    const list = generateList(256);
    const knotLengths = [...generateKnotLengths(input, true), 17, 31, 73, 47, 23];
    const sparseHash = tieHash(list, knotLengths, 0, 64);
    const denseHash = generateDenseHash(sparseHash, 16);

    return generateHashHexValue(denseHash);
};
