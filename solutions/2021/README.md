# 2021 — ⭐️: 14

My solutions for the
[2021 edition of Advent of Code](https://adventofcode.com/2021/)

## Solutions

-   [01](./01/index.js): ⭐️⭐️
-   [02](./02/index.js): ⭐️⭐️
-   [03](./03/index.js): ⭐️⭐️
-   [04](./04/index.js): ⭐️⭐️
-   [05](./05/index.js): ⭐️⭐️
-   [06](./06/index.js): ⭐️⭐️
-   [07](./07/index.js): ⭐️⭐️
