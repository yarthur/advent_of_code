import bitwiseSurvey from "./bitwiseSurvey.js";

/**
 * Find the most common bit value from the survey of a position.
 * @private
 * @param { Map<string, number> } result - The results of the position survey.
 * @returns { string } - The most common bit value (or "1" in the event of a tie).
 */
const findMostCommon = (result) => {
    if (result.get("0") === result.get("1")) {
        return "1";
    }

    const sortedResult = [...result.entries()].sort((a, b) => {
        return b[1] - a[1];
    });

    return sortedResult[0][0];
};

/**
 * Finds the Rating of a life support system from the submarine's Diagnostic Report.
 * @private
 * @param { string[] } report - The submarine's Diagnostic Report.
 * @param { boolean } [leastCommon = false] - Flag to indicate if we're looking for the least common value in a position.
 * @returns { number } The rating for the given system.
 */
const computeRating = (report, leastCommon = false) => {
    const length = report.reduce((ln, reading) => {
        return reading.length > ln ? reading.length : ln;
    }, 0);

    let oxygen = [...report];

    for (let index = 0; index < length; index += 1) {
        const survey = bitwiseSurvey(oxygen);
        const mostCommon = findMostCommon(survey[index]);

        oxygen = oxygen.filter((reading) => {
            if (leastCommon) {
                return reading.charAt(index) !== mostCommon;
            }

            return reading.charAt(index) === mostCommon;
        });

        if (oxygen.length <= 1) {
            index = length;
        }
    }

    return Number.parseInt(oxygen[0], 2);
};

/**
 * Find the Oxygen Generator Rating from the submarine's Diagnostic Report.
 * @function findO2GeneratorRating
 * @memberof 2021.03
 * @param { Array<string> } report - The submarine's Diagnostic Report.
 * @returns { number } The Oxygen Generator Rating.
 */
export const findO2GeneratorRating = (report) => {
    return computeRating(report, false);
};

/**
 * Find the Carbon Dioxide Scrubber Rating from the submarine's Diagnostic Report.
 * @function findCO2ScrubberRating 
 * @memberof 2021.03
 * @param { Array<string> } report - The submarine's Diagnostic Report.
 * @returns { number } The Carbon Dioxide Scrubber Rating.
 */
export const findCO2ScrubberRating = (report) => {
    return computeRating(report, true);
};
