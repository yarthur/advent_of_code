/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 3](https://adventofcode.com/2021/03/).
 * @namespace 03
 * @summary Binary Diagnostic
 * @memberof 2021
 */

import parseReport from "./parseReport.js";
import computePowerConsumption from "./computePowerConsumption.js";
import { findO2GeneratorRating, findCO2ScrubberRating } from "./findLifeSupportRatings.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2021.03
 * @param { string } input - The input string.
 * @returns { number } The power consumption of the submarine.
 */
export const part1 = (input) => {
    const report = parseReport(input);

    return computePowerConsumption(report);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2021.03
 * @param { string } input - The input string.
 * @returns { number } The life support rating of the submarine.
 */
export const part2 = (input) => {
    const report = parseReport(input);

    const oxygenGeneratorRating = findO2GeneratorRating(report);
    const carbonDioxideScrubberRating = findCO2ScrubberRating(report);
    return oxygenGeneratorRating * carbonDioxideScrubberRating;
};
