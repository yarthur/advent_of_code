/**
 * Maps the number of times each bit occurs in that position.
 * @function bitwiseSurvey
 * @memberof 2021.03
 * @param { Array<string> } report - The submarine's diagnostic report.
 * @returns { Array<Map<string, number>> } A Map of each position with the number of times each bit was in that position.
 */
export default (report) => {
    return report.reduce(
        /**
         * Updates the bit count for the position.
         * @param {Map<string, number>[]} survey - The survey of bit occurences by position.
         * @param {string} reading - The current report reading being analyzed.
         * @returns {Map<string, number>[]} - The updated survey fo bit occurences by position.
         */
        (survey, reading) => {
            return reading.split("").reduce((s, bit, index) => {
                if (s[index] === undefined) {
                    s.push(
                        new Map([
                            ["0", 0],
                            ["1", 0],
                        ])
                    );
                }

                s[index].set(bit, s[index].get(bit) + 1);

                return s;
            }, survey);
        },
        []
    );
};
