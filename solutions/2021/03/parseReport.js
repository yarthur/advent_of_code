/**
 * Parses the puzzle input into a diagnostic report.
 * @function parseReport
 * @memberof 2021.03
 * @param { string } input - The puzzle's input string
 * @returns { Array<string> } The diagnostic report of the submarine.
 */
export default (input) => {
    return input.split("\n");
};
