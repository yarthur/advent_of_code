import { describe, it, expect } from "vitest";
import { report } from "./testInput.js";
import computePowerConsumption from "../computePowerConsumption.js";

describe("computePowerConsumption", () => {
    it("computes the sub's power consumption from the survey results.", () => {
        expect(computePowerConsumption(report)).toBe(198);
    });
});
