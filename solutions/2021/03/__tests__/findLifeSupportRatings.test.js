import { describe, it, expect } from "vitest";
import { report } from "./testInput.js";
import { findO2GeneratorRating, findCO2ScrubberRating } from "../findLifeSupportRatings.js";

describe("findO2GeneratorRating", () => {
    it("computes the Oxygen Generator Rating from the report by using the survey.", () => {
        expect(findO2GeneratorRating(report)).toBe(23);
    });
});

describe("findCO2ScrubberRating", () => {
    it("computes the Carbon Dioxide Scrubber Rating from the report by using the survey.", () => {
        expect(findCO2ScrubberRating(report)).toBe(10);
    });
});
