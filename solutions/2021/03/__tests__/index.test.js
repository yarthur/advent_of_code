import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("Solutions for 2021, day 3.", () => {
    it("solves for Part 1.", () => {
        expect(part1(input)).toBe(198);
    });

    it("solves for Part 2.", () => {
        expect(part2(input)).toBe(230);
    });
});
