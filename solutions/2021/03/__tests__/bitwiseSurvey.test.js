import { describe, it, expect } from "vitest";
import { report, surveyResults } from "./testInput.js";
import bitwiseSurvey from "../bitwiseSurvey.js";

describe("bitwiseSurvey", () => {
    it("surveys the values of the report and maps the occurance of a `1` or `0` in each bit.", () => {
        expect(bitwiseSurvey(report)).toStrictEqual(surveyResults);
    });
});
