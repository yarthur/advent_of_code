import { describe, it, expect } from "vitest";
import { input, report } from "./testInput.js";
import parseReport from "../parseReport.js";

describe("parseReport", () => {
    it("parses the input into a useable report.", () => {
        expect(parseReport(input)).toStrictEqual(report);
    });
});
