import bitwiseSurvey from "./bitwiseSurvey.js";

/**
 * Computes the power consumption from the diagnostic report.
 * @function computePowerConsumption
 * @memberof 2021.03
 * @param { Array<string> } report - The submarine's diagnostic report.
 * @returns { number } The power consumption of the submarine.a
 */
export default (report) => {
    const survey = bitwiseSurvey(report);

    const [gamma, epsilon] = survey.reduce(
        (values, surveyResults) => {
            if (surveyResults.get("0") > surveyResults.get("1")) {
                values[0] += "0";
                values[1] += "1";
            } else {
                values[0] += "1";
                values[1] += "0";
            }

            return values;
        },
        ["", ""]
    );

    return Number.parseInt(gamma, 2) * Number.parseInt(epsilon, 2);
};
