/**
 * Parses the puzzle input into a planned course.
 * @function parseInput
 * @memberof 2021.02
 * @param { string } input - The puzzle input string.
 * @returns { 2021.02.Movement } - The planned course for the submarine.
 */
export default (input) => {
    return input.split("\n")
        .map((move) => {
            const [direction, distance] = move.split(" ");

            return [direction, Number.parseInt(distance, 10)];
        }
    );
};
