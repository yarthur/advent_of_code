/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 2](https://adventofcode.com/2021/02/).
 * @namespace 02
 * @summary Dive!
 * @memberof 2021
 */

import parseInput from "./parseInput.js";
import computeLocation from "./computeLocation.js";
import computeAimAndLocation from "./computeAimAndLocation.js";

/**
 * Solves part 1 of the day's puzzle.
 * @function part1
 * @memberof 2021.02
 * @param { string } input - The input string.
 * @returns { number } The multiple of the submarine's horizontal position and final depth.
 */
export const part1 = (input) => {
    const movements = parseInput(input);

    const { horizontal, vertical } = computeLocation(movements);

    return horizontal * vertical;
};

/**
 * Solves part 2 of the day's puzzle.
 * @function part2
 * @memberof 2021.02
 * @param { string } input - The input string.
 * @returns { number } The multiple of the submarine's horizontal position and final depth.
 */
export const part2 = (input) => {
    const movements = parseInput(input);

    const { horizontal, vertical } = computeAimAndLocation(movements);

    return horizontal * vertical;
};
