/**
 * @typedef { Array } Movement
 * @summary The planned course for the submarine.
 * @memberof 2021.02
 * @property { string } 0 - The direction indicated for the current move.
 * @property { number } 1 - The distance indicated for the current move.
 */

/**
 * @typedef { object } Location
 * @summary The location of the submarine at a given point of travel.
 * @memberof 2021.02
 * @property { number } horizontal - The location of the submarine along the horizontal axis.
 * @property { number } vertical - The location of the submarine along the vertical axis.
 */

/**
 * @typedef { Location } AimAndLocation
 * @summary  The location of the submarine at a given point of travel.
 * @memberof 2021.02
 * @property { number } horizontal - The location of the submarine along the horizontal axis.
 * @property { number } horizontal - The location of the submarine along the horizontal axis.
 * @property { number } vertical - The location of the submarine along the vertical axis.
 * @property { number } aim - The aim (up or down) of the submarine's travel.
 */
