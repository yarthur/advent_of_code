/**
 * Computes and returns the aim and location of the submarine.
 * @function computeAimAndLocation
 * @memberof 2021.02
 * @param { Array<2021.02.Movement> } movements - The planned course for the submarine.
 * @returns { 2021.02.AimAndLocation } The location of the submarine at the end of the course.
 */
export default (movements) => {
    return movements.reduce(
        ({ horizontal, vertical, aim }, [direction, distance]) => {
            switch (direction) {
                case "forward":
                    horizontal += distance;
                    vertical += distance * aim;
                    break;

                case "up":
                    aim -= distance;
                    break;

                case "down":
                    aim += distance;
                    break;
            }

            return { horizontal, vertical, aim };
        },
        { horizontal: 0, vertical: 0, aim: 0 }
    );
};
