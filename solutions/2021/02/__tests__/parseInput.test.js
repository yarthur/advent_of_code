import { describe, it, expect } from "vitest";
import { input, movements } from "./testInput.js";
import parseInput from "../parseInput.js";

describe("parseInput", () => {
    it("parses the input into a collection of movements", () => {
        expect(parseInput(input)).toStrictEqual(movements);
    });
});
