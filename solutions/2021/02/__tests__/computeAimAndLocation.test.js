import { describe, it, expect } from "vitest";
import { movements, aimAndLocation } from "./testInput.js";
import computeAimAndLocation from "../computeAimAndLocation.js";

describe("computeAimAndLocation", () => {
    it("computes aim and location based off a set of movements.", () => {
        expect(computeAimAndLocation(movements)).toStrictEqual(aimAndLocation);
    });
});
