import { describe, it, expect } from "vitest";
import { movements, loc } from "./testInput.js";
import computeLocation from "../computeLocation.js";

describe("computeLocation", () => {
    it("returns an array of location coordinates", () => {
        expect(computeLocation(movements)).toStrictEqual(loc);
    });
});
