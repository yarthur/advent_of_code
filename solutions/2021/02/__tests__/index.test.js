import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2021, Day 2", () => {
    describe("part1", () => {
        it("solves for part 1", () => {
            expect(part1(input)).toBe(150);
        });
    });

    describe("part2", () => {
        it("solves for part 2", () => {
            expect(part2(input)).toBe(900);
        });
    });
});
