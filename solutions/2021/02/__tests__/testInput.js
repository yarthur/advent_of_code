// @ts-check
/**
 * @typedef {import("../types").Movement} Movement
 * @typedef {import("../types").Location} Location
 * @typedef {import("../types").AimAndLocation} AimAndLocation
 */

/**
 * @type {string}
 */
export const input = `forward 5
down 5
forward 8
up 3
down 8
forward 2`;

/**
 * @type {Movement[]}
 */
export const movements = [
    ["forward", 5],
    ["down", 5],
    ["forward", 8],
    ["up", 3],
    ["down", 8],
    ["forward", 2],
];

/**
 * @type {Location}
 */
export const loc = {
    horizontal: 15,
    vertical: 10,
};

/**
 * @type {AimAndLocation}
 */
export const aimAndLocation = {
    horizontal: 15,
    vertical: 60,
    aim: 10,
};
