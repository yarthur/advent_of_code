/**
 * Computes and returns the location of the submarine.
 * @function computeLocation
 * @memberof 2021.02
 * @param { Array<2021.02.Movement> } movements - The planned course for the submarine.
 * @returns { 2021.02.Location } The location of the submarine at the end of the course.
 */
export default (movements) => {
    return movements.reduce(
        ({ horizontal, vertical }, [direction, distance]) => {
            switch (direction) {
                case "forward":
                    horizontal += distance;
                    break;

                case "up":
                    vertical -= distance;
                    break;

                case "down":
                    vertical += distance;
                    break;
            }

            return { horizontal, vertical };
        },
        { horizontal: 0, vertical: 0 }
    );
};
