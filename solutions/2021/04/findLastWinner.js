import playBingo from "./playBingo.js";

/**
 * Find the last possible winner of the Bingo Game.
 * @function findLastWinner
 * @memberof 2021.04
 * @param { 2021.04.Game } game - The Game of Bingo being  played.
 * @returns { 2021.04.Winner } The last Winner of the game.
 */
export default (game) => {
    let numbers = [...game.numbers];
    let winner = playBingo(game);

    while (winner.remainingBoards.length > 0) {
        numbers = numbers.slice(winner.numbersCalled.length);

        winner = playBingo({ numbers, boards: winner.remainingBoards });
    }

    return winner;
};
