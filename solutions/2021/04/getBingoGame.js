/**
 * Parses the puzzle input into the parts of a Bingo game.
 * @function getBingoGame
 * @memberof 2021.04
 * @param { string } input - The puzzle input.
 * @returns { 2021.04.Game } All the necessary parts for the game.
 */
export default (input) => {
    const [numbersInput, ...boardsInput] = input.split("\n\n");

    const numbers = numbersInput.split(",").map((numberString) => {
        return Number.parseInt(numberString, 10);
    });

    const boards = boardsInput.map((boardInput) => {
        const rowInput = boardInput.split("\n");

        const columns = [];

        const rows = rowInput.reduce((rowArray, rowInput) => {
            const row = rowInput.split(/\s+/).map((numberString) => {
                return Number.parseInt(numberString, 10);
            });

            if (Number.isNaN(row[0])) {
                row.shift();
            }

            rowArray.push(row);

            row.forEach((num, columnIndex) => {
                if (columns[columnIndex] === undefined) {
                    columns[columnIndex] = [];
                }

                columns[columnIndex].push(num);
            });

            return rowArray;
        }, []);

        return new Set([...rows, ...columns]);
    });

    return {
        numbers,
        boards,
    };
};
