/**
 * @typedef { Set<Array<number>> } Board
 * @summary A Bingo Board
 * @memberof 2021.04
 */

/**
 * @typedef { object } Game
 * @summary All the necessary parts for a game of Bingo.
 * @memberof 2021.04
 * @property { Array<number> } numbers - The numbers called in the game (in order that they're called).
 * @property { Array<Board> } boards - The playable boards in the game.
 */

/**
 * @typedef { object } Winner
 * @summary The data around a winning Board in a game of Bingo.
 * @memberof 2021.04
 * @property { Array<number> } numbersCalled - The numbers that had been called up to the point of winning.
 * @property { 2021.04.Board } winningBoard - The board that won the game.
 * @property { Array<2021.04.Board> } remainingBoards - The remaining, non-winning boards.
 */
