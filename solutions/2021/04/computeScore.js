/**
 * Computes the score of a winning Bingo board.
 * @function computeScore
 * @memberof 2021.04
 * @param { 2021.04.Winner } winner - The winning Bingo Board.
 * @returns { number } The score for the winning Board of the Game.
 */
export default ({ numbersCalled, winningBoard }) => {
    const lastNumberCalled = [...numbersCalled].pop();
    const remainingNumbers = new Set();

    winningBoard.forEach((winScenario) => {
        winScenario.forEach((num) => {
            remainingNumbers.add(num);
        });
    });

    const sumOfRemaining = [...remainingNumbers].reduce((sum, num) => {
        return (sum += num);
    }, 0);

    return sumOfRemaining * lastNumberCalled;
};
