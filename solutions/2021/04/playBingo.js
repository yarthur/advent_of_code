/**
 * Play through a game of Bingo.
 * @param { 2021.04.Game } game - The Game being played.
 * @returns { 2021.04.Winner } The winner of the game.
 */
export default ({ numbers, boards }) => {
    const numbersCalled = [];

    let winningBoard;
    let remainingBoards = [...boards];

    while (
        numbersCalled.length < numbers.length &&
        winningBoard === undefined
    ) {
        const currentNumber = numbers[numbersCalled.length];

        numbersCalled.push(currentNumber);

        remainingBoards = remainingBoards.reduce((newBoards, board) => {
            let hasWinner = false;

            const newBoard = [...board].map(
                (possibleWinner) => {
                    const newPW = possibleWinner.filter((num) => {
                        return num !== currentNumber;
                    });

                    if (newPW.length === 0) {
                        hasWinner = true;
                    }

                    return newPW;
                }
            );

            if (hasWinner) {
                winningBoard = new Set(newBoard);
            } else {
                newBoards.push(new Set(newBoard));
            }

            return newBoards;
        }, []);
    }

    return { numbersCalled, winningBoard, remainingBoards };
};
