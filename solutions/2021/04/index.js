/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 4](https://adventofcode.com/2021/04/).
 * @namespace 04
 * @summary Giant Squid
 * @memberof 2021
 */

import getBingoGame from "./getBingoGame.js";
import playBingo from "./playBingo.js";
import computeScore from "./computeScore.js";
import findLastWinner from "./findLastWinner.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2021.04
 * @param { string } input - The input string.
 * @returns { number } The final score if you choose the winning board.
 */
export const part1 = (input) => {
    const game = getBingoGame(input);

    const winner = playBingo(game);

    return computeScore(winner);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2021.04
 * @param { string } input - The input string.
 * @returns { number } The final score of the board that will win last.
 */
export const part2 = (input) => {
    const game = getBingoGame(input);
    const lastWinner = findLastWinner(game);

    return computeScore(lastWinner);
};
