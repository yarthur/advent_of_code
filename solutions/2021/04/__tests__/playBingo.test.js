import { describe, it, expect } from "vitest";
import { gameReturned, firstWinner } from "./testInput.js";
import playBingo from "../playBingo.js";

describe("playBingo", () => {
    it("finds the first winner of the game.", () => {
        expect(playBingo(gameReturned)).toStrictEqual(firstWinner);
    });
});
