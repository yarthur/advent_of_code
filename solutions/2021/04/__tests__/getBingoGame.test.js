import { describe, it, expect } from "vitest";
import { input, gameReturned } from "./testInput.js";
import getBingoGame from "../getBingoGame.js";

describe("getBingoGame", () => {
    it("receives input and returns inidividual game parts.", () => {
        expect(getBingoGame(input)).toStrictEqual(gameReturned);
    });
});
