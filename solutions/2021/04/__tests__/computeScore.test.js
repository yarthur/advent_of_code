import { describe, it, expect } from "vitest";
import { firstWinner } from "./testInput.js";
import computeScore from "../computeScore.js";

describe("computeScore", () => {
    it("computes the score of a Bingo winner.", () => {
        expect(computeScore(firstWinner)).toBe(4512);
    });
});
