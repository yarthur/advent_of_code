import { describe, it, expect } from "vitest";
import { gameReturned, lastWinner } from "./testInput.js";
import findLastWinner from "../findLastWinner.js";

describe("findLastWinner", () => {
    it("finds the last board that would win the game.", () => {
        expect(findLastWinner(gameReturned)).toStrictEqual(lastWinner);
    });
});
