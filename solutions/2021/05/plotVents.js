/**
 * @function sortCoordinates
 * @summary A sort function for XY Coordinates
 * @memberof 2021.05
 * @private
 * @param { Array } firstCoordinates - The first XY coordinates in the sort comparison.
 * @param { number } firstCoordinates.x1 - The first X coordinates in the sort comparison.
 * @param { number } firstCoordinates.y1 - The first Y coordinates in the sort comparison.
 * @param { Array } secondCoordinates - The second XY coordinates in the sort comparison.
 * @param { number } secondCoordinates.x2 - The second X coordinates in the sort comparison.
 * @param { number } secondCoordinates.y2 - The second Y coordinates in the sort comparison.
 * @returns { number } An integer that informs the sort function.
 */
const sortCoordinates = ([x1, y1], [x2, y2]) => {
    if (x1 === x2) {
        return y1 - y2;
    }

    return x1 - x2;
};

/**
 * @function plotDiagonals
 * @summary An accumulator function to plot the number of vents on a point on a grid.
 * @memberof 2021.05
 * @private
 * @param { Map<number, string> } map - The grid map, with a count of vents per grid point.
 * @param { number } xStart - The Starting X Coordinate.
 * @param { number } xEnd - The Ending X Coordinate.
 * @param { number } yStart - The Starting Y Coordinate.
 * @param { number } yEnd - The Ending Y Coordinate. *
 * @returns { Map<number, string> } The grid map, with a count of vents per grid point.
 */
const plotDiagonals = (map, xStart, xEnd, yStart, yEnd) => {
    const direction = yStart < yEnd ? "up" : "down";
    const distance = xEnd - xStart;

    for (let index = 0; index <= distance; index += 1) {
        const x = xStart + index;
        const y = direction === "up" ? yStart + index : yStart - index;

        const coordinateKey = [x, y].join(",");
        const count = map.get(coordinateKey) || 0;
        map.set(coordinateKey, count + 1);
    }

    return map;
};

/**
 * @function plotVents
 * @summary Plot the number of vents present on each point of a grid.
 * @memberof 2021.05
 * @param { Array<2021.05.VentCoordinate> } coordinates - The Vent Coordinates to be mapped.
 * @param { boolean } [withDiagonals = false] - Flag to indicate  whether diagonals can exist.
 * @returns { Map<string, number> } The grid map, with a count of vents per grid point.
 */
export default (coordinates, withDiagonals = false) => {
    const plot = coordinates.reduce((map, coordinate) => {
        const [[xStart, yStart], [xEnd, yEnd]] =
            Object.values(coordinate).sort(sortCoordinates);

        if (xStart !== xEnd && yStart !== yEnd) {
            if (withDiagonals) {
                map = plotDiagonals(map, xStart, xEnd, yStart, yEnd);
            }

            return map;
        }

        for (let x = xStart; x <= xEnd; x += 1) {
            for (let y = yStart; y <= yEnd; y += 1) {
                const coordinateKey = `${x},${y}`;
                const count = map.get(coordinateKey) || 0;
                map.set(coordinateKey, count + 1);
            }
        }

        return map;
    }, new Map());
    return plot;
};
