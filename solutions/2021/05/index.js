/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 5](https://adventofcode.com/2021/05/).
 * @namespace 05
 * @summary Hydrothermal Venture
 * @memberof 2021
 */

import getVentCoordinates from "./getVentCoordinates.js";
import plotVents from "./plotVents.js";
import countIntersections from "./countIntersections.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2021.05
 * @param { string } input - The input string.
 * @returns {number} The number of points where at least two lines overlap.
 */
export const part1 = (input) => {
    const ventCoordinates = getVentCoordinates(input);
    const ventPlot = plotVents(ventCoordinates);

    return countIntersections(ventPlot);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2021.05
 * @param { string } input - The input string.
 * @returns {number} The number of points where at least two lines overlap.
 */
export const part2 = (input) => {
    const ventCoordinates = getVentCoordinates(input);
    const ventPlot = plotVents(ventCoordinates, true);

    return countIntersections(ventPlot);
};
