import { describe, it, expect } from "vitest";
import { ventMapNoDiag, ventMapWithDiag } from "./testInput.js";
import countIntersections from "../countIntersections.js";

describe("countIntersections", () => {
    it("counts the number of points where 2+ vents intersect", () => {
        expect(countIntersections(ventMapNoDiag)).toBe(5);
        expect(countIntersections(ventMapWithDiag)).toBe(12);
    });
});
