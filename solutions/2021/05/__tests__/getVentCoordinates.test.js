import { describe, it, expect } from "vitest";
import { input, ventCoordinates } from "./testInput.js";
import getVentCoordinates from "../getVentCoordinates.js";

describe("getVentCoordinates", () => {
    it("parses the input into a series of vent coordinates.", () => {
        expect(getVentCoordinates(input)).toStrictEqual(ventCoordinates);
    });
});
