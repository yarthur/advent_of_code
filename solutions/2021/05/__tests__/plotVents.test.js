import { describe, it, expect } from "vitest";
import {
    ventCoordinates,
    ventMapNoDiag,
    ventMapWithDiag,
} from "./testInput.js";
import plotVents from "../plotVents.js";

describe("plotVents", () => {
    describe("plots vents from a list of coordinates.", () => {
        it("does not plot diagonal lines.", () => {
            expect(plotVents(ventCoordinates)).toStrictEqual(ventMapNoDiag);
        });

        it("does plot diagonal lines.", () => {
            expect(plotVents(ventCoordinates, true)).toStrictEqual(
                ventMapWithDiag
            );
        });
    });
});
