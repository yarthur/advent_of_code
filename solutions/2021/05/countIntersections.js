/**
 * @function countIntersections
 * @summary Counts the number of grid points with multiple vents.
 * @memberof 2021.05
 * @param { Map<string, number> } ventPlot - The grid map, with a count of vents per grid point.
 * @returns { number } - The total number of grid points with multiple vents.
 */
export default (ventPlot) => {
    return [...ventPlot.values()].reduce((count, value) => {
        if (value > 1) {
            count += 1;
        }

        return count;
    }, 0);
};
