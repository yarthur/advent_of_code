/**
 * @function parseXYCoordiates
 * @summary Parses a string into a set of XY coordinates
 * @memberof 2021.05
 * @private
 * @param { string } coordinateString - The string version of the XY Coordinate.
 * @returns { Array } xycoordinate - The XY Coordinate array.
 */
const parseCoordinate = (coordinateString) => {
    return coordinateString.split(",")
        .map((coordinate) => {
            return Number.parseInt(coordinate, 10);
        });
};

/**
 * @function getVentCoordinates
 * @summary Parses the puzzle input into an array of Vent Coordinates.
 * @memberof 2021.05
 * @param { string } input - The puzzle input string.
 * @returns { Array<2021.05.VentCoordinate> } An array of Vent Coordinates.
 */
export default (input) => {
    return input.split("\n").map((vent) => {
        const coords = vent.split(" -> ");

        return {
            start: parseCoordinate(coords[0]),
            end: parseCoordinate(coords[1]),
        };
    });
};
