/**
 * @typedef { object } VentCoordinate
 * @summary A vent expressed as a pair of XY Coordinates.
 * @memberof 2021.05
 * @property { Array<number, number> } start - The starting coordinate for the vent.
 * @property { Array<number, number> } end - The ending coordinate for the vent.
 * @description
 * Each line of vents is given as a line segment in the format `x1,y1 -> x2,y2`
 * where `x1`,`y1` are the coordinates of one end the line segment and
 * `x2`,`y2` are the coordinates of the other end. These line segments include
 * the points at both ends.
 */
