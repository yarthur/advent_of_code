/**
 * Adds up the total population of the school of lanternfish.
 * @function getTotalPopulation
 * @memberof 2021.06
 * @param { 2021.06.PopulationSet } population - The
 * current Population Set of the school.
 * @returns {number} The total population of the school of lanternfish.
 */
export default (population) => {
    return [...population.values()].reduce((count, group) => {
        return count + group[0] + group[1];
    }, 0);
};
