/**
 * @typedef { Map<number, [number, number]> } PopulationSet
 * @summary The Population information for the school of lanternfish.
 * @memberof 2021.06
 */
