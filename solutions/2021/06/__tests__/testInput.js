// @ts-check

/**
 * @typedef {import("../types").PopulationSet} PopulationSet
 */

/**
 * @type {string}
 */
export const input = `3,4,3,1,2`;

/**
 * @type {PopulationSet}
 */
export const initialState = new Map([
    [0, [0, 0]],
    [1, [1, 0]],
    [2, [1, 0]],
    [3, [2, 0]],
    [4, [1, 0]],
    [5, [0, 0]],
    [6, [0, 0]],
]);

/**
 * @type {PopulationSet}
 */
export const stateAfter18Days = new Map([
    [0, [2, 0]],
    [1, [2, 0]],
    [2, [1, 0]],
    [3, [5, 0]],
    [4, [3, 1]],
    [5, [5, 4]],
    [6, [3, 0]],
]);
