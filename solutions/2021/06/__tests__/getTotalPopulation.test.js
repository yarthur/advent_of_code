import { describe, it, expect } from "vitest";
import { stateAfter18Days } from "./testInput.js";
import getTotalPopulation from "../getTotalPopulation.js";

describe("getTotalPopulation", () => {
    it("returns the total population count from a given population set.", () => {
        expect(getTotalPopulation(stateAfter18Days)).toBe(26);
    });
});
