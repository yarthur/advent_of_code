import { describe, it, expect } from "vitest";
import { initialState, stateAfter18Days } from "./testInput.js";
import modelPopulation from "../modelPopulation.js";

describe("modelPopulation", () => {
    it("models a given population state after a given number of days.", () => {
        expect(modelPopulation(initialState, 18)).toStrictEqual(
            stateAfter18Days
        );
    });
});
