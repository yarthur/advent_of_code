import { describe, it, expect } from "vitest";
import { input, initialState } from "./testInput.js";
import getInitialState from "../getInitialState.js";

describe("getInitialState", () => {
    it("returns a dataset representing the initial state.", () => {
        expect(getInitialState(input)).toStrictEqual(initialState);
    });
});
