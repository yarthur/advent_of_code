/**
 * Parses the puzzle input string into the current Population state of the Lanternfish school.
 * @function getIntitialState
 * @memberof 2021.06
 * @param { string } input - The puzzle input string.
 * @returns { 2021.06.PopulationSet } The starting Population state of the school.
 */
export default (input) => {
    /**
     * @type {PopulationSet}
     */
    const fishState = new Map([
        [0, [0, 0]],
        [1, [0, 0]],
        [2, [0, 0]],
        [3, [0, 0]],
        [4, [0, 0]],
        [5, [0, 0]],
        [6, [0, 0]],
    ]);

    input.split(",").forEach((key) => {
        const group = Number.parseInt(key, 10);
        const fishCount = fishState.get(group);
        fishState.set(group, [fishCount[0] + 1, 0]);
    });

    return fishState;
};
