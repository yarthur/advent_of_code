/**
 * Models the population details of the school of lanternfish over a given time.
 * @function modelPopulation
 * @memberof 2021.06
 * @param { 2021.06.PopulationSet } population - The initial Population information for the school of lanternfish.
 * @param { number } simulationLength - The length of time the simulation runs.
 * @returns { 2021.06.PopulationSet } The final Population information for the school of lanternfish.
 */
export default (population, simulationLength) => {
    const spawnCycle = population.size;

    for (let day = 0; day < simulationLength; day += 1) {
        const dayInCycle = day % spawnCycle;
        const spawnPopulationGroup = (dayInCycle + 2) % spawnCycle;

        const spawningPopulation = population.get(dayInCycle);
        const spawnedPopulation = population.get(spawnPopulationGroup);

        population.set(spawnPopulationGroup, [
            spawnedPopulation[0],
            spawningPopulation[0],
        ]);
        population.set(dayInCycle, [
            spawningPopulation[0] + spawningPopulation[1],
            0,
        ]);
    }

    return population;
};
