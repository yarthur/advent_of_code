/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 6](https://adventofcode.com/2021/06/).
 * @namespace 06
 * @summary Lanternfish
 * @memberof 2021
 */

import getInitialState from "./getInitialState.js";
import modelPopulation from "./modelPopulation.js";
import getTotalPopulation from "./getTotalPopulation.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2021.06
 * @param { string } input - The input string.
 * @returns { number } How many lanternfish there would be after 80 days.
 */
export const part1 = (input) => {
    const initialState = getInitialState(input);
    const endPopulationState = modelPopulation(initialState, 80);

    return getTotalPopulation(endPopulationState);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2021.06
 * @param { string } input - The input string.
 * @returns { number } How many lanternfish there would be after 256 days.
 */
export const part2 = (input) => {
    const initialState = getInitialState(input);
    const endPopulationState = modelPopulation(initialState, 256);

    return getTotalPopulation(endPopulationState);
};
