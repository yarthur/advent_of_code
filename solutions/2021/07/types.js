/**
 * @typedef { number } Position
 * @summary The name/label for a horizontal position.
 * @memberof 2021.07
 */

/**
 * @typedef { Map< 2021.07.Position, number > } CrabMap
 * @summary A map of the number of Crabs per Position
 * @memberof 2021.07
 */

/**
 * @typedef { Array } MostEfficientPosition
 * @summary Data on the Most Efficient Position.
 * @memberof 2021.07
 * @property { 2021.07.Position } 0 - The Position that would be most efficient to move the crabs to.
 * @property { , number > } 1 - The fuel cost to move all crabs to that position.
 */
