/**
 * Calculate the fuel cost at each position to align all crabs to that position.
 * @function calculateFuelCosts
 * @memberof 2021.07
 * @param { 2021.07.CrabMap } positions - The Map of positions and the number of crabs in that position.
 * @param { boolean } [series] - A flag to indicate whether the fuel costs are calculated in series or not.
 * @returns { Map<2021.07.Position, number> } A Map of the fuel cost to align all crabs to a single position.
 */
export default (positions, series = false) => {
    const [floor, ceiling] = [...positions.keys()].reduce(
        ([smallest, largest], position) => {
            if (position < smallest || smallest === null) {
                smallest = position;
            }

            if (position > largest || largest === null) {
                largest = position;
            }

            return [smallest, largest];
        },
        [null, null]
    );

    const fuelCosts = new Map();

    for (let endPosition = floor; endPosition <= ceiling; endPosition += 1) {
        let totalFuelCost = 0;

        [...positions.entries()].forEach(([startingPosition, crabCount]) => {
            const distance = Math.abs(startingPosition - endPosition);
            let cost = distance;

            if (series) {
                cost = distance * ((1 + distance) / 2);
            }

            totalFuelCost += cost * crabCount;
        });

        fuelCosts.set(endPosition, totalFuelCost);
    }

    return fuelCosts;
};
