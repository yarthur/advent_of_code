import { describe, it, expect } from "vitest";
import { input, initialPositions } from "./testInput.js";
import getInitialPositions from "../getInitialPositions.js";

describe("getInitialPositions", () => {
    it("takes the input, and returns a map of the number of crabs per position.", () => {
        expect(getInitialPositions(input)).toStrictEqual(initialPositions);
    });
});
