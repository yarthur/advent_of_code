import { describe, it, expect } from "vitest";
import { initialPositions, calculatedFuelCosts } from "./testInput.js";
import calculateFuelCosts from "../calculateFuelCosts.js";

describe("calculateFuelCosts", () => {
    it("calculates the straight-line fuel costs if changing to each position.", () => {
        const fuelCosts = calculateFuelCosts(initialPositions);

        [...calculatedFuelCosts.entries()].forEach(([position, value]) => {
            expect(fuelCosts.get(position)).toBe(value);
        });
    });

    it("calculates the arithmetic series fuel cost.", () => {
        const fuelCosts = calculateFuelCosts(initialPositions, true);
        const expected = [
            [5, 168],
            [2, 206],
        ];

        expected.forEach(([position, value]) => {
            expect(fuelCosts.get(position)).toBe(value);
        });
    });
});
