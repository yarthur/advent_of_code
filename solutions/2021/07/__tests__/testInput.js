// @ts-check

/**
 * @type {string}
 */
export const input = `16,1,2,0,4,2,7,1,2,14`;

/**
 * @type {Map<number, number>}
 */
export const initialPositions = new Map([
    [0, 1],
    [1, 2],
    [2, 3],
    [4, 1],
    [7, 1],
    [14, 1],
    [16, 1],
]);

export const calculatedFuelCosts = new Map([
    [2, 37],
    [1, 41],
    [3, 39],
    [10, 71],
]);
