import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2021, day 07", () => {
    it("Solves for part 1.", () => {
        expect(part1(input)).toBe(37);
    });

    it("Solves for part 2.", () => {
        expect(part2(input)).toBe(168);
    });
});
