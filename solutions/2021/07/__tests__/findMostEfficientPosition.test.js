import { describe, it, expect } from "vitest";
import { calculatedFuelCosts } from "./testInput.js";
import findMostEfficientPosition from "../findMostEfficientPosition.js";

describe("findMostEfficientPosition", () => {
    it("finds the most fuel-efficient position from a set.", () => {
        expect(findMostEfficientPosition(calculatedFuelCosts)).toStrictEqual([
            2, 37,
        ]);
    });
});
