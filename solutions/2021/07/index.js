/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 7](https://adventofcode.com/2021/07/).
 * @namespace 07
 * @summary The Treachery of Whales
 * @memberof 2021
 */

import getInitialPositions from "./getInitialPositions.js";
import calculateFuelCosts from "./calculateFuelCosts.js";
import findMostEfficientPosition from "./findMostEfficientPosition.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2021.07
 * @param { string } input - The puzzle's input string.
 * @returns { number } How much fuel must be spent to align to horizontal position that the crabs can align to using the least fuel possible.
 */
export const part1 = (input) => {
    const initialFormation = getInitialPositions(input);
    const fuelCosts = calculateFuelCosts(initialFormation);
    const mostEfficientPosition = findMostEfficientPosition(fuelCosts);

    return mostEfficientPosition[1];
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2021.07
 * @param { string } input - The puzzle's input string.
 * @returns { number } How much fuel must be spent to align to horizontal position that the crabs can align to using the least fuel possible so they can make you an escape route.
 */
export const part2 = (input) => {
    const initialFormation = getInitialPositions(input);
    const fuelCosts = calculateFuelCosts(initialFormation, true);
    const mostEfficientPosition = findMostEfficientPosition(fuelCosts);

    return mostEfficientPosition[1];
};
