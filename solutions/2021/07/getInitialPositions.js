/**
 * Parses the puzzle input into a Map of positions and the count of crabs in each position.
 * @function getInitialPositions
 * @memberof 2021.07
 * @param { string } input - The puzzle input string.
 * @returns { 2021.07.CrabMap } The Map of positions and the number of crabs in that position.
 */
export default (input) => {
    return input.split(",").reduce(
        (positions, crab) => {
            const crabPosition = Number.parseInt(crab, 10);
            const crabCount = positions.get(crabPosition) || 0;

            positions.set(crabPosition, crabCount + 1);

            return positions;
        },
        new Map()
    );
};
