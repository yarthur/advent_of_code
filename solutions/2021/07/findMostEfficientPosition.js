/**
 * Find the most efficient position for the crabs to move to based off fuel costs.
 * @function findMostEfficientPosition
 * @memberof 2021.07
 * @param { Map<2021.07.Position, number> } fuelCosts - A Map of the fuel cost to align all crabs to a single position.
 * @returns { 2021.07.MostEfficientPosition } Data on the Most Efficient Position.
 */
export default (fuelCosts) => {
    return [...fuelCosts.entries()].reduce(
        (cheapest, cost) => {
            if (cost[1] < cheapest[1] || cheapest[0] === null) {
                cheapest = cost;
            }

            return cheapest;
        },
        [null, null]
    );
};
