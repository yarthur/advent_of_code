/**
 * Parses the puzzle input into a sonar sweep of the sea floor.
 * @function parseReadings
 * @memberof 2021.01
 * @param { string } input - The puzzle input.
 * @returns { Array<number> } An array of depth readings.
 */
export default (input) => {
    return input.split("\n").map((depthString) => {
        return Number.parseInt(depthString, 10);
    });
};
