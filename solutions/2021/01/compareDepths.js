/**
 * Determines the number of readings that are greater than the previous depth in the array.
 * @function compareDepths
 * @memberof 2021.01
 * @param {number[]} readings - The collection of depth readings.
 * @returns {number} The number of measurements that are larger than the previous measurement.
 */
export default (readings) => {
    return readings.reduce(
        /**
         * @param {number} count
         * @param {number} depth
         * @param {number} index
         * @param {number[]} readings
         * @returns {number}
         */
        (count, depth, index, readings) => {
            if (index > 0 && depth > readings[index - 1]) {
                count += 1;
            }

            return count;
        },
        0
    );
};
