/**
 * Determines the number of three-measurement windows are greater than the previous.
 * @function computeSlidingWindow
 * @memberof 2021.01
 * @param {number[]} readings - The collection of depth readings.
 * @returns {number[]} The number of three-measurement windows that are larger than the previous.
 */
export default (readings) => {
    /**
     * type {number[]}
     */
    let windows = [];

    /**
     * type {number[]}
     */
    let depths = [];

    readings.forEach((depth) => {
        windows.push(0);
        windows = windows.map((win) => {
            return win + depth;
        });

        if (windows.length === 3) {
            const trueDepth = windows.shift();
            depths.push(trueDepth);
        }
    });

    return depths;
};
