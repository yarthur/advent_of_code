/**
 * My solutions for the
 * [2021 edition of Advent of Code, day 1](https://adventofcode.com/2021/01/).
 * @namespace 01
 * @summary Sonar Sweep
 * @memberof 2021
 */

import parseReadings from "./parseReadings.js";
import compareDepths from "./compareDepths.js";
import computeSlidingWindow from "./computeSlidingWindow.js";

/**
 * @function part1
 * @summary Solves for part 1 of the day's puzzle.
 * @memberof 2021.01
 * @param { string } input - The puzzle input string.
 * @returns { number } The number of measurements that are larger than the previous measurement.
 */
export const part1 = (input) => {
    const readings = parseReadings(input);

    return compareDepths(readings);
};

/**
 * @function part2
 * @summary Solves for part 2 of the day's puzzle.
 * @memberof 2021.01
 * @param { string } input - The puzzle input string.
 * @returns { number } The number of measurements that are larger than the previous measurement.
 */
export const part2 = (input) => {
    const readings = parseReadings(input);

    const depths = computeSlidingWindow(readings);

    return compareDepths(depths);
};
