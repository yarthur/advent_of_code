import { describe, it, expect } from "vitest";
import { depths, slidingWindowDepths } from "./testInput.js";
import computeSlidingWindow from "../computeSlidingWindow.js";

describe("computeSlidingWindow", () => {
    it("computes sliding window values of a dataset", () => {
        expect(computeSlidingWindow(depths)).toStrictEqual(slidingWindowDepths);
    });
});
