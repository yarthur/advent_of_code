import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2021, day 01", () => {
    describe("Part 1", () => {
        it("solves for part 1", () => {
            expect(part1(input)).toBe(7);
        });
    });

    describe("Part 2", () => {
        it("solves for part 2", () => {
            expect(part2(input)).toBe(5);
        });
    });
});
