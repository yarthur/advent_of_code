import { describe, it, expect } from "vitest";
import { depths, slidingWindowDepths } from "./testInput.js";
import compareDepths from "../compareDepths.js";

describe("compareDepths", () => {
    it("returns the number of depth increases from a set of data", () => {
        expect(compareDepths(depths)).toBe(7);
        expect(compareDepths(slidingWindowDepths)).toBe(5);
    });
});
