/**
 * @type {string}
 */
export const input = `199
200
208
210
200
207
240
269
260
263`;

/**
 * @type {number[]}
 */
export const depths = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
/**
 * @type {number[]}
 */
export const slidingWindowDepths = [607, 618, 618, 617, 647, 716, 769, 792];
