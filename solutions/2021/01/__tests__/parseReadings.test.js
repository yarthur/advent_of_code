import { describe, it, expect } from "vitest";
import { input, depths } from "./testInput.js";
import parseReadings from "../parseReadings.js";

describe("parseReadings", () => {
    it("parses the input into an array of depths.", () => {
        expect(parseReadings(input)).toStrictEqual(depths);
    });
});
