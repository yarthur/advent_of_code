import { describe, it, expect } from "vitest";
import calculateOverlap from "../calculateOverlap";

const inputs = `#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2`;

describe("calculateOverlap", () => {
    it("calculates the overlap between claims", function () {
        expect(calculateOverlap(inputs).length).toBe(4);
    });
});
