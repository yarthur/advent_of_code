import { describe, it, expect } from "vitest";
import calculateBoxIDChecksum from "../calculateBoxIDChecksum.js";

const input = `abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab`;

describe ("calculateBoxIDChecksum", () => {
    it("produces a checksum from a list of box IDs", () => {
        expect(calculateBoxIDChecksum(input)).toEqual(12);
    })
})
