import { describe, it, expect } from "vitest";
import compareIds from "../compareIds.js";

const input = `abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz`;

describe("compareIds", () => {
    it("does the thing", () => {
        expect(compareIds(input)).toEqual("fgij");
    })
})
