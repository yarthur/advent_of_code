import { describe, it, expect } from "vitest";
import { inputs, steps } from "./sampleData";
import parseSteps from "../parseSteps";

describe("parseSteps", () => {
    it("parses steps correctly", function () {
        expect(parseSteps(inputs)).toEqual(steps);
    });
});
