import { describe, it, expect } from "vitest";
import { steps } from "./sampleData";
import calculateTimeToCompletion from "../calculateTimeToCompletion";

describe("calculateTimeToCompletion", () => {
    it("calculates the correct time", function () {
        expect(calculateTimeToCompletion(steps, 2, 0)).toEqual(15);
    });
});
