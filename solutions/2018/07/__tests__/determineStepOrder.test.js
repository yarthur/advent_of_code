import { describe, it, expect } from "vitest";
import { steps, stepOrder } from "./sampleData";
import determineStepOrder from "../determineStepOrder";

describe("determineStepOrder", () => {
    it("determines the correct step order", function () {
        expect(determineStepOrder(steps)).toEqual(stepOrder);
    });
});
