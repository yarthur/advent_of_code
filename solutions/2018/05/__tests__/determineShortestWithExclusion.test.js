import { describe, it, expect } from "vitest";
import determineShortestWithExclusion from "../determineShortestWithExclusion";
const inputs = "dabAcCaCBAcCcaDA";

describe("determineShortestWithExclusion", () => {
    it("returns the length of the shortest polymer with 1 unit excluded", function () {
        expect(determineShortestWithExclusion(inputs)).toEqual(4);
    });
});
