import { describe, it, expect } from "vitest";
import reactPolymer from "../reactPolymer";

const inputs = "dabAcCaCBAcCcaDA";
const resultsAll = "dabCBAcaDA";
const resultsExclusions = {
    a: 6,
    b: 8,
    c: 4,
    d: 6,
};

describe("reactPolymer", () => {
    it("fully reacts the polymer", function () {
        expect(reactPolymer(inputs)).toEqual(resultsAll);
    });

    it("fully reacts the polymer with exclusions", function () {
        Object.entries(resultsExclusions).forEach(function ([exclusion, result]) {
            expect(reactPolymer(inputs, exclusion).length).toEqual(result);
        });
    });
});
