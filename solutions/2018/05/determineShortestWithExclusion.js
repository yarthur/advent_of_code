import determineUnits from "./determineUnits.js";
import reactPolymer from "./reactPolymer.js";

/**
 *
 * @param polymer
 */
export default function (polymer) {
    var uniqueUnits = determineUnits(polymer);

    return uniqueUnits.reduce(function (defender, contender) {
        var contenderPolymer = reactPolymer(polymer, contender);

        if (defender === null || contenderPolymer.length < defender) {
            return contenderPolymer.length;
        } else {
            return defender;
        }
    }, null);
}
