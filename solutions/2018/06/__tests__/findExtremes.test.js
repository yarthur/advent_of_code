import { describe, it, expect } from "vitest";
import { coordinates, extremes } from "./sampleData";
import findExtremes from "../findExtremes";

describe("findExtremes", () => {
    it("finds the outside bounds of x & y coordinates", function () {
        expect(findExtremes(coordinates)).toEqual(extremes);
    });
});
