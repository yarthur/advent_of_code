import { describe, it, expect } from "vitest";
import { inputs, coordinates } from "./sampleData";
import parseCoordinates from "../parseCoordinates";

describe("parseCoordinates", () => {
    it("parses coordinates into x & y values", function () {
        expect(parseCoordinates(inputs)).toEqual(coordinates);
    });
});
