import { describe, it, expect } from "vitest";
import { coordinates, extremes } from "./sampleData";
import mapArea from "../mapArea";

describe("mapArea", () => {
    it("map the areas of the different locations", function () {
        expect(mapArea(coordinates, extremes)).toEqual({});
    });
});
