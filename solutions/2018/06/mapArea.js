const calcManhattanDistance = function ({ x: xA, y: yA }, { x: xB, y: yB }) {
    return Math.abs(xA - xB) + Math.abs(yA - yB);
};

/**
 *
 * @param coordinates
 * @param root0
 * @param root0.x
 * @param root0.y
 */
export default function (coordinates, { x: xLimit, y: yLimit }) {
    var firstCoord = coordinates.shift(),
        map = [];

    for (let x = xLimit[0]; x < xLimit[1]; x += 1) {
        for (let y = yLimit[0]; y < yLimit[1]; y += 1) {
            let distance = calcManhattanDistance(firstCoord, { x, y });

            map[`${x}:${y}`] = coordinates.reduce(
                function (defender, contender) {
                    let newDistance = calcManhattanDistance(contender, {
                        x,
                        y,
                    });

                    if (newDistance < defender[0]) {
                        defender = [newDistance, contender];
                    } else if (newDistance === defender[0]) {
                        defender[1] = null;
                    }

                    return defender;
                },
                [distance, firstCoord]
            );
        }
    }

    return {};
}
