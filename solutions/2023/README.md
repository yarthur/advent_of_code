# 2023 — ⭐️: 18

Something is wrong with global snow production, and you've been selected to take a look. The Elves have even given you a map; on it, they've used stars to mark the top fifty locations that are likely to be having problems.

You've been doing this long enough to know that to restore snow operations, you need to check all **fifty stars** by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants **one star**. Good luck!

(My solutions for the [2023 edition of Advent of Code](https://adventofcode.com/2023/))

## Solutions

- [Day 1](/solutions/2023/01/index.js) — ⭐️⭐️
- [Day 2](/solutions/2023/02/index.js) — ⭐️⭐️
- [Day 3](/solutions/2023/03/index.js) — ⭐️⭐️
- [Day 4](/solutions/2023/04/index.js) — ⭐️⭐️
- [Day 5](/solutions/2023/05/index.js) — ⭐️⭐️
- [Day 6](/solutions/2023/06/index.js) — ⭐️⭐️
- [Day 7](/solutions/2023/07/index.js) — ⭐️⭐️
- [Day 8](/solutions/2023/08/index.js) — ⭐️⭐️
- [Day 9](/solutions/2023/09/index.js) — ⭐️⭐️
