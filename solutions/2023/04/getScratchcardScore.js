/**
 * @function getScratchcardScore
 * @summary Calculates the Score of a given Scratchcard.
 * @memberof 2023.04
 * @param { Array<number> } matchingNumbers - The
 * matching numbers on the Scratchcard.
 * @returns { number } The score of the Scratchcard
 */
export default (matchingNumbers) => {
    const matchCount = matchingNumbers.length;

    if (matchCount <= 1) {
        return matchCount;
    }

    return Math.pow(2, matchCount - 1);
};
