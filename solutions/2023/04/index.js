/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 4](https://adventofcode.com/2023/day/4/).
 * @namespace 04
 * @summary Scratchcards
 * @memberof 2023
 */

import getScratchcards from "./getScratchcards.js";
import getMatchingNumbers from "./getMatchingNumbers.js";
import getScratchcardScore from "./getScratchcardScore.js";
import getAllCopies from "./getAllCopies.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.04
 * @param { string } input The puzzle's input string.
 * @returns { number } The total number of points the elf's Scratchcards are worth.
 */
export const part1 = (input) => {
    const scratchcards = getScratchcards(input);
    const matchingNumbers = Array.from(scratchcards.values())
        .map(getMatchingNumbers);
    const scores = matchingNumbers.map(getScratchcardScore);

    return scores.reduce((sum, score) => {
        return sum + score;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.04
 * @param { string } input The puzzle's input string.
 * @returns { number } The total number of scratchcards you end up with.
 */
export const part2 = (input) => {
    const scratchcards = getScratchcards(input);
    const allCopies = getAllCopies(scratchcards);

    return Array.from(allCopies.values())
        .reduce((sum, score) => {
            return sum + score;
        }, 0);
};
