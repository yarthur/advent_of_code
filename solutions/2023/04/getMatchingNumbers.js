/**
 * @function getMatchingNumbers
 * @summary Gets the Card Numbers that match the Winning Numbers of the Scratchcard.
 * @memberof 2023.04
 * @param { 2023.04.Scratchcard } card - The
 * Scratchcard being evaluated.
 * @returns { Array<number> } The collection of matching numbers on the Scratchcard.
 */
export default ({ winningNumbers, cardNumbers }) => {
    return winningNumbers.filter((number) => {
        return cardNumbers.includes(number);
    });
};
