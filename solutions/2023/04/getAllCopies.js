import getMatchingNumbers from "./getMatchingNumbers.js";

/**
 * @function getAllCopies
 * @summary Gets all copies of all the Scratchcards.
 * @memberof 2023.04
 * @param { Map<number, 2023.04.Scratchcards> } scratchcards - The initial collection of Scratchcards.
 * @returns { Map<number, number> } The amount of
 * copies of each card.
 */
export default (scratchcards) => {
    const copies = new Map(
        Array.from(scratchcards.keys())
            .map((key) => {
                return [key, 1];
            })
    );

    Array.from(scratchcards.entries())
        .forEach(([key, card]) => {
            const copiesOfThisCard = copies.get(key);

            const matchingNumbers = getMatchingNumbers(card);

            for(let keyModifier = 1; keyModifier <= matchingNumbers.length; keyModifier += 1) {
                const keyToAddTo = key + keyModifier;
                const currentCount = copies.get(keyToAddTo);
                copies.set(keyToAddTo, currentCount + copiesOfThisCard);
            }
        });

    return copies;
}
