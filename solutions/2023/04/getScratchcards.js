/**
 * @typedef { object } Scratchcard
 * @summary Data representation of a Scratchcard.
 * @memberof 2023.04
 * @property { Array<number> } winningNumbers - The Winning Numbers for this particular Scratchcard.
 * @property { Array<number> } cardNumbers - The numbers on the card itself.
 */

/**
 * @function getScratchcards
 * @summary Parses an Input String into a collection of Scratchcards.
 * @memberof 2023.04
 * @param { string } input - The Input String.
 * @returns { Map<number, 2023.04.Scratchcard> } The collection of Scratchards.
 */
export default (input) => {
    return input.split("\n")
        .reduce((cards, cardString) => {
            const [keyStr, numbers] = cardString.split(": ");
            const key = parseInt(keyStr.substr("Card ".length), 10);

            const [winningNumbers, cardNumbers] = numbers.split(" | ")

            const card = {
                winningNumbers: parseNumbers(winningNumbers.trim()),
                cardNumbers: parseNumbers(cardNumbers.trim()),
            };

            cards.set(key, card);

            return cards;
        }, new Map());
};

const parseNumbers = (numberString) => {
    return numberString.split(/\s+/)
        .map((val) => {
            return parseInt(val.trim(), 10);
        })
}
