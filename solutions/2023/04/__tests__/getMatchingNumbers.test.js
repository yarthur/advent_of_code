import { describe, it, expect } from "vitest";
import getMatchingNumbers from "../getMatchingNumbers.js";
import { scratchcards, matchingNumbers } from "./testInput.js";

describe("getMatchingNumbers", () => {
    it("gets the card numbers that match the winning numbers of the scratchcard", () => {
        Array.from(scratchcards.values())
            .forEach((card, index) => {
                expect(getMatchingNumbers(card).sort()).toStrictEqual(matchingNumbers[index].sort());
            });
    });
});
