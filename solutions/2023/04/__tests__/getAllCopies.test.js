import { describe, it, expect } from "vitest";
import getAllCopies from "../getAllCopies.js";
import { scratchcards, copies } from "./testInput.js";

describe("getAllCopies", () => {
    it("gets all copies of all the scratchcards", () => {
        expect(getAllCopies(scratchcards)).toEqual(copies);
    });
});
