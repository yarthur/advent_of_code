import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input } from "./testInput.js";

describe("2023, day 4", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(13);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toEqual(30);
    });
});
