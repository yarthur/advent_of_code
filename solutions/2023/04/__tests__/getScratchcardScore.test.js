import { describe, it, expect } from "vitest";
import getScratchcardScore from "../getScratchcardScore.js";
import { matchingNumbers, scores } from "./testInput.js";

describe("getScratchcardScore", () => {
    it("calculates the score of a given scratchcard", () => {
        matchingNumbers.forEach((card, index) => {
            expect(getScratchcardScore(card)).toEqual(scores[index]);
        });
    });
});
