/**
 * @type {string}
 */
export const input = `Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11`;

/**
 * @type { 2023.04.scratchcards }
 */
export const scratchcards = new Map([
    [
        1,
        {
            winningNumbers: [41, 48, 83, 86, 17],
            cardNumbers: [83, 86,  6, 31, 17,  9, 48, 53],
        }
    ],
[
    2,
    { 
        winningNumbers: [13, 32, 20, 16, 61],
        cardNumbers: [61, 30, 68, 82, 17, 32, 24, 19],
    }
],
[
    3,
    {
        winningNumbers: [1, 21, 53, 59, 44],
        cardNumbers: [69, 82, 63, 72, 16, 21, 14, 1],
    }
],
[
    4,
    {
        winningNumbers: [41, 92, 73, 84, 69],
        cardNumbers: [59, 84, 76, 51, 58,  5, 54, 83],
    }
],
[
    5,
    {
        winningNumbers: [87, 83, 26, 28, 32],
        cardNumbers: [88, 30, 70, 12, 93, 22, 82, 36],
    }
],
[
    6,
    {
        winningNumbers: [31, 18, 13, 56, 72],
        cardNumbers: [74, 77, 10, 23, 35, 67, 36, 11],
    }
],
])

export const matchingNumbers = [
    [48, 83, 17, 86],
    [32, 61],
    [1, 21],
    [84],
    [],
    [],
];

export const scores = [8, 2, 2, 1, 0, 0];

export const copies = new Map([
    [1, 1],
    [2, 2],
    [3, 4],
    [4, 8],
    [5, 14],
    [6, 1],
]);
