import { describe, it, expect } from "vitest";
import getScratchcards from "../getScratchcards.js";
import { input, scratchcards } from "./testInput.js";

describe("getScratchcards", () => {
    it("parses an input string into a collection of scratchcards", () => {
        expect(getScratchcards(input)).toStrictEqual(scratchcards);
    });
});
