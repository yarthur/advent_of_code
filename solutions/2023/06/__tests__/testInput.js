/**
 * @type { string }
 */
export const input = `Time:      7  15   30
Distance:  9  40  200`;

/**
 * @type { Array< 2023.06.RaceData > }
 */
export const races = [
    {
        time: 7,
        distance: 9,
    },
    {
        time: 15,
        distance: 40,
    },
    {
        time: 30,
        distance: 200,
    },
];

export const kernedRace = [
    {
        time: 71530,
        distance: 940200,
    },
];

/**
 * @type { Array< number > }
 */
export const winningScenarios = [4, 8, 9, 71503];
