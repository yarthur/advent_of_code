import { describe, it, expect } from "vitest";
import getRaces from "../getRaces.js";
import { input, races, kernedRace } from "./testInput.js";

describe("getRaces", () => {
    it("parses the input string into race data", () => {
        expect(getRaces(input)).toStrictEqual(races);
    });

    it("parses the input string into race data after fixing kerning", () => {
        expect(getRaces(input, true)).toStrictEqual(kernedRace);
    });
})


