import { describe, it, expect } from "vitest";
import findNumberOfWinningScenarios from "../findNumberOfWinningScenarios.js";
import { races, kernedRace, winningScenarios } from "./testInput.js";

describe("findNumberOfWinningScenarios", () => {
    it("finds the number of winning scenarios for a given race", () => {
        [
            ...races,
            ...kernedRace,
        ].forEach((race, index) => {
            expect(findNumberOfWinningScenarios(race)).toBe(winningScenarios[index]);
        });
    });
});
