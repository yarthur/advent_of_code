/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 6](https://adventofcode.com/2023/day/6/).
 * @namespace 06
 * @summary undefined
 * @memberof 2023
 */

import getRaces from "./getRaces.js";
import findNumberOfWinningScenarios from "./findNumberOfWinningScenarios.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.06
 * @param { string } input The puzzle's input string.
 * @returns { number } The product of each race's possible winning outcomes.
 */
export const part1 = (input) => {
    const races = getRaces(input);
    const winningScenarios = races.map((race) => {
        return findNumberOfWinningScenarios(race);
    });

    return winningScenarios.reduce((prod, wS) => {
        return prod * wS;
    }, 1);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.06
 * @param { string } input The puzzle's input string.
 * @returns { number }
 */
export const part2 = (input) => {
    const [kernedRace] = getRaces(input, true);

    return findNumberOfWinningScenarios(kernedRace);
};
