/**
 * @function findNumberOfWinningScenarios
 * @summary Finds the number of winning scenarios to be had in a race.
 * @memberof 2023.06
 * @param { 2023.06.RaceData } race - The Race Data for a given race.
 * @returns { number } The number of winning scenarios for that race.
 *
 * Determines the winning scenarios of a race by starting in the "middle". We
 * determine the midpoint of the race (by time), and work our way out... once
 * we find a losing scenario, we're done.
 */
const findNumberOfWinningScenarios = (race) => {
    const halfTime = race.time / 2;

    let charge = Math.floor(halfTime);
    let move = Math.ceil(halfTime);

    // if charge === move, there won't be an inverse race scenario, so we
    // start at -1 to account for that.
    let wins = (charge === move) ? -1 : 0;

    while (move * charge > race.distance) {
        // 2 wins because the inverse scenario (charge and move values
        // flipped) will also result in a win.
        wins += 2;
        
        // set up next scenario
        charge -= 1;
        move += 1;
    }

    return wins;
};

export default findNumberOfWinningScenarios;
