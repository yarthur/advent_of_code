/**
 * @typedef { object } RaceData
 * @summary Data representation of a race
 * @memberof 2023.06
 * @property { number } time - The amount of time the race took fix the kerning first.
 * @property { number } distance - The distance the record holder travelled
 */

/**
 * @function getRaces
 * @summary Parses the puzzle's input string into race data
 * @memberof 2023.06
 * @param { string } input - The puzzle's input string.
 * @param { boolean } fixKerning = false - Flag to determine if we should
 * @returns { Array< 2023.06.RaceData > } An array of race data
 */
const getRaces = (input, fixKerning = false) => {
    const [time, distances] = input.split("\n")
        .reduce((races, data) => {
            let parsedData = data.split(/\s+/)
                .slice(1);

            if (fixKerning === true) {
                parsedData = [parsedData.join("")];
            }

            parsedData = parsedData.map((datum) => {
                return parseInt(datum, 10);
            });

            races.push(parsedData);

            return races;
        }, []);

    return time.reduce((races, raceTime, raceIndex) => {
        races.push({
            time: raceTime,
            distance: distances[raceIndex] || 0,
        });

        return races;
    }, []);
};

export default getRaces;
