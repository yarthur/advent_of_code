/**
 * @function rankHands
 * @summary Returns a set of hands in ranked order.
 * @memberof 2023.07
 * @param { Array< 2023.07.Hand > } hands - A collection of Hands.
 */
export default (hands) => {
    let ranked = [];

    for (let typeCode = 7; typeCode > 0; typeCode -= 1) {
        ranked = [
            ...ranked,
            ...hands.filter((hand) => {
                return hand.typeCode === typeCode;
            })
            .sort(sortByCards)
        ];
    }

    return ranked;
};

/**
 * @function sortByCards
 * @summary Sorts a pair of hands by comparing cards in the hands.
 * @memberof 2023.07
 * @private
 * @param { 2023.07.Hand } handA - The first hand to compare.
 * @param { 2023.07.Hand } handB - The second hand to compare.
 * @returns { number } The difference between the first two different cards of each hand.
 */
const sortByCards = (handA, handB) => {
    /**
     * @function
     * @private
     * @param { Array<number> } cardsA - The cards of the first hand.
     * @param { Array<number> } cardsB - The cards of the second hand.
     * @returns { number } The difference between the first two different cards of each hand.
    */
    const sort = (cardsA, cardsB) => {
        const cardA = cardsA.shift() || 0;
        const cardB = cardsB.shift() || 0;

        if (cardA === cardB && cardA !== undefined) {
            return sort(cardsA, cardsB);
        }

        return cardB - cardA;
    };

    return sort([...handA.cards], [...handB.cards]);
};
