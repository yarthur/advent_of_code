/**
 * @function getTypeCode
 * @summary Returns the code for the "type" of hand.
 * @memberof 2023.07
 * @param { Array<number> } cards - The cards in the hand.
 * @param { boolean } jacks = true - Flag to indicate if the hand has Jacks (true) or Jokers (false).
 * @returns { number } The type code for the hand.
 * Type Code Key:
 *     1: "High card",
 *     2: "One pair",
 *     3: "Two pair",
 *     4: "Three of a kind",
 *     5: "Full house",
 *     6: "Four of a kind",
 *     7: "Five of a kind",
 */
export default (cards, jacks = true) => {
    const hand = cards.reduce((map, card) => {
        const count = map.get(card) || 0;
        map.set(card, count + 1);
        return map;
    }, new Map());

    const counts = Array.from(hand.entries())
        .filter(([key]) => {
            return jacks || key !== 1;
        }).map((entry) => {
            return entry[1];
        });

    let jokerCount = 0;

    if (jacks === false) {
        jokerCount += hand.get(1) || 0;
    }

    let code = 0;

    switch (counts.length) {
        case 1: 
            code = 7;
            break;

        case 2:
            code = counts.includes(4 - jokerCount) ? 6 : 5;
            break;

        case 3:
            code = counts.includes(3 - jokerCount) ? 4 : 3;
            break;

        case 4:
            code = 2;
            break;

        case 5:
            code = 1;
            break;

        default: 
            code = jokerCount === 5 ? 7 : 0;
            break;
    }

    return code;
};
