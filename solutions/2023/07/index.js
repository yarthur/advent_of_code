/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 7](https://adventofcode.com/2023/day/7/).
 * @namespace 07
 * @summary undefined
 * @memberof 2023
 */

import getHands from "./getHands.js";
import rankHands from "./rankHands.js";
import calculateTotalWinnings from "./calculateTotalWinnings.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.07
 * @param { string } input The puzzle's input string.
 * @returns { number }
 */
export const part1 = (input) => {
    const unranked = getHands(input);
    const ranked = rankHands(unranked);
    
    return calculateTotalWinnings(ranked);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.07
 * @param { string } input The puzzle's input string.
 * @returns { number }
 */
export const part2 = (input) => {
    const unranked = getHands(input, false);
    const ranked = rankHands(unranked);

    return calculateTotalWinnings(ranked);
};
