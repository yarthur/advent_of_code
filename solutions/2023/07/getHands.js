import getTypeCode from "./getTypeCode.js";

/**
 * @function getFaceCards
 * @summary Produces a list of values for the face cards of the deck.
 * @memberof 2023.07
 * @private
 * @param { boolean } jacks = true - Flag to indicate that "J" is a Jack (true) or Joker (false)
 * @returns { object } The key/value pairs for the face card values.
 */
const getFaceCards = (jacks = true) => {
    return {
        T: 10,
        J: jacks ? 11 : 1,
        Q: 12,
        K: 13,
        A: 14
    };
};

/**
 * @function getHands
 * @summary Parses the Input String into a collection of Hands.
 * @memberof 2023.07
 * @param { string } input The puzzle's input string.
 * @param { boolean } jacks = true - Flag to indicate 
 * @returns { Array< 2023.07.Hand > } A collection of Hands.
 */
export default (input, jacks = true) => {
    const handStrings = input.split("\n");

    return handStrings.map((str) => {
        let parts = str.split(" ");
        const faceCards = getFaceCards(jacks);

        const cards = parts[0].split("")
            .map((card) => {
                if (isNaN(parseInt(card, 10))) {
                    return faceCards[card] || 0;
                }

                return parseInt(card, 10);
            });

        const bid = parseInt(parts[1], 10);

        const typeCode = getTypeCode(cards, jacks);

        return { cards, bid, typeCode };
    });
};

/**
 * @typedef { Array< number number number number number > } CardsInHand
 * @summary Numerical representation of the cards in a given hand.
 * @memberof 2023.07
 */

/**
 * @typedef { object } Hand
 * @summary The data for a given hand.
 * @memberof 2023.07
 * @property { 2023.07.CardsInHand } cards - The cards that make up the hand.
 * @property { number } bid - The bid given for the hand.
 * @property { number } typeCode - The code representing the hand's type (pair, three of a kind, etc).
 */
