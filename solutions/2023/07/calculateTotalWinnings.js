/**
 * @function calculateTotalWinnings
 * @summary Calculates the total winnings for a game of camelCards.
 * @memberof 2023.07
 * @param { Array< 2023.07.Hand > } rankedHands - The collection of hands, sorted by rank.
 * @returns { number } The calculated total winnings of the game.
 */
export default (rankedHands) => {
    let totalWinnings = 0;

    while(rankedHands.length > 0) {
        let rank = rankedHands.length;
        let hand = rankedHands.shift();

        totalWinnings += hand.bid * rank;
    }

    return totalWinnings;
};
