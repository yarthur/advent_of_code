import { describe, it, expect } from "vitest";
import getHands from "../getHands.js";
import { input, hands, handsWithJokers } from "./testInput.js";

describe("getHands", () => {
    it("parses the input string into a collection of hands", () => {
        expect(getHands(input)).toStrictEqual(hands);
    });

    it("parses the input string into a collection of hands with Jokers", () => {
        expect(getHands(input, false)).toStrictEqual(handsWithJokers);
    });
});
