import { describe, it, expect } from "vitest";
import calculateTotalWinnings from "../calculateTotalWinnings.js";
import { rankedHands, rankedHandsWithJokers } from "./testInput.js";

describe("calculateTotalWinnings", () => {
    it("calculates the total winnings of a collection of hands", () => {
        expect(calculateTotalWinnings(rankedHands)).toBe(6440);
    });

    it("calculates the total winnings of a collection of hands with jokers", () => {
        expect(calculateTotalWinnings(rankedHandsWithJokers)).toBe(5905);
    });
});

