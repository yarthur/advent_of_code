/**
 * @type {string}
 */
export const input = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`;

export const hands = [
    {
        cards: [3, 2, 10, 3, 13],
        bid: 765,
        typeCode: 2,
    },
    {
        cards: [10, 5, 5, 11, 5],
        bid: 684,
        typeCode: 4,
    },
    {
        cards: [13, 13, 6, 7, 7],
        bid: 28,
        typeCode: 3,
    },
    {
        cards: [13, 10, 11, 11, 10],
        bid: 220,
        typeCode: 3,
    },
    {
        cards: [12, 12, 12, 11, 14],
        bid: 483,
        typeCode: 4
    },
];

export const rankedHands = [
    {
        cards: [12, 12, 12, 11, 14],
        bid: 483,
        typeCode: 4
    },
    {
        cards: [10, 5, 5, 11, 5],
        bid: 684,
        typeCode: 4,
    },
    {
        cards: [13, 13, 6, 7, 7],
        bid: 28,
        typeCode: 3,
    },
    {
        cards: [13, 10, 11, 11, 10],
        bid: 220,
        typeCode: 3,
    },
    {
        cards: [3, 2, 10, 3, 13],
        bid: 765,
        typeCode: 2,
    },
];

export const handsWithJokers = [
    {
        cards: [3, 2, 10, 3, 13],
        bid: 765,
        typeCode: 2,
    },
    {
        cards: [10, 5, 5, 1, 5],
        bid: 684,
        typeCode: 6,
    },
    {
        cards: [13, 13, 6, 7, 7],
        bid: 28,
        typeCode: 3,
    },
    {
        cards: [13, 10, 1, 1, 10],
        bid: 220,
        typeCode: 6,
    },
    {
        cards: [12, 12, 12, 1, 14],
        bid: 483,
        typeCode: 6
    },
];

export const rankedHandsWithJokers = [
    {
        cards: [13, 10, 1, 1, 10],
        bid: 220,
        typeCode: 6,
    },
    {
        cards: [12, 12, 12, 1, 14],
        bid: 483,
        typeCode: 6
    },
    {
        cards: [10, 5, 5, 1, 5],
        bid: 684,
        typeCode: 6,
    },
    {
        cards: [13, 13, 6, 7, 7],
        bid: 28,
        typeCode: 3,
    },
    {
        cards: [3, 2, 10, 3, 13],
        bid: 765,
        typeCode: 2,
    },
];

export const sampleTypeCodes = [
    [[1, 2, 3, 4, 5], 1],
    [[1, 1, 2, 3, 4], 2],
    [[1, 1, 2, 2, 3], 3],
    [[1, 1, 1, 2, 3], 4],
    [[1, 1, 1, 2, 2], 5],
    [[1, 1, 1, 1, 2], 6],
    [[1, 1, 1, 1, 1], 7],
];

export const sampleTypeCodesWithJokers = [
    [[2, 3, 4, 5, 6], 1],
    [[2, 2, 3, 4, 5], 2],
    [[1, 2, 3, 4, 5], 2],
    [[2, 2, 3, 3, 4], 3],
    [[2, 2, 2, 3, 4], 4],
    [[1, 2, 2, 3, 4], 4],
    [[1, 1, 2, 3, 4], 4],
    [[2, 2, 2, 3, 3], 5],
    [[1, 2, 2, 3, 3], 5],
    [[2, 2, 2, 2, 3], 6],
    [[1, 2, 2, 2, 3], 6],
    [[1, 1, 2, 2, 3], 6],
    [[1, 1, 1, 2, 3], 6],
    [[2, 2, 2, 2, 2], 7],
    [[1, 2, 2, 2, 2], 7],
    [[1, 1, 2, 2, 2], 7],
    [[1, 1, 1, 2, 2], 7],
    [[1, 1, 1, 1, 2], 7],
    [[1, 1, 1, 1, 1], 7],
]

