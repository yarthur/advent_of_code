import { describe, it, expect } from "vitest";
import getTypeCode from "../getTypeCode.js";
import { sampleTypeCodes, sampleTypeCodesWithJokers } from "./testInput.js";

describe("getTypeCode", () => {
    it("sets the type code for a given hand", () => {
        sampleTypeCodes.forEach(([cards, typeCode]) => {
            expect(getTypeCode(cards)).toBe(typeCode);
        })
    });

    it("sets the type code for a given hand with jokers", () => {
        sampleTypeCodesWithJokers.forEach(([cards, typeCode]) => {
            expect(getTypeCode(cards, false)).toBe(typeCode);
        })
    });
});
