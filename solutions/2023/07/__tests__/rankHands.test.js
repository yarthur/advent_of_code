import { describe, it, expect } from "vitest";
import rankHands from "../rankHands.js";
import { hands, rankedHands } from "./testInput.js";

describe("rankHands", () => {
    it("Returns a set of hands in ranked order", () => {
        expect(rankHands(hands)).toStrictEqual(rankedHands);
    });
})
