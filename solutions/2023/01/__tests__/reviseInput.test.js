import { describe, it, expect } from "vitest";
import reviseInput from "../reviseInput.js";

describe("reviseInput", () => {
    it("finds spelled-out numbers in the Input String, and replaces them with digits", () => {
        const input = `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`;
        const expected =`t2o1n9e
e8t2ot3e
abco1e2t3exyz
xt2o1e3f4r
4n9ee8ts7n2
zo1e8t234
7pqrsts6xteen`;

        expect(reviseInput(input)).toEqual(expected);
    });
})
