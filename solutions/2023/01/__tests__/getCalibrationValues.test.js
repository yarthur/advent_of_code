import { describe, it, expect } from "vitest";
import getCalibrationValues from "../getCalibrationValues.js";
import { input } from "./testInput.js";

describe("getCalibrationValues", () => {
    it("parses the input string into a collection of calibration values", () => {
        const expected = [12, 38, 15, 77];

        expect(getCalibrationValues(input)).toEqual(expected);
    });
})
