import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input } from "./testInput.js";

describe("2023, day 1", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(142);
    });

    it("solves for part 2", () => {
        const input = `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`;

        expect(part2(input)).toBe(281);
    });
});
