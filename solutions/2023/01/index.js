/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 1](https://adventofcode.com/2023/day/1/).
 * @namespace 01
 * @summary Trebuchet?!
 * @memberof 2023
 */

import getCalibrationValues from "./getCalibrationValues.js";
import reviseInput from "./reviseInput.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all of the calibration values.
 */
export const part1 = (input) => {
    const calibrationValues = getCalibrationValues(input);

    return calibrationValues.reduce((total, value) => {
        return total + value;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.01
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all of the calibration values.
 */
export const part2 = (input) => {
    const revisedInput = reviseInput(input);

    return part1(revisedInput);
};
