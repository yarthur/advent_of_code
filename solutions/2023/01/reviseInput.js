/**
 * @function reviseInput
 * @summary Finds spelled-out numbers in the Input String, and replaces them with digits.
 * @memberof 2023.01
 * @param { string } input - The (original) Input String.
 * @returns { string } The (revised) Input String.
 */
export default (input) => {
    const numberStrings = [
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ];

    return input.split("\n")
        .map((line) => {

            let newLine = ""
            for (newLine; line.length > 0; newLine) {
                numberStrings.forEach((str, num) => {

                    line = line.replace(new RegExp(`^${str}`), `${str.slice(0,1)}${num}${str.slice(-1)}`);
                });
                
                let [first, ...rest] = line.split("");

                newLine += first;
                line = rest.join("");
            }

            console.log(newLine);
            return newLine;
        })
        .join("\n");
}
