/**
 * @function getCalibrationValues
 * @summary Parses an Input String into a collection of Callibration Values.
 * @memberof 2023.01
 * @param { string } input - The Input String.
 * @returns { Array<number> } The collection of Calibration Values.
 */
export default (input) => {
    return input.split("\n")
        .map((line) => {
            const chars = line.split("");

            const digits = chars.filter((char) => {
                return parseInt(char, 10);
            });

            const value = digits[0] + [...digits].reverse()[0];

            return parseInt(value, 10);
        });
};
