import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input } from "./testInput.js";

describe("2023, day 2", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(8);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toBe(2286);
    });
});
