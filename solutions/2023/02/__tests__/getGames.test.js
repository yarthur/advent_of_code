import { describe, it, expect } from "vitest";
import getGames from "../getGames.js";
import { input, games } from "./testInput.js";

describe("getGames", () => {
    it("parses a puzzle input string into a collection of games", () => {
        expect(getGames(input)).toEqual(games);
    });
});
