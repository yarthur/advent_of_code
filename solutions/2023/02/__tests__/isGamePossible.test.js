import { describe, it, expect } from "vitest";
import isGamePossible from "../isGamePossible.js";
import { maxCubes } from "./testInput.js";

describe("isGamePossible", () => {
    it("determines whether a game could be possible given a max quantity of cubes of each color", () => {
        const cubes = new Map([
            [ "red", 12 ],
            [ "green", 13 ],
            [ "blue", 14 ],
        ]);

        const expected = [true, true, false, false, true];

        maxCubes.forEach((game, index) => {
            expect(isGamePossible(game, cubes)).toEqual(expected[index]);
        })
    })
})
