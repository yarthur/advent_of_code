/**
 * @type {string}
 */
export const input = `Game1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;

export const games = [
    [
        new Map ([
            [ "blue", 3 ],
            [ "red", 4 ],
        ]),
        new Map([
            [ "red", 1 ],
            [ "green", 2 ],
            [ "blue", 6 ],
        ]),
        new Map([
            ["green", 2 ],
        ]),
    ],
    [
        new Map([
            [ "blue", 1 ],
            [ "green", 2 ],
        ]),
        new Map([
            [ "green", 3 ],
            [ "blue",  4 ],
            [ "red", 1 ]
        ]),
        new Map([
            [ "green", 1 ],
            [ "blue", 1 ],
        ]),
    ],
    [
        new Map([
            [ "green", 8 ],
            [ "blue", 6 ],
            [ "red", 20 ]
        ]),
        new Map([
            [ "blue", 5 ],
            [ "red", 4 ],
            [ "green", 13 ],
        ]),
        new Map([
            [ "green", 5 ],
            [ "red", 1 ],
        ]),
    ],
    [
        new Map([
            [ "green", 1 ],
            [ "red", 3 ],
            [ "blue", 6 ],
        ]),
        new Map([
            [ "green", 3 ],
            [ "red", 6 ],
        ]),
        new Map([
            [ "green", 3 ],
            [ "blue", 15 ],
            [ "red", 14 ],
        ]),
    ],
    [
        new Map([
            [ "red", 6 ],
            [ "blue", 1 ],
            [ "green", 3 ],
        ]),
        new Map([
            [ "blue", 2 ],
            [ "red", 1 ],
            [ "green", 2 ],
        ]),
    ],
];

export const maxCubes = [
    new Map ([
        [ "blue", 6 ],
        [ "green", 2 ],
        [ "red", 4 ],
    ]),
    new Map([
        [ "blue",  4 ],
        [ "green", 3 ],
        [ "red", 1 ]
    ]),
    new Map([
        [ "blue", 6 ],
        [ "green", 13 ],
        [ "red", 20 ]
    ]),
    new Map([
        [ "blue", 15 ],
        [ "green", 3 ],
        [ "red", 14 ],
    ]),
    new Map([
        [ "blue", 2 ],
        [ "green", 3 ],
        [ "red", 6 ],
    ]),
];

export const powers = [48, 12, 1560, 630, 36];
