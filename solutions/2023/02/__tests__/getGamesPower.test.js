import { describe, it, expect } from "vitest";
import getGamesPower from "../getGamesPower.js";
import { maxCubes, powers } from "./testInput.js";

describe("getGamesPower", () => {
    it("calculates the game's power", () => {
        maxCubes.forEach((game, index) => {
            expect(getGamesPower(game)).toEqual(powers[index]);
        });
    });
});
