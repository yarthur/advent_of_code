import { describe, it, expect } from "vitest";
import getMaximumCubes from "../getMaximumCubes.js";
import { games, maxCubes } from "./testInput.js";

describe("getMaximumCubes", () => {
    it("finds the Maximum Cubes exposed in the samples", () => {
        games.forEach((game, index) => {
            expect(getMaximumCubes(game)).toStrictEqual(maxCubes[index]);
        });
    });
});
