/**
 * @function getGamesPower
 * @summary Calculates the Game's Power.
 * @memberof 2023.02
 * @param { Map } game - The Game being caluclated.
 * @returns { number } The Power value of the Game.
 */
export default (game) => {
    return Array.from(game.values())
        .reduce((power, value) => {
            return power * value;
        }, 1);
};
