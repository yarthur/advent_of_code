/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 2](https://adventofcode.com/2023/day/2/).
 * @namespace 02
 * @summary Cube Conundrum
 * @memberof 2023
 */

import getGames from "./getGames.js";
import getMaximumCubes from "./getMaximumCubes.js";
import isGamePossible from "./isGamePossible.js";
import getGamesPower from "./getGamesPower.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.02
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of the Game IDs that would be possible with the given mix of cubes.
 */
export const part1 = (input) => {
    const games = getGames(input);
    const maxPerGame = games.map(getMaximumCubes);
    const testLoad = new Map([
            [ "red", 12 ],
            [ "green", 13 ],
            [ "blue", 14 ],
        ]);
    const gamesPossible = maxPerGame.map((game) => {
        return isGamePossible(game, testLoad);
    });

    return gamesPossible.reduce((sum, possible, index) => {
        if (possible) {
            sum += index + 1;
        }

        return sum;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.02
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of the power of the sets of games.
 */
export const part2 = (input) => {
    const games = getGames(input);
    const maxPerGame = games.map(getMaximumCubes);
    const powers = maxPerGame.map(getGamesPower);

    return powers.reduce((total, power) => {
        return total + power;
    }, 0);
};
