/**
 * @function getGames
 * @summary Parses a Puzzle's Input String into a  collection of Games.
 * @memberof 2023.02
 * @param { string } input - The Puzzle's Input String.
 * @returns { Array<Map> } A parsed collection of Games.
 */
export default (input) => {
    return input.split("\n")
        .map((game) => {
            return game.substring(game.indexOf(": ") + 2)
                .split(";")
                .map((sample) => {
                    return sample.split(", ")
                        .reduce((sampleMap, color) => {
                            const [qty, name] = color.trim()
                                .split(" ");

                            sampleMap.set(name, parseInt(qty, 10));

                            return sampleMap;
                        }, new Map());
                });
        });
};
