/**
 * @function getMaximumCubes
 * @summary Finds the Maximum Cubes exposed in the samples.
 * @memberof 2023.02
 * @param { Array<Map> } game - The Game being examined.
 * @returns { Map } The most Cubes shown for each color in the Game.
 */
export default (game) => {
    return game.reduce((max, sample) => {
        Array.from(sample.entries())
            .forEach(([color, qty]) => {
                if (max.has(color) === false || max.get(color) < qty) {
                    max.set(color, qty);
                }

            });
        return max;
    }, new Map());
};
