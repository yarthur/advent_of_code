/**
 * @function isGamePossible
 * @summary Determines whether a game could be possible given a max quantity of cubes of each color.
 * @memberof 2023.02
 * @param { Map } game - The game being analyzed
 * @param { Map } cubes - The hypothetical load of cubes to test against.
 * @returns { boolean } Whether or not that game could be possible.
 */
export default (game, cubes) => {
    let possible = true;

    Array.from(cubes.entries())
        .forEach(([color, qty]) => {
            if (game.has(color) && game.get(color) > qty) {
                possible = false;
            }
        });

    return possible;
}
