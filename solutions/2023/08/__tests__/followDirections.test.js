import { describe, it, expect } from "vitest";
import followDirections from "../followDirections.js";
import { directionsA, directionsB } from "./testInput.js";

describe("followDirections", () => {
    it("follows the directions to reach the destination", () => {
        expect(followDirections(directionsA)).toBe(2);
        expect(followDirections(directionsB)).toBe(6);
    });
});
