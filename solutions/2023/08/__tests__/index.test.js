import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { inputA, inputB, inputGhosts } from "./testInput.js";

describe("2023, day 8", () => {
    it("solves for part 1", () => {
        expect(part1(inputA)).toBe(2);
        expect(part1(inputB)).toBe(6);
    });

    it("solves for part 2", () => {
        expect(part2(inputGhosts)).toBe(6);
    });
});
