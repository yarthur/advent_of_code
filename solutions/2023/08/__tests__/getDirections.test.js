import { describe, it, expect } from "vitest";
import getDirections from "../getDirections.js";
import { inputA, inputB, inputGhosts, directionsA, directionsB, directionsGhosts } from "./testInput.js";

describe("getDirections", () => {
    it("parses the input string into directions", () => {
        expect(getDirections(inputA)).toStrictEqual(directionsA);
        expect(getDirections(inputB)).toStrictEqual(directionsB);
        expect(getDirections(inputGhosts)).toStrictEqual(directionsGhosts);
    })
})
