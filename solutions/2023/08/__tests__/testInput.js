/**
 * @type {string}
 */
export const inputA = `RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)`;

/**
 * @type {string}
 */
export const inputB = `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`;

/**
 * @type { string }
 */
export const inputGhosts = `LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`;

/**
 * @type { 2023.08.Directions }
 */
export const directionsA = {
    instructions: ["R", "L"],
    map: new Map([
        ["AAA", [ "BBB", "CCC" ]],
        ["BBB", [ "DDD", "EEE" ]],
        ["CCC", [ "ZZZ", "GGG" ]],
        ["DDD", [ "DDD", "DDD" ]],
        ["EEE", [ "EEE", "EEE" ]],
        ["GGG", [ "GGG", "GGG" ]],
        ["ZZZ", [ "ZZZ", "ZZZ" ]],
    ]),
};

export const directionsB = {
    instructions: ["L", "L", "R"],
    map: new Map([
        ["AAA", ["BBB", "BBB"]],
        ["BBB", ["AAA", "ZZZ"]],
        ["ZZZ", ["ZZZ", "ZZZ"]],
    ]),
};

export const directionsGhosts = {
    instructions: ["L", "R"],
    map: new Map([
        ["11A", ["11B", "XXX"]],
        ["11B", ["XXX", "11Z"]],
        ["11Z", ["11B", "XXX"]],
        ["22A", ["22B", "XXX"]],
        ["22B", ["22C", "22C"]],
        ["22C", ["22Z", "22Z"]],
        ["22Z", ["22B", "22B"]],
        ["XXX", ["XXX", "XXX"]],
    ]),
};
