import { describe, it, expect } from "vitest";
import followGhostDirections from "../followGhostDirections.js";
import { directionsGhosts } from "./testInput.js";

describe("followGhostDirections", () => {
    it("follows the directions to reach the destination", () => {
        expect(followGhostDirections(directionsGhosts)).toBe(6);
    });
})
