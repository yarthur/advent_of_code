/**
 * @typedef { object } Directions
 * @summary The Directions to navigate the haunted wasteland.
 * @memberof 2023.08
 * @property { Array<string> } instructions - The Navigation Instructions.
 * @property { Map<string, Array<string, string>> } map - A map of the nodes of the wasteland.
 */

/**
 * @function getDirections
 * @summary Parses the Input String into Directions.
 * @memberof 2023.08
 * @param { string } input - The Input String.
 * @returns { 2023.08.Directions } The Directions to navigate the haunted wasteland.
 */
export default (input) => {
    const [instuctionsString, mapString] = input.split("\n\n");
    const instructions = instuctionsString.split("");

    const map = mapString.split("\n")
        .reduce((map, node) => {
            const nodeRE = /^([A-Z\d]+) = \(([A-Z\d]+), ([A-Z\d]+)\)$/;
            const match = node.match(nodeRE);

            map.set(match[1], [match[2], match[3]]);

            return map;
        }, new Map());

    return { instructions, map };
};
