/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 8](https://adventofcode.com/2023/day/8/).
 * @namespace 08
 * @summary undefined
 * @memberof 2023
 */

import getDirections from "./getDirections.js";
import followDirections from "./followDirections.js";
import followGhostDirections from "./followGhostDirections.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.08
 * @param { string } input The puzzle's input string.
 * @returns { number } - The number of steps required to reach the destination.
 */
export const part1 = (input) => {
    const directions = getDirections(input);

    return followDirections(directions);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.08
 * @param { string } input The puzzle's input string.
 * @returns { number } - The number of steps required to reach the destination.
 */
export const part2 = (input) => {
    const directions = getDirections(input);

    return followGhostDirections(directions);
};
