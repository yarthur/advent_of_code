/**
 * @function followDirections
 * @summary Follows the Directions to reach the destination.
 * @memberof 2023.08
 * @param { 2023.08.Directions } directions - The Directions to follow.
 * @param { string } startingLocation = "AAA" - The node we're starting from.
 * @param { RegExp } destinationPattern = /^ZZZ$/ - The pattern to match for our destination.
 * @returns { number } The number of steps it takes to get to the destination.
 */
export default (directions, startingLocation = "AAA", destinationPattern = /^ZZZ$/) => {
    const { instructions, map } = directions
    const instructionCount = instructions.length

    let currentNode = startingLocation;
    let stepCount = 0;

    while (destinationPattern.test(currentNode) === false) {
        let instruction = instructions[stepCount % instructionCount];
        let move = instruction === "L" ? 0 : 1;
        let options = map.get(currentNode);

        currentNode = options[move];
        stepCount += 1;
    }

    return stepCount;
};
