import followDirections from "./followDirections.js";
import { leastCommonMultiple } from "../../common-functions/math.js";

/**
 * @function getGhostDirections
 * @summary Follows the Directions for Ghosts to reach the destination.
 * @memberof 2023.08
 * @param { 2023.08.Directions } directions - The Directions to follow.
 * @returns { number } The number of steps it takes to get to the destination.
 */
export default (directions) => {
    const map = directions.map;

    return Array.from(map.keys())
        .filter((key) => {
            return key.endsWith("A");
        })
        .map((startingLocation) => {
            return followDirections(directions, startingLocation, /^[A-Z\d]*Z$/);
        })
        .reduce((lcm, pathLength) => {
            return leastCommonMultiple(lcm, pathLength);
        }, 1);
};
