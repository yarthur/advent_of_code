import isDigit from "./isDigit.js";

/**
 * @function retrieveNumber
 * @summary Pulls the number out of an array of strings.
 * @memberof 2023.03
 * @param { Array<string> } line - The array of strings the number gets pulled from.
 * @param { number } index - The index of a _single_ digit within the number.
 * @returns { Array<Array, number> } The updated line (digits replaced) and the parsed number value.
 */
export default (line, index) => {
    let digitIndex = index;
    let num = []

    while(isDigit(line[digitIndex - 1])) {
        digitIndex -= 1;
    }

    for(true; isDigit(line[digitIndex]); digitIndex += 1) {
        num.push(line[digitIndex]);
        line[digitIndex] = undefined;
    }

    return [line, parseInt(num.join(""), 10)];
}
