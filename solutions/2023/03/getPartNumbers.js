import eachInMatrix from "./eachInMatrix.js";
import isSymbol from "./isSymbol.js";
import eachAdjacent from "./eachAdjacent.js";
import isDigit from "./isDigit.js";
import retrieveNumber from "./retrieveNumber.js";

/**
 * @function getPartNumbers
 * @summary Retreives Part Numbers from a Schematic.
 * @memberof 2023.03
 * @param schematic
 * @input { 2023.03.Schematic } schematic
 * @returns { Array<number> } A collection of Part Numbers.
 */
export default (schematic) => {
    const partNumbers = [];

    eachInMatrix(schematic, ({point, coordinates: [x, y] }) => {
        if (isSymbol(point)) {
            eachAdjacent(schematic, [x, y], ({ point, coordinates: [newX, newY] }) => {
                if (isDigit(point)) {
                    const tmp = retrieveNumber(schematic[newY], newX);

                    schematic[newY] = tmp[0];
                    partNumbers.push(tmp[1]);
                }
            });
        }
    });

    return partNumbers;
}
