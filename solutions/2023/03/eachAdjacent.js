/**
 * @function eachAdjacen
 * @summary
 * @memberof 2023.03
 * @param { Array<Array> } matrix - The matrix
 * being examined.
 * @param { Array<number> } coordinates - The indices of the point's position in the matrix.
 * @param { Function } callback - The callback function to perform on each (defined) adjacent point.
 * @returns { void }
 */
export default (matrix, [x, y], callback) => {
    [y -1, y, y + 1].forEach((adjY) => {
        [x - 1, x, x + 1].forEach((adjX) => {
            // Skip if point is undefined ("outside" defined matrix).
            if (matrix[adjY]?.[adjX] === undefined) {
                return;
            }

            // Skip the original point we're looking around.
            if (x === adjX && y === adjY) {
                return;
            }

            callback({
                matrix,
                point: matrix[adjY][adjX],
                coordinates: [adjX, adjY],
            });
        })
    })
}
