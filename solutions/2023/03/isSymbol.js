/**
 * @function isSymbol
 * @summary Evaluates whether a string character is a Symbol or not.
 * @memberof 2023.03
 * @param { string } char - The character being evaluated.
 * @returns { boolean } The `true`/`false` result of the evaluation.
 */
export default (char) => {
    // if the character position is outside the bounds of the schematic
    if (char === undefined) {
        return false;
    }

    // if the character is "."
    if (char === ".") {
        return false;
    }

    // if the character is a digit
    if (/\d/.test(char)) {
        return false;
    }

    return true;
}

