/**
 * @typedef { Array<Array<string>> } Schematic
 * @summary The schematic representation of a gondola lift engine.
 * @memberof 2023.03
 */

/**
 * @function getSchematic
 * @summary Parses an Input String into a Schematic.
 * @memberof 2023.03
 * @param input
 * @input { string } input - An Input String.
 * @returns { 2023.03.Schematic } The engine's Schematic.
 */
export default (input) => {
    return input.split("\n")
        .map((line) => {
            return line.split("");
        });
};
