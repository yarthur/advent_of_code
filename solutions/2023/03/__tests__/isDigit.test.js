import { describe, it, expect } from "vitest";
import isDigit from "../isDigit.js";

const cases = [
    ["*", false],
    [undefined, false],
    [".", false],
    ["5", true],
];

describe("isSymbol", () => {
    it("evaluates whether a string character is a Symbol or not", () => {
        cases.forEach(([input, expected]) => {
            expect(isDigit(input)).toEqual(expected);
        });
    });
});
