import { describe, it, expect } from "vitest";
import getGearRatios from "../getGearRatios.js";
import { schematic, gearRatios } from "./testInput.js";

describe("getGearRatios", () => {
    it("retreives gear ratios from a schematic", () => {
        const actual = getGearRatios(schematic);

        actual.forEach((act) => {
            expect(gearRatios.includes(act)).toEqual(true);
        });

        gearRatios.forEach((pN) => {
            expect(actual.includes(pN)).toEqual(true);
        });
    });
})


