import { describe, it, expect } from "vitest";
import eachAdjacent from "../eachAdjacent.js";

const matrix = [
    [ "no", "no", "no", "no", "no" ],
    [ "no", "yes", "yes", "yes", "no" ],
    [ "no", "yes", "no", "yes", "no" ],
    [ "no", "yes", "yes", "yes", "no" ],
    [ "no", "no", "no", "no", "no" ],
];

const onEdge = [
    [ "no", "no", "no" ],
    [ "yes", "yes", "no" ],
    [ "no", "yes", "no" ],
    [ "yes", "yes", "no" ],
    [ "no", "no", "no" ],
]

describe("eachAdjacent", () => {
    it("applies the callback function to each point adjacent to the given point", () => {
        eachAdjacent(matrix, [2, 2], ({ point }) => {
            expect(point).toEqual("yes");
        });
    });

    it("skips undefined points", () => {
        eachAdjacent(onEdge, [0, 2], ({ point }) => {
            expect(point).toEqual("yes");
        });
    })
});
