import { describe, it, expect } from "vitest";
import getSchematic from "../getSchematic.js";
import { input, schematic } from "./testInput.js";

describe("getSchematic", () => {
    it("parses an input string into a schematic", () => {
        expect(getSchematic(input)).toEqual(schematic);
    });
});
