import { describe, it, expect } from "vitest";
import isSymbol from "../isSymbol.js";

const cases = [
    ["*", true],
    [undefined, false],
    [".", false],
    ["5", false],
];

describe("isSymbol", () => {
    it("evaluates whether a string character is a Symbol or not", () => {
        cases.forEach(([input, expected]) => {
            expect(isSymbol(input)).toEqual(expected);
        });
    });
});
