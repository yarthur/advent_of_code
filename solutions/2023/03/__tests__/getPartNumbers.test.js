import { describe, it, expect } from "vitest";
import getPartNumbers from "../getPartNumbers.js";
import { schematic, partNumbers } from "./testInput.js";

describe("getPartNumbers", () => {
    it("retreives part numbers from a schematic", () => {
        const actual = getPartNumbers(schematic);

        actual.forEach((act) => {
            expect(partNumbers.includes(act)).toEqual(true);
        });

        partNumbers.forEach((pN) => {
            expect(actual.includes(pN)).toEqual(true);
        });
    });
})


