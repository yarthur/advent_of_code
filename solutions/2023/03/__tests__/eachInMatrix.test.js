import { describe, it, expect } from "vitest";
import eachInMatrix from "../eachInMatrix.js";

const matrix = [
    [0, 1, 2, 3],
    [4, 5],
    [6, 7, 8],
    [9],
];

describe("eachInMatrix", () => {
    it("traverses each value in a Matrix (Array of Arrays)", () => {
        let expected = -1;

        eachInMatrix(matrix, ({ point }) => {
            expected += 1;

            expect(point).toEqual(expected);
        });
    });
});
