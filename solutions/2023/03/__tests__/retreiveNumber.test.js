import { describe, it, expect } from "vitest";
import retrieveNumber from "../retrieveNumber.js";

const test = [".", "9", "9", "9", "*"];
const expected = [
    [ ".", undefined, undefined, undefined, "*"],
    999,
];

describe("retrieveNumber", () => {
    it("pulls the number out of an array of strings", () => {
        [1, 2, 3].forEach((index) => {
            expect(retrieveNumber([...test], index)).toStrictEqual(expected);
        });
    });
});
