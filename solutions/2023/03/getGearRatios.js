import eachInMatrix from "./eachInMatrix.js";
import eachAdjacent from "./eachAdjacent.js";
import isDigit from "./isDigit.js";
import retrieveNumber from "./retrieveNumber.js";

/**
 * @function getGearRatios
 * @summary Retreives Gear Ratios from a Schematic.
 * @memberof 2023.03
 * @param schematic
 * @input { 2023.03.Schematic } schematic
 * @returns { Array<number> } A collection of Gear Ratios.
 */
export default (schematic) => {
    const gearRatios = [];

    eachInMatrix(schematic, ({point, coordinates: [x, y] }) => {
        if (point === "*") {
            const tempSchematic = schematic.map((line) => {
                return [...line];
            });
            const tempNumbers = [];

            eachAdjacent(tempSchematic, [x, y], ({ point, coordinates: [newX, newY] }) => {
                if (isDigit(point)) {
                    const tmp = retrieveNumber(tempSchematic[newY], newX);

                    tempSchematic[newY] = tmp[0];
                    tempNumbers.push(tmp[1]);
                }
            });

            if (tempNumbers.length === 2) {
                schematic = tempSchematic;
                gearRatios.push(tempNumbers[0] * tempNumbers[1]);
            }
        }
    });

    return gearRatios;
}
