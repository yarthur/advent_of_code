/**
 * @function isDigit
 * summary Evaluates whether a string character is a symbol or not.
 * @memberof 2023.03
 * @param { string } char - The character being evaluated.
 * @returns { boolean } The results of the character's evaluation.
 */
export default (char) => {
    return /\d/.test(char);
};
