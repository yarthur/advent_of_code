/**
 * @function eachInMatrix
 * @summary Traverses each value in a Matrix (Array of Arrays).
 * @memberof 2023.03
 * @param { Array<Array> } matrix - The matrix being traversed.
 * @param { Function } callback - The user-provided callback function.
 * @returns { void }
 */
export default (matrix, callback) => {
    matrix.forEach((row, y) => {
        row.forEach((point, x) => {
            callback({
                matrix,
                point,
                coordinates: [x, y]
            });
        });
    });
};
