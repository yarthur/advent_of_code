/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 3](https://adventofcode.com/2023/day/3/).
 * @namespace 03
 * @summary Gear Ratios
 * @memberof 2023
 */

import getSchematic from "./getSchematic.js";
import getPartNumbers from "./getPartNumbers.js";
import getGearRatios from "./getGearRatios.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.03
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all of the part numbers in the engine schematic.
 */
export const part1 = (input) => {
    const schematic = getSchematic(input);
    const partNumbers = getPartNumbers(schematic);

    return partNumbers.reduce((total, num) => {
        return total + num;
    }, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.03
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of all the gear ratios in the engine schematic.
 */
export const part2 = (input) => {
    const schematic = getSchematic(input);
    const gearRatios = getGearRatios(schematic);

    return gearRatios.reduce((total, num) => {
        return total + num;
    }, 0);
};
