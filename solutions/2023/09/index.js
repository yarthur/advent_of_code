/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 9](https://adventofcode.com/2023/day/9/).
 * @namespace 09
 * @summary undefined
 * @memberof 2023
 */

import getReport from "./getReport.js";
import findOutsideValues from "./findOutsideValues.js";
import { add } from "../../common-functions/math.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.09
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of the next values if each datum.
 */
export const part1 = (input) => {
    const report = getReport(input);

    return report.map((environmentData) => {
        return findOutsideValues(environmentData)[1];
    })
    .reduce(add, 0);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.09
 * @param { string } input The puzzle's input string.
 * @returns { number } The sum of the previous values if each datum.
 */
export const part2 = (input) => {
    const report = getReport(input);

    return report.map((environmentData) => {
        return findOutsideValues(environmentData)[0];
    })
    .reduce(add, 0);
};
