/**
 * @function getReport
 * @summary Parses the Input String into an OASIS Report.
 * @memberof 2023.09
 * @param { string } input - The Input String.
 * @returns { Array< Array< number > > } The OASIS Report.
 */
export default (input) => {
    return input.split("\n")
        .map((environmentalData) => {
            return environmentalData.split(" ")
                .map((value) => {
                    return parseInt(value, 10);
                });
        });
};
