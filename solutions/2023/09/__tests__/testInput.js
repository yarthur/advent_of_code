/**
 * @type {string}
 */
export const input = `0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45`;

/**
 * @type { Array< Array< number > > }
 */
export const report = [
    [0, 3, 6, 9, 12, 15],
    [1, 3, 6, 10, 15, 21],
    [10, 13, 16, 21, 30, 45],
];

/**
 * @type { Array< Array< number, number > > }
 */
export const outsideValues = [
    [ -3, 18 ],
    [ 0, 28 ],
    [ 5, 68 ],
];
