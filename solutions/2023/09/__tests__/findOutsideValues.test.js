import { describe, it, expect } from "vitest";
import findOutsideValues from "../findOutsideValues.js";
import { report, outsideValues } from "./testInput.js";


describe("findOutsideValues", () => {
    it("finds the values before and after the given values of the environmental data", () => {
        report.forEach((environmentalData, index) => {
            expect(findOutsideValues(environmentalData)).toStrictEqual(outsideValues[index]);
        });
    });
});
