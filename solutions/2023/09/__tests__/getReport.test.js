import { describe, it, expect } from "vitest";
import getReport from "../getReport.js";
import { input, report } from "./testInput.js";

describe("getReport", () => {
    it("parses the input string into an oasis report", () => {
        expect(getReport(input)).toStrictEqual(report);
    });
});
