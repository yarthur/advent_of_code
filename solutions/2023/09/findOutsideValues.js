/**
 * @function findOutsideValues
 * @summary Finds the Values before and after the given values of the Environmental Data.
 * @memberof 2023.09
 * @param { Array< number > } environmentalData - Data from the OASIS Report.
 * @returns { Array< number > } The previous and next Values.
 */
const findOutsideValues = (environmentalData) => {
    let [previous, ...rest] = environmentalData;

    const diffs = rest.map((value) => {
        const diff = value - previous;

        previous = value;

        return diff;
    });

    const uniqueValues = new Set(diffs);

    if (uniqueValues.size === 1 && uniqueValues.has(0) === true) {
        return [
            environmentalData.slice(1)[0],
            environmentalData.slice(-1)[0],
        ];
    }

    const [ previousDiff, nextDiff ] = findOutsideValues(diffs);

    return [
        environmentalData.slice(0)[0] - previousDiff,
        environmentalData.slice(-1)[0] + nextDiff,
    ];
};

export default findOutsideValues;
