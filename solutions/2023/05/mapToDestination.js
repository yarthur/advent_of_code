/**
 * @function mapToDest
 * @summary Maps a Source Category to a Destination Category.
 * @memberof 2023.05
 * @param { number } source - The Source Category being mapped.
 * @param { Array<2023.05.MapData> } map - The map that informs where the source should end up.
 * @returns { number } The Destination Category for the Source.
 */
export default (source, map) => {
    return map.reduce((destination, { destinationStart, sourceStart, range }) => {
        const offset = source - sourceStart;

        if (offset >= 0 && offset < range) {
            return destinationStart + offset;
        }

        return destination;
    }, source + 0);
};
