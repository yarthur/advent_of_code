import { describe, it, expect } from "vitest";
import findLowestInRange from "../mapSeedRanges.js";
import { almanac } from "./testInput.js";

describe("findLowestInRange ", () => {
    it ("finds the seed journey in a range of seeds with the lowest location", () => {
        const { seeds, destinations, maps } = almanac;
        const lowest = findLowestInRange(seeds[0], seeds[1], destinations, maps);

        expect(lowest.location).toBe(46);
    });
})
