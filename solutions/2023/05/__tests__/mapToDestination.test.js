import { describe, it, expect } from "vitest";
import mapToDestination from "../mapToDestination.js";
import { almanac, journeys } from "./testInput.js";

describe("mapToDest", () => {
    it("maps a source category to a destination category", () => {
        const map = almanac.maps.get("soil");
        almanac.seeds.forEach((seed, index) => {
            expect(mapToDestination(seed, map)).toEqual(journeys[index].soil);
        });
    });
});
