import { describe, it, expect } from "vitest";
import mapJourney from "../mapJourney.js";
import { almanac, journeys } from "./testInput.js";

describe("mapToDest", () => {
    it("maps a source category to a destination category", () => {
        almanac.seeds.forEach((seed, index) => {
            expect(mapJourney(seed, almanac.destinations, almanac.maps)).toEqual(journeys[index]);
        });
    });
});
