import { describe, it, expect } from "vitest";
import getAlmanac from "../getAlmanac.js";
import { input, almanac } from "./testInput.js";

describe("getAlmanac", () => {
    it("parses an input string into an almanac", () => {
        expect(getAlmanac(input)).toEqual(almanac);
    });
});

