/**
 * @type {string}
 */
export const input = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`;

export const almanac = {
    seeds: [79, 14, 55, 13],
    destinations: new Map([
        ["seed", "soil"],
        ["soil", "fertilizer"],
        ["fertilizer", "water"],
        ["water", "light"],
        ["light", "temperature"],
        ["temperature", "humidity"],
        ["humidity", "location"],
    ]),
    maps: new Map([
        [
            "soil",
            [
                {
                    destinationStart: 50,
                    sourceStart: 98,
                    range: 2,
                },
                {
                    destinationStart: 52,
                    sourceStart: 50,
                    range: 48
                },
            ],
        ],
        [
            "fertilizer",
            [
                {
                    destinationStart: 0,
                    sourceStart: 15,
                    range: 37,
                },
                {
                    destinationStart: 37,
                    sourceStart: 52,
                    range: 2,
                },
                {
                    destinationStart: 39,
                    sourceStart: 0,
                    range: 15,
                },
            ],
        ],
        [
            "water",
            [
                {
                    destinationStart: 49,
                    sourceStart: 53,
                    range: 8,
                },
                {
                    destinationStart: 0,
                    sourceStart: 11,
                    range: 42,
                },
                {
                    destinationStart: 42,
                    sourceStart: 0,
                    range: 7,
                },
                {
                    destinationStart: 57,
                    sourceStart: 7,
                    range: 4,
                },
            ],
        ],
        [
            "light",
            [
                {
                    destinationStart: 88,
                    sourceStart: 18,
                    range: 7,
                },
                {
                    destinationStart: 18,
                    sourceStart: 25,
                    range: 70,
                },
            ],
        ],
        [
            "temperature",
            [
                {
                    destinationStart: 45,
                    sourceStart: 77,
                    range: 23,
                },
                {
                    destinationStart: 81,
                    sourceStart: 45,
                    range: 19,
                },
                {
                    destinationStart: 68,
                    sourceStart: 64,
                    range: 13,
                },
            ],
        ],
        [
            "humidity",
            [
                {
                    destinationStart: 0,
                    sourceStart: 69,
                    range: 1,
                },
                {
                    destinationStart: 1,
                    sourceStart: 0,
                    range: 69,
                },
            ],
        ],
        [
            "location",
            [
                {
                    destinationStart: 60,
                    sourceStart: 56,
                    range: 37,
                },
                {
                    destinationStart: 56,
                    sourceStart: 93,
                    range: 4,
                },
            ]
        ]
    ]),
};

export const journeys = [
    {
        seed: 79,
        soil: 81,
        fertilizer: 81,
        water: 81,
        light: 74,
        temperature: 78,
        humidity: 78,
        location: 82,
    },
    {
        seed: 14,
        soil: 14,
        fertilizer: 53,
        water: 49,
        light: 42,
        temperature: 42,
        humidity: 43,
        location: 43,
    },
    {
        seed: 55,
        soil: 57,
        fertilizer: 57,
        water: 53,
        light: 46,
        temperature: 82,
        humidity: 82,
        location: 86,
    },
    {
        seed: 13,
        soil: 13,
        fertilizer: 52,
        water: 41,
        light: 34,
        temperature: 34,
        humidity: 35,
        location: 35,
    },
]
