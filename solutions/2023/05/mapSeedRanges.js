import mapJourney from "./mapJourney.js";

/**
 * @function findLowestInRange
 * @summary Finds the lowest location in a range of seeds.
 * @memberof 2023.05
 * @param { number } start - The starting value of the range.
 * @param { number } length - The length of the current range.
 * @param { Map< string, string > } destinations - The destination of each step.
 * @param { Map<string, 2023.05.MapData> } maps - The map data for each destination.
 * @returns { object } The seed with the lowest location value within the range.
 */
export default (start, length, destinations, maps) => {
    let lowest = mapJourney(start, destinations, maps);

    for (let seed = start + 1; seed < start + length; seed += 1) {
        const test = mapJourney(seed, destinations, maps);

        if (test.location < lowest.location) {
            lowest = test;
        }
    }

    return lowest;
};
