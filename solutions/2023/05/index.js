/**
 * My solutions for the
 * [2023 edition of Advent of Code, day 5](https://adventofcode.com/2023/day/5/).
 * @namespace 05
 * @summary If You Give A Seed A Fertilizer
 * @memberof 2023
 */

import getAlmanac from "./getAlmanac.js";
import mapJourney from "./mapJourney.js";
import findLowestInRange from "./mapSeedRanges.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2023.05
 * @param { string } input The puzzle's input string.
 * @returns { number } The lowest Location Number that corresponds to any of the Initial Seed Numbers.
 */
export const part1 = (input) => {
    const { seeds, destinations, maps } = getAlmanac(input);

    const journeys = seeds.reduce((journeys, seed) => {
        journeys.push(mapJourney(seed, destinations, maps));

        return journeys;
    }, []);
    
    const sorted = journeys.sort((a, b) => {
        return a.location - b.location;
    });

    return sorted[0].location;
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2023.05
 * @param { string } input The puzzle's input string.
 * @returns { number } The lowest Location Number that corresponds to any of the Initial Seed Numbers.
 */
export const part2 = (input) => {
    const { seeds, destinations, maps } = getAlmanac(input); 
    let lowest = null;

    while (seeds.length >= 2) {
        let start = seeds.shift();
        let length = seeds.shift();
        let test = findLowestInRange(start, length, destinations, maps);

        if (lowest === null) {
            lowest = test;
        }

        if (test.location < lowest.location) {
            lowest = test;
        }
    }

    return lowest.location;
};
