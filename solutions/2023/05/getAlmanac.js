/**
 * @typedef { object } Almanac
 * @summary The parsed almanac data
 * @memberof 2023.05
 * @property { Array< number > } seeds - The Seed data of the almanac
 * @property { Array< string, string > } destinations - The destination of each step.
 * @property { Map<string, object> } maps - The map data for each destination.
 */

/**
 * @function getAlmanac
 * @summary Parses an Input String into an Almanac
 * @memberof 2023.05
 * @param { string } input - The Input String being parsed.
 * @returns { 2023.05.Almanac } The Almanac data.
 */
export default (input) => {
    const [ seedString, ...mapsStrings] = input.split("\n\n");

    const seeds = seedString.substring("seeds: ".length)
        .split(" ")
        .map((str) => {
            return parseInt(str, 10);
        })

    const [ destinations, maps ] = mapsStrings.reduce(
        ([destinations, maps], mS) => {
            const [srcToDest, ...mapData] = mS.split("\n");
            const [ , src, dest] = srcToDest.match(/^(\w+)-to-(\w+)/);

            const mapArr = mapData.map((mD) => {
                const [ destinationStart, sourceStart, range] = mD.split(" ");
                return {
                    destinationStart: parseInt(destinationStart, 10),
                    sourceStart: parseInt(sourceStart, 10),
                    range: parseInt(range, 10),
                };
            });

            destinations.set(src, dest);
            maps.set(dest, mapArr);

            return [destinations, maps];
        },
        [new Map(), new Map()]
    );

    return {
        seeds,
        destinations,
        maps,
    };
}
