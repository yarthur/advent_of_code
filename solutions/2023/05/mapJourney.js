import mapToDestination from "./mapToDestination.js";

/**
 * @function mapJourney
 * @summary Maps the full journey of a seed through the Almanac's Maps.
 * @memberof 2023.05
 * @param { number } source - The source which is having its journey mapped.
 * @param { Map< string, string > } destinations - The relationship between sources and destinations.
 * @param { Map< string, Array< 2023.05.MapData > > } maps - The maps to each destination along the journey.
 * @param { string } [sourceName = "seed"] The source we're currently mapping from.
 * @param { object } [journey = {}] The journey so far.
 * @returns { object } The journey that was mapped.
 */
const mapJourney = (source, destinations, maps, sourceName = "seed", journey = {}) => {
    journey[sourceName] = source;

    if (destinations === undefined) {
        console.log(sourceName, destinations);
    }

    if (destinations.has(sourceName)) {
        const destinationName = destinations.get(sourceName);
        const destinationMap = maps.get(destinationName);
        const destination = mapToDestination(source, destinationMap);

        return mapJourney(destination, destinations, maps, destinationName, journey);
    }

    return journey;
};

export default mapJourney;
