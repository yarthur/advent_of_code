export default (input) => {
    return input
        .trim()
        .split("\n\n")
        .reduce((cameraArray, cameraData) => {
            const [idString, ...image] = cameraData.split("\n");

            const id = parseInt(idString.match(/\d+/)[0], 10);

            const borders = [
                image[0],
                "",
                image[image.length - 1].split("").reverse().join(""),
                "",
            ];

            image.forEach((imageRow) => {
                borders[1] = imageRow.slice(0, 1) + borders[1];
                borders[3] += imageRow.slice(-1);
            });

            cameraArray.set(id, { image, borders });

            return cameraArray;
        }, new Map());
};
