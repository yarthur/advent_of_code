import getReceived from "./getReceived.js";
import matchBorders from "./matchBorders.js";
import findCorners from "./findCorners.js";

export const part1 = (input) => {
    const imageMap = getReceived(input);

    const matchedBorders = matchBorders(imageMap);

    const corners = findCorners(matchedBorders);

    return corners.reduce((product, corner) => {
        return product * corner;
    }, 1);
};
