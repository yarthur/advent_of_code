export default (imageMap) => {
    const matchedBorders = new Map();

    imageMap.forEach(({ borders: myBorders }, myId) => {
        const myMatchedBorders = matchedBorders.has(myId)
            ? matchedBorders.get(myId)
            : {};

        imageMap.forEach(({ borders: otherBorders }, otherId) => {
            const myKeys = Object.keys(myMatchedBorders);

            // If we have 4 matched borders, do nothing further.
            if (myKeys.length === 4) {
                return;
            }

            // filter out "myId" from the rest
            if (myId === otherId) {
                return;
            }

            // if we've already matched this border, do nothing further.
            if (myKeys.includes(otherId)) {
                return;
            }

            const otherMatchedBorders = matchedBorders.has(otherId)
                ? matchedBorders.get(otherId)
                : {};

            myBorders.forEach((myBorder) => {
                if (otherBorders.includes(myBorder)) {
                    myMatchedBorders[otherId] = myBorder;
                    otherMatchedBorders[myId] = myBorder;

                    matchedBorders.set(otherId, otherMatchedBorders);

                    return;
                }

                const myBorderReversed = [...myBorder].reverse().join("");
                if (otherBorders.includes(myBorderReversed)) {
                    myMatchedBorders[otherId] = myBorderReversed;
                    otherMatchedBorders[myId] = myBorder;
                }
            });
        });

        matchedBorders.set(myId, myMatchedBorders);
    });

    return matchedBorders;
};
