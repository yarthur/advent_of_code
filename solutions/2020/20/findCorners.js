export default (matchedBorders) => {
    return Array.from(matchedBorders.entries()).reduce(
        (corners, [id, borders]) => {
            if (Object.keys(borders).length === 2) {
                corners.push(id);
            }

            return corners;
        },
        []
    );
};
