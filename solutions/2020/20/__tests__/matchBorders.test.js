import { describe, it, expect } from "vitest";
import matchBorders from "../matchBorders.js";
import { received, matchedBorders } from "./testInput";

describe("getReceived", () => {
    it("correctly matches up the borders of the individual images", () => {
        expect(matchBorders(received)).toEqual(matchedBorders);
    });
});
