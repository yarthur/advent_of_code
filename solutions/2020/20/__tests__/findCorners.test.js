import { describe, it, expect } from "vitest";
import findCorners from "../findCorners.js";
import { matchedBorders, corners } from "./testInput.js";

describe("findCorners", () => {
    it("finds the corners of the image array", () => {
        expect(findCorners(matchedBorders)).toEqual(corners);
    });
});
