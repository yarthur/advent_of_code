import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1 } from "../index.js";

describe("2020, day 20", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(20899048083289);
    });
});
