import { describe, it, expect } from "vitest";
import getReceived from "../getReceived.js";
import { input, received } from "./testInput";

describe("getReceived", () => {
    it("correctly parses the received input to a more usable state.", () => {
        expect(getReceived(input)).toEqual(received);
    });
});
