const playARound = (player1, player2) => {
    const p1Card = player1.shift();
    const p2Card = player2.shift();

    if (p1Card > p2Card) {
        player1.push(p1Card, p2Card);
    } else {
        player2.push(p2Card, p1Card);
    }

    return [player1, player2];
};
export default ([player1, player2]) => {
    while (player1.length > 0 && player2.length > 0) {
        [player1, player2] = playARound(player1, player2);
    }

    if (player2.length === 0) {
        return {
            winner: 1,
            deck: player1,
        };
    }

    return {
        winner: 2,
        deck: player2,
    };
};
