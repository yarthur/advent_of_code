import getReceived from "./getReceived.js";
import playGame from "./playGame.js";
import calculateScore from "./calculateScore.js";

export const part1 = (input) => {
    const startingDecks = getReceived(input);

    const winner = playGame(startingDecks);

    return calculateScore(winner.deck);
};
