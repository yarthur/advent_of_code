export default (deck) => {
    return deck.reverse().reduce((score, card, index) => {
        return score + card * (index + 1);
    }, 0);
};
