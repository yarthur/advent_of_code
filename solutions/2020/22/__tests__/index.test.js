import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1 } from "../index.js";

describe("2020, day 22", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(306);
    });
});
