import { describe, it, expect } from "vitest";
import { gameResult } from "./testInput.js";
import calculateScore from "../calculateScore.js";

describe("calculateScore", () => {
    it("calculates the score of the winning hand", () => {
        expect(calculateScore(gameResult.deck)).toEqual(306);
    });
});
