export const input = `Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`;

export const startingDecks = [
    [9, 2, 6, 3, 1],
    [5, 8, 4, 7, 10],
];

export const gameResult = {
    winner: 2,
    deck: [3, 2, 10, 6, 8, 5, 9, 4, 7, 1],
};
