import { describe, it, expect } from "vitest";
import { input, startingDecks } from "./testInput.js";
import getReceived from "../getReceived.js";

describe("getReceived", () => {
    it("formats the received input", () => {
        expect(getReceived(input)).toStrictEqual(startingDecks);
    });
});
