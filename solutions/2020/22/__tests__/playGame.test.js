import { describe, it, expect } from "vitest";
import { startingDecks, gameResult } from "./testInput.js";
import playGame from "../playGame.js";

describe("playGame", () => {
    it("plays the game and gives the winning deck", () => {
        expect(playGame(startingDecks)).toStrictEqual(gameResult);
    });
});
