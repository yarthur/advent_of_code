export default (input) => {
    const decks = input.split(/\n*Player [\d*]:\n/);
    return decks.reduce((formattedDecks, deck) => {
        if (deck === "") {
            return formattedDecks;
        }

        formattedDecks.push(
            deck.split("\n").map((card) => {
                return Number.parseInt(card, 10);
            })
        );

        return formattedDecks;
    }, []);
};
