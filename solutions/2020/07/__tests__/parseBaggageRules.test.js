import { describe, it, expect } from "vitest";
import { input, baggageRules } from "./testInput.js";
import parseBaggageRules from "../parseBaggageRules.js";

describe("parseBaggageRules", () => {
    it("returns declaration form answers", () => {
        expect(parseBaggageRules(input)).toEqual(baggageRules);
    });
});
