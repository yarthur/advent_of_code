import { describe, it, expect } from "vitest";
import { baggageRules } from "./testInput.js";
import findTotalPossibleContents from "../findTotalPossibleContents.js";

describe("findTotalPossibleContents", () => {
    it("returns the total possible contents", () => {
        expect(findTotalPossibleContents(baggageRules, "shiny gold")).toEqual(32);
    });
});
