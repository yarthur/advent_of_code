import { describe, it, expect } from "vitest";
import { baggageRules } from "./testInput.js";
import findPossibleContainers from "../findPossibleContainers.js";

describe("findPossibleContainers", () => {
    const expected = new Set(["bright white", "muted yellow", "dark orange", "light red"]);
    it("it finds the right possible containers", () => {
        expect(findPossibleContainers(baggageRules, "shiny gold")).toEqual(expected);
    });
});
