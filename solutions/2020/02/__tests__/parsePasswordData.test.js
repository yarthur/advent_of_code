import { describe, it, expect } from "vitest";
import { input, passwordData } from "./testInput.js";
import parsePasswordData from "../parsePasswordData.js";

describe("parsePasswordData", () => {
    it("formats puzzle input as expected", () => {
        expect(parsePasswordData(input)).toEqual(passwordData);
    });
});
