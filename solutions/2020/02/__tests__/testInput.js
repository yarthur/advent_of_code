export const input = `1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc`;

export const passwordData = [
    { floor: 1, ceiling: 3, letter: "a", password: "abcde" },
    { floor: 1, ceiling: 3, letter: "b", password: "cdefg" },
    { floor: 2, ceiling: 9, letter: "c", password: "ccccccccc" },
];
