import { describe, it, expect } from "vitest";
import { passwordData } from "./testInput.js";
import {
    validatePassword,
    validatePasswordStrictly,
} from "../validatePassword.js";

describe("validatePassword", () => {
    it("validates the passwords", () => {
        const actual = passwordData
            .filter(validatePassword)
            .map((pwd) => pwd.password);

        expect(actual).toStrictEqual(["abcde", "ccccccccc"]);
    });
});

describe("validatePasswordStrictly", () => {
    it("validates the passwords strictly", () => {
        const actual = passwordData
            .filter(validatePasswordStrictly)
            .map((pwd) => pwd.password);

        expect(actual).toStrictEqual(["abcde"]);
    });
});
