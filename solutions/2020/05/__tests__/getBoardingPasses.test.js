import { describe, it, expect } from "vitest";
import { input, boardingPasses } from "./testInput.js";
import getBoardingPasses from "../getBoardingPasses.js";

describe("getBoardingPasses", () => {
    it("returns boarding pass data", () => {
        expect(getBoardingPasses(input)).toEqual(boardingPasses);
    });
});
