import findNextDeparture from "./findNextDeparture.js";

/**
 * @function findSoonestDeparture
 * @summary Find the soonest departure time to get you to your destination.
 * @memberof 2020.13
 * @param { 2020.13.ShuttleData } shuttleData - Shuttle list and departure times.
 * @returns { 2020.13.soonestDeparture } Values around the soonest departure.
 */
export default ({ earliestDeparture, busList }) => {
    const departureSchedule = Array.from(busList.keys()).reduce(
        (schedule, busId) => {
            schedule.set(busId, findNextDeparture(earliestDeparture, busId));

            return schedule;
        },
        new Map()
    );

    const soonestDepartingBus = Array.from(departureSchedule.keys()).reduce(
        (soonest, busId) => {
            if (soonest === -1) {
                return busId;
            }

            if (departureSchedule.get(soonest) > departureSchedule.get(busId)) {
                return busId;
            }

            return soonest;
        },
        -1
    );

    return [soonestDepartingBus, departureSchedule.get(soonestDepartingBus)];
};
