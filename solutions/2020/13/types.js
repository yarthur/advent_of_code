/**
 * @typedef { object } ShuttleData
 * @summary Shuttle list and departure times.
 * @memberof 2020.13
 * @property { number } earliestDeparture - The earliest time you can depart from the station.
 * @property { Map<number, number> } busList - A list of Bus IDs and their offset values.
 */

/**
 * @typedef { Array } soonestDeparture
 * @summary Values around the soonest departure.
 * @memberof 2020.13
 * @property { number } 0 - The ID of the bus with the soonest departure.
 * @property { number } 1 - The departure time of
 */
