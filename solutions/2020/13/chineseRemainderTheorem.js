/**
 * @function chineseRemainderTheorem
 * @summary A function to compute the Chinese Reminder Theorem.
 * @memberof 2020.13
 * @param { Array } given - The known values; when solving for x = b % n, given === [ b, n ].
 * @param { number } given.0 - The dividend given.
 * @param { number } given.1 - The divisor given.
 * @returns { number } The result (I still don't really know how to describe it.).
 * @description A theorem which gives a unique solution to simultaneous linear congruences
 * with coprime moduli. In its basic form, the Chinese remainder theorem will
 * determine a number ppp that, when divided by some given divisors, leaves
 * given remainders.
 *
 * A pretty good explainer: <https://www.youtube.com/watch?v=zIFehsBHB8o>
 */
export default (given) => {
    const N = given.reduce((N, { 1: n }) => {
        return N * n;
    }, 1);

    const sigma = given.map(([bi, ni]) => {
        const Ni = N / ni;
        return {
            bi,
            Ni: Ni,
            xi: getInverse(Ni, ni),
        };
    });

    const x = sigma.reduce((x, { bi, Ni, xi }) => {
        const biNixi = BigInt(BigInt(bi) * BigInt(Ni) * BigInt(xi));
        return x + biNixi;
    }, BigInt(0));

    return x % BigInt(N);
};

/**
 * @function getInverse
 * @summary I don't rightly understand this. I absolutely snagged the code from somewhere else.
 * @memberof 2022.11
 * @private
 * @param { number } numerator - The Numerator. 🤷‍♂️
 * @param { number } remainder - The Remainder. 🤷‍♂️🤷‍
 * @returns { number } The inverse, I guess. 🤷‍♂️🤷‍🤷‍
 */
const getInverse = (numerator, remainder) => {
    const b = numerator % remainder;

    for (let inverse = 1; inverse < remainder; inverse += 1) {
        if ((b * inverse) % remainder === 1) {
            return inverse;
        }
    }

    return 1;
};
