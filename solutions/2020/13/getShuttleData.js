/**
 * @function getShuttleData
 * @summary Parse the puzzle input into the earliest possible departure time and bus IDs in service.
 * @memberof 2020.13
 * @param { string } input - The puzzle's input string.
 * @returns { 2020.13.ShuttleData } Shuttle list and departure times.
 */
export default (input) => {
    const [earliestDeparture, busRoster] = input.split("\n");

    const busList = busRoster.split(",").reduce((list, bus, busOffset) => {
        const busId = bus * 1;

        if (isNaN(busId) === false) {
            list.set(busId, busOffset);
        }
        return list;
    }, new Map());

    return {
        earliestDeparture: earliestDeparture * 1,
        busList,
    };
};
