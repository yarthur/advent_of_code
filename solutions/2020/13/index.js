/**
 * @namespace 2020
 */

/**
 * My solutions for the
 * [2020 edition of Advent of Code, day 13](https://adventofcode.com/2020/13/).
 * @namespace 13
 * @summary Shuttle Search
 * @memberof 2020
 */

import getShuttleData from "./getShuttleData.js";
import findSoonestDeparture from "./findSoonestDeparture.js";
import chineseRemainderTheorem from "./chineseRemainderTheorem.js";

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof 2020.13
 * @param { string } input - The input string.
 * @returns {number} The ID of the earliest bus you can take to the airport multiplied by the number of minutes you'll need to wait for that bus
 */
export const part1 = (input) => {
    const shuttleData = getShuttleData(input);

    const [soonestBus, departureTime] = findSoonestDeparture(shuttleData);

    return soonestBus * (departureTime - shuttleData.earliestDeparture);
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof 2020.13
 * @param { string } input - The input string.
 * @returns {number} The earliest timestamp such that all of the listed bus IDs depart at offsets matching their positions in the list.
 */
export const part2 = (input) => {
    const { busList } = getShuttleData(input);

    const busIds = Array.from(busList.keys());
    const scheduleSet = busIds.map((busId) => {
        const mod = busId - busList.get(busId);

        return [mod, busId];
    });

    return chineseRemainderTheorem(scheduleSet);
};
