import { describe, it, expect } from "vitest";
import { input, shuttleData } from "./testInput.js";
import getShuttleData from "../getShuttleData.js";

describe("getShuttleData", () => {
    it("parses the shuttle data correctly", () => {
        expect(getShuttleData(input)).toEqual(shuttleData);
    });
});
