import { describe, it, expect } from "vitest";
import chineseRemainderTheorem from "../chineseRemainderTheorem.js";

describe("chineseRemainderTheorem", () => {
    it("returns the correct value", () => {
        expect(
            chineseRemainderTheorem([
                [3, 5],
                [1, 7],
                [6, 8],
            ])
        ).toEqual(BigInt(78));
    });
});
