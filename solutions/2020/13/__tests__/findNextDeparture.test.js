import { describe, it, expect } from "vitest";
import { nextDepartures } from "./testInput.js";
import findNextDeparture from "../findNextDeparture.js";

describe("findNextDeparture", () => {
    it("finds the next departure after the given timestamp", () => {
        nextDepartures.forEach((expected, busId) => {
            expect(findNextDeparture(939, busId)).toBe(expected);
        });
    });
});
