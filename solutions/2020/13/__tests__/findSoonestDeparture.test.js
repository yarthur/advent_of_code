import { describe, it, expect } from "vitest";
import { shuttleData } from "./testInput.js";
import findSoonestDeparture from "../findSoonestDeparture.js";

describe("findSoonestDeparture", () => {
    it("finds the next bus to depart after timestamp", () => {
        expect(findSoonestDeparture(shuttleData)).toEqual([59, 944]);
    });
});
