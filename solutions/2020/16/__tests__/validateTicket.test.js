import { describe, it, expect } from "vitest";
import { ticketData, validatedTickets } from "./testInput.js";
import * as validateTicket from "../validateTicket.js";

const { ticketRules } = ticketData;

describe("validateTicket.default", () => {
    it("correctly validates a ticket", () => {
        validatedTickets.valid.forEach((expected) => {
            expect(
                validateTicket.default(expected.ticketInfo, ticketRules)
            ).toEqual(expected);
        });

        validatedTickets.invalid.forEach((expected) => {
            expect(
                validateTicket.default(expected.ticketInfo, ticketRules)
            ).toEqual(expected);
        });
    });
});

describe("validateTicket.batch", () => {
    const tickets = [ticketData.yourTicket, ...ticketData.nearbyTickets];

    it("properly validates a batch of tickets", () => {
        expect(validateTicket.batch(tickets, ticketRules)).toEqual(
            validatedTickets
        );
    });
});
