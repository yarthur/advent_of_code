import { describe, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1 } from "../index.js";

/**
 * This exercise did not give a final test scenario for part 2. 🤷‍♂️
 */

describe("2020, day 16", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(71);
    });
});
