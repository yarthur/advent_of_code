import { describe, it, expect } from "vitest";
import { ticketStructure } from "./testInput.js";
import getTicketStructure from "../getTicketStructure.js";

const { ticketRules } = ticketStructure.given;
const allTickets = [
    ticketStructure.given.yourTicket,
    ...ticketStructure.given.nearbyTickets,
];

describe("getTicketStructure", () => {
    it("determines the correct ticket structure based on given data", () => {
        expect(getTicketStructure(allTickets, ticketRules)).toEqual(
            ticketStructure.structure
        );
    });
});
