import { describe, it, expect } from "vitest";
import { input, ticketData } from "./testInput.js";
import getTicketData from "../getTicketData.js";

describe("getTicketData", () => {
    it("gets the ticket data from input", () => {
        expect(getTicketData(input)).toEqual(ticketData);
    });
});
