import { describe, it, expect } from "vitest";
import { decodedInitializations, memorySums } from "./testInput.js";
import sumMemory from "../sumMemory.js";

describe("sumMemory", () => {
    it("sums the values in memory", () => {
        Object.keys(decodedInitializations).forEach((key) => {
            expect(sumMemory(decodedInitializations[key])).toEqual(memorySums[key]);
        });
    });
});
