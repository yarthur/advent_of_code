import { describe, it, expect } from "vitest";
import { inputs, initializationPrograms } from "./testInput.js";
import getInitializationProgram from "../getInitializationProgram.js";

describe("getInitializationProgram", () => {
    it("gets the initialization program", () => {
        expect(getInitializationProgram(inputs.version1)).toEqual(
            initializationPrograms.version1
        );
    });
});
