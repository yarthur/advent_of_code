import { describe, it, expect } from "vitest";
import { inputs, memorySums } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 14", () => {
    it("solves for part 1", () => {
        expect(part1(inputs.version1)).toEqual(memorySums.version1);
    });

    it("solves for part 2", () => {
        expect(part2(inputs.version2)).toEqual(memorySums.version2);
    });
});
