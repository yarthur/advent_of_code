import { describe, it, expect } from "vitest";
import { initializationPrograms, decodedInitializations } from "./testInput.js";
import * as decodeInitialization from "../decodeInitialization.js";

describe("decodeInitialization", () => {
    Object.keys(decodeInitialization).forEach((version) => {
        describe(`${version}`, () => {
            it("successfully parses out initialization program", () => {
                expect(
                    decodeInitialization[version](initializationPrograms[version])
                ).toEqual(decodedInitializations[version]);
            });
        });
    });
});
