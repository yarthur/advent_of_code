import { describe, it, expect } from "vitest";
import { input, declarationForms } from "./testInput.js";
import getDeclarationForms from "../getDeclarationForms.js";

describe("getDeclarationForms", () => {
    it("returns declaration form answers", () => {
        expect(getDeclarationForms(input)).toEqual(declarationForms);
    });
});
