import { describe, it, expect } from "vitest";
import { input, navInstructions } from "./testInput.js";
import getNavInstructions from "../getNavInstructions.js";

describe("getNavInstructions", () => {
    it("returns formatted inputs", () => {
        expect(getNavInstructions(input)).toEqual(navInstructions);
    });
});
