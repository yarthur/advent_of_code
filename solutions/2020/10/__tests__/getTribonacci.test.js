import { describe, it, expect } from "vitest";
import getTribonacci from "../getTribonacci.js";

const answerKey = [
    [0, 0],
    [1, 1],
    [2, 1],
    [3, 2],
    [4, 4],
    [5, 7],
    [6, 13],
    [7, 24],
    [8, 44],
    [9, 81],
    [10, 149],
];

describe("getTribonacci", () => {
    it("generates the correct tribonacci value", () => {
        answerKey.forEach(([index, expected]) => {
            expect(getTribonacci(index)).toEqual(expected);
        });
    });
});
