import { describe, it, expect } from "vitest";
import { input, adapterChain as expected } from "./testInput.js";
import getAdapterChain from "../getAdapterChain.js";

describe("getAdapterChain", () => {
    it("returns formatted inputs", () => {
        expect(getAdapterChain(input)).toEqual(expected);
    });
});
