export default (input) => {
    const data = input.split("\n");

    return data.map((point) => {
        return point * 1;
    });
};
