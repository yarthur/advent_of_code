import { describe, it, expect } from "vitest";
import { data, decrypted } from "./testInput.js";
import decryptXmas from "../decryptXmas.js";

describe("decryptXmas", () => {
    it("decrypts the XMAS data until first weakpoint", () => {
        expect(decryptXmas(data, 5)).toStrictEqual(decrypted);
    });
});
