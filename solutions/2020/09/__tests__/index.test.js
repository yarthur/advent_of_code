import { describe, beforeEach, vi, afterEach, it, expect } from "vitest";
import { input } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 09", async () => {
    beforeEach(async () => {
        vi.mock('../decryptXmas.js', async (importOriginal) => {
            const original = await importOriginal();

            return {
                default: (data) => {
                    return original.default(data, 5);
                },
            };
        });
    })

    afterEach(() => {
        vi.restoreAllMocks();
    });

    it("solves for part 1", () => {
        expect(part1(input)).toEqual(127);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toEqual(62);
    });
});
