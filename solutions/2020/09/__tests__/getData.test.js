import { describe, it, expect } from "vitest";
import { input, data } from "./testInput.js";
import getData from "../getData.js";

describe("getData", () => {
    it("returns formatted boot code", () => {
        expect(getData(input)).toEqual(data);
    });
});
