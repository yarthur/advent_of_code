import { describe, it, expect } from "vitest";
import { received, validationResults } from "./testInput.js";
import checkMessage from "../checkMessage.js";

const { rules, messages } = received;

describe("checkMessage", () => {
    it("correctly validates the message", () => {
        messages.forEach((message, messageIndex) => {
            expect(checkMessage(message, rules)).toEqual(
                validationResults[messageIndex]
            );
        });
    });
});
