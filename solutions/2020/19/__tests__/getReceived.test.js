import { describe, it, expect } from "vitest";
import { input, received } from "./testInput.js";
import getReceived from "../getReceived.js";

describe("getReceived", () => {
    it("parses the received data", () => {
        expect(getReceived(input)).toEqual(received);
    });
});
