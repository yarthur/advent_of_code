import { describe, it, expect } from "vitest";
import { input, secondInput } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 19", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(2);
        expect(part1(secondInput)).toEqual(3);
    });

    it.todo("solves for part 2", () => {
        expect(part2(secondInput)).toEqual(12);
    });
});
