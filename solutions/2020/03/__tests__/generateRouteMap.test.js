import { describe, it, expect } from "vitest";
import { input, routeMap } from "./testInput.js";
import generateRouteMap from "../generateRouteMap.js";

describe("generateRouteMap", () => {
    it("formats puzzle input as expected", () => {
        expect(generateRouteMap(input)).toEqual(routeMap);
    });
});
