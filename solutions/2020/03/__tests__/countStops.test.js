import { describe, it, expect } from "vitest";
import { routeStops } from "./testInput.js";
import countStops from "../countStops.js";

describe("countStops", () => {
    it("counts the stops", () => {
        const expected = new Map([
            [".", 3],
            ["#", 7],
        ]);
        expect(countStops(routeStops)).toStrictEqual(expected);
    });
});
