import { describe, it, expect } from "vitest";
import { routeMap, routeStops } from "./testInput.js";
import mapTobogganRun from "../mapTobogganRun.js";

describe("mapTobogganRun", () => {
    it("maps the toboggan run correclty", () => {
        expect(mapTobogganRun(routeMap, 3, 1)).toStrictEqual(routeStops);
    });
});
