import { describe, it, expect } from "vitest";
import { bootCode, stepByStep } from "./testInput.js";
import executeBootInstruction from "../executeBootInstruction.js";

describe("executeBootInstruction", () => {
    it("correctly executes the instructions", () => {
        stepByStep.reduce(
            ([accumulator, bootLocation], [expectedAccum, expectedLoc]) => {
                let [receivedAccum, receivedLoc] = executeBootInstruction(
                    bootCode[bootLocation],
                    accumulator,
                    bootLocation
                );

                expect(receivedAccum).toEqual(expectedAccum);
                expect(receivedLoc).toEqual(expectedLoc);
                return [receivedAccum, receivedLoc];
            },
            [0, 0]
        );
    });
});
