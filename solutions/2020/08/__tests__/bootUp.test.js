import { describe, it, expect } from "vitest";
import { bootCode, corrected } from "./testInput.js";
import bootUp from "../bootUp.js";

describe("bootUp", () => {
    it("boots up to the point of looping", () => {
        expect(bootUp(bootCode)).toEqual([5, false]);
    });

    it("boots up successfully", () => {
        expect(bootUp(corrected)).toEqual([8, true]);
    });
});
