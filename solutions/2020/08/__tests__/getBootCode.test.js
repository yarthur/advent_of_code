import { describe, it, expect } from "vitest";
import { input, bootCode } from "./testInput.js";
import getBootCode from "../getBootCode.js";

describe("getBootCode", () => {
    it("returns formatted boot code", () => {
        expect(getBootCode(input)).toEqual(bootCode);
    });
});
