import { describe, it, expect } from "vitest";
import { coordinates } from "./testInput.js";
import * as coordinateUtils from "../coordinateUtils.js";

describe("coordinateUtils", () => {
    describe("decode", () => {
        it("correctly decodes a coordinate string", () => {
            coordinates.forEach(([coordinateString, expected]) => {
                expect(coordinateUtils.decode(coordinateString)).toEqual(expected);
            });
        });
    });

    describe("encode", () => {
        it("correctly encodes a coordinate obejct", () => {
            coordinates.forEach(([expected, coordinateObject]) => {
                expect(coordinateUtils.encode(coordinateObject)).toEqual(expected);
            });
        });
    });
});
