import { describe, it, expect } from "vitest";
import { input, initialState } from "./testInput.js";
import getInitialState from "../getInitialState.js";

describe("getInitialState", () => {
    it("gets the initial state of the pocket dimmension", () => {
        expect(getInitialState(input)).toEqual(initialState);
    });
});
