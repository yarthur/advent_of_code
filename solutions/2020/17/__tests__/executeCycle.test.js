import { describe, it, expect } from "vitest";
import { initialState } from "./testInput.js";
import executeCycle from "../executeCycle.js";

describe("executeCycle", () => {
    it("correctly models the dimmension after a cycle", () => {
        const expected = new Map([
            [1, 11],
            [2, 21],
            [3, 38],
        ]);

        let dimmension = initialState;

        for (let cycle = 1; cycle <= 3; cycle += 1) {
            dimmension = executeCycle(dimmension);

            expect(dimmension.size).toEqual(expected.get(cycle));
        }
    });
});
