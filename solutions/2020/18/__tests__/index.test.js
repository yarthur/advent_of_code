import { describe, it, expect } from "vitest";
import { input, answerKey } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 18", () => {
    it("solves part 1 of the puzzle", () => {
        const expected = answerKey.standard.reduce((exp, answer) => {
            return exp + answer;
        });

        expect(part1(input)).toEqual(expected);
    });

    it("solves part 2 of the puzzle", () => {
        const expected = answerKey.advanced.reduce((exp, answer) => {
            return exp + answer;
        });

        expect(part2(input)).toEqual(expected);
    });
});
