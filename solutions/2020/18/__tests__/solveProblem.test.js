import { describe, it, expect } from "vitest";
import { homework, answerKey } from "./testInput.js";
import * as solveProblem from "../solveProblem.js";

describe("solveProblem", () => {
    describe("default", () => {
        it("solves the problem correctly", () => {
            homework.forEach((problem, problemNumber) => {
                expect(solveProblem.default(problem)).toEqual(
                    answerKey.standard[problemNumber]
                );
            });
        });
    });

    describe("advanced", () => {
        it("solves the problem correctly using the advanced rules", () => {
            homework.forEach((problem, problemNumber) => {
                expect(solveProblem.advanced(problem)).toEqual(
                    answerKey.advanced[problemNumber]
                );
            });
        });
    });
});
