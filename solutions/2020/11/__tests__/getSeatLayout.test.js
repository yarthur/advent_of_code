import { describe, it, expect } from "vitest";
import { input, part1Changes } from "./testInput.js";
import getSeatLayout from "../getSeatLayout.js";

describe("getSeatLayout", () => {
    it("returns formatted inputs", () => {
        expect(getSeatLayout(input)).toEqual(part1Changes[0]);
    });
});
