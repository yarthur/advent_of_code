import { describe, it, expect } from "vitest";
import { input, passportBatch } from "./testInput.js";
import generatePassportBatch from "../generatePassportBatch.js";

describe("passportBatch", () => {
    it("returns passport data in a usable way", () => {
        expect(generatePassportBatch(input)).toEqual(passportBatch);
    });
});
