import { describe, it, expect } from "vitest";
import { input, part2Input, validPassports } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 04", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(2);
    });

    it("solves for part 2", () => {
        expect(part2(part2Input)).toEqual(validPassports.length);
    });
});
