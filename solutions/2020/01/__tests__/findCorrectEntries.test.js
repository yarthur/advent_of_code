import { describe, it, expect } from "vitest";
import { expenseReport } from "./testInput.js";
import findCorrectEntries, { findWithThree } from "../findCorrectEntries.js";

describe("findCorrectEntries", () => {
    it("finds the correct number of entries", () => {
        expect(findCorrectEntries(expenseReport)).toStrictEqual([1721, 299]);
    });
});

describe("findWithThree", () => {
    it("it finds the correct number of entries", () => {
        expect(findWithThree(expenseReport)).toStrictEqual([979, 366, 675]);
    });
});
