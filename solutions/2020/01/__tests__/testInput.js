export const input = `1721
979
366
299
675
1456`;

export const expenseReport = [1721, 979, 366, 299, 675, 1456];

export const testSolutionA = 514579;
export const testSolutionB = 241861950;
