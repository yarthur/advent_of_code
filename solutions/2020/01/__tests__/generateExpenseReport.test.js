import { describe, it, expect } from "vitest";
import { input, expenseReport } from "./testInput.js";
import generateExpenseReport from "../generateExpenseReport.js";

describe("generateExpenseReport", () => {
    it("formats puzzle input as expected", () => {
        expect(generateExpenseReport(input)).toEqual(expenseReport);
    });
});
