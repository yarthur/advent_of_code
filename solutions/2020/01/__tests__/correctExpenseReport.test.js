import { describe, it, expect } from "vitest";
import correctExpenseReport from "../correctExpenseReport.js";

describe("correctExpenseReport", () => {
    it("corrects the expense report correctly", () => {
        expect(correctExpenseReport([1721, 299])).toEqual(514579);
        expect(correctExpenseReport([979, 366, 675])).toEqual(241861950);
    });
});
