# 2020 — ⭐️: 41

My solutions for the
[2020 edition of Advent of Code](https://adventofcode.com/2020/)

## Solutions

-   [01](./01/index.js): ⭐️⭐️
-   [02](./02/index.js): ⭐️⭐️
-   [03](./03/index.js): ⭐️⭐️
-   [04](./04/index.js): ⭐️⭐️
-   [05](./05/index.js): ⭐️⭐️
-   [06](./06/index.js): ⭐️⭐️
-   [07](./07/index.js): ⭐️⭐️
-   [08](./08/index.js): ⭐️⭐️
-   [09](./09/index.js): ⭐️⭐️
-   [10](./10/index.js): ⭐️⭐️
-   [11](./11/index.js): ⭐️⭐️
-   [12](./12/index.js): ⭐️⭐️
-   [13](./13/index.js): ⭐️⭐️
-   [14](./14/index.js): ⭐️⭐️
-   [15](./15/index.js): ⭐️⭐️
-   [16](./16/index.js): ⭐️⭐️
-   [17](./17/index.js): ⭐️⭐️
-   [18](./18/index.js): ⭐️⭐️
-   [19](./19/index.js): ⭐️
-   [20](./20/index.js): ⭐️
-   [21](./21/index.js): ⭐️⭐️
-   [22](./22/index.js): ⭐️
