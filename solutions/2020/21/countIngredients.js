import gatherAllergens from "./gatherAllergens.js";

export default (food) => {
    const allergens = gatherAllergens(food);
    const allergenicIngredients = new Set();

    allergens.forEach((ingredients) => {
        ingredients.forEach((ingredient) => {
            allergenicIngredients.add(ingredient);
        });
    });

    return Array.from(food).reduce((count, { ingredients }) => {
        const nonAllergenicIngredients = ingredients.filter((ingredient) => {
            return allergenicIngredients.has(ingredient) === false;
        });

        return (count += nonAllergenicIngredients.length);
    }, 0);
};
