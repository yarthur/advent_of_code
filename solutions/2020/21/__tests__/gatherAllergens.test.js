import { describe, it, expect } from "vitest";
import { received, allergenList } from "./testInput.js";
import gatherAllergens from "../gatherAllergens.js";

describe("gatherAllergens", () => {
    it("gathers a list of unique allergens", () => {
        expect(gatherAllergens(received)).toStrictEqual(allergenList);
    });
});
