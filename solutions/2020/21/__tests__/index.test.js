import { describe, it, expect } from "vitest";
import { input, formattedAllergenList } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 21", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toEqual(5);
    });

    it("solves for part 2", () => {
        expect(part2(input)).toEqual(formattedAllergenList);
    });
});
