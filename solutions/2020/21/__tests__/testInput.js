export const input = `mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`;

export const received = new Set([
    {
        ingredients: ["mxmxvkd", "kfcds", "sqjhc", "nhms"],
        allergens: ["dairy", "fish"],
    },
    {
        ingredients: ["trh", "fvjkl", "sbzzf", "mxmxvkd"],
        allergens: ["dairy"],
    },
    {
        ingredients: ["sqjhc", "fvjkl"],
        allergens: ["soy"],
    },
    {
        ingredients: ["sqjhc", "mxmxvkd", "sbzzf"],
        allergens: ["fish"],
    },
]);

export const allergenList = new Map([
    ["dairy", ["mxmxvkd"]],
    ["fish", ["mxmxvkd", "sqjhc"]],
    ["soy", ["sqjhc", "fvjkl"]],
]);

export const refinedAllergenList = new Map([
    ["dairy", "mxmxvkd"],
    ["fish", "sqjhc"],
    ["soy", "fvjkl"],
]);

export const formattedAllergenList = "mxmxvkd,sqjhc,fvjkl";
