import { describe, it, expect } from "vitest";
import { received } from "./testInput.js";
import countIngredients from "../countIngredients.js";

describe("countIngredients", () => {
    it("counts the number of times a non-allergenic ingredient is listed", () => {
        expect(countIngredients(received)).toBe(5);
    });
});
