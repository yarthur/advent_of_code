import { describe, it, expect } from "vitest";
import { input, received } from "./testInput.js";
import getRecieved from "../getRecieved.js";

describe("getReceived", () => {
    it("formats the input", () => {
        expect(getRecieved(input)).toStrictEqual(received);
    });
});
