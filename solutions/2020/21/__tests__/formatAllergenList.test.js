import { describe, it, expect } from "vitest";
import { refinedAllergenList, formattedAllergenList } from "./testInput.js";
import formatAllergenList from "../formatAllergenList.js";

describe("formatAllergenList", () => {
    it("formats the allergen list", () => {
        expect(formatAllergenList(refinedAllergenList)).toBe(
            formattedAllergenList
        );
    });
});
