import { describe, it, expect } from "vitest";
import { allergenList, refinedAllergenList } from "./testInput.js";
import refineAllergenList from "../refineAllergenList.js";

describe("refineAllergens", () => {
    it("refines the list of allergenList", () => {
        expect(refineAllergenList(allergenList)).toStrictEqual(
            refinedAllergenList
        );
    });
});
