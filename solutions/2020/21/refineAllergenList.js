export default (allergenList) => {
    const refinedAllergenList = new Map();

    while (
        Array.from(refinedAllergenList).length !==
        Array.from(allergenList).length
    ) {
        Array.from(allergenList).forEach(([allergen, ingredients]) => {
            if (refinedAllergenList.has(allergen)) {
                return;
            }

            const refinedIngredients = ingredients.filter((ingredient) => {
                const identifiedIngredients = Array.from(
                    refinedAllergenList.values()
                );

                return identifiedIngredients.includes(ingredient) === false;
            });

            allergenList.set(allergen, refinedIngredients);

            if (refinedIngredients.length === 1) {
                refinedAllergenList.set(allergen, refinedIngredients[0]);
            }
        });
    }

    return refinedAllergenList;
};
