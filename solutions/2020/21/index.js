import getReceived from "./getRecieved.js";
import countIngredients from "./countIngredients.js";
import gatherAllergens from "./gatherAllergens.js";
import refineAllergenList from "./refineAllergenList.js";
import formatAllergenList from "./formatAllergenList.js";

export const part1 = (input) => {
    const food = getReceived(input);
    return countIngredients(food);
};

export const part2 = (input) => {
    const food = getReceived(input);
    const allergenList = gatherAllergens(food);
    const refinedAllergenList = refineAllergenList(allergenList);

    return formatAllergenList(refinedAllergenList);
};
