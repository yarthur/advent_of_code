export default (input) => {
    const list = input.split("\n");

    return list.reduce((set, food) => {
        const parseString = /([a-z ]*)(?: \(contains (.*)\))?$/;
        const parsed = parseString.exec(food);

        const ingredients = parsed[1].split(" ");
        const allergens = parsed[2] ? parsed[2].split(", ") : [];

        return set.add({ ingredients, allergens });
    }, new Set());
};
