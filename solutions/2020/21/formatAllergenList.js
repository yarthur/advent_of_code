export default (allergenList) => {
    const allergens = Array.from(allergenList.keys());

    return allergens
        .sort()
        .reduce((ingredients, allergen) => {
            ingredients.push(allergenList.get(allergen));

            return ingredients;
        }, [])
        .join(",");
};
