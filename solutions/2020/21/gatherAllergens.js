export default (food) => {
    return Array.from(food).reduce((gathered, { ingredients, allergens }) => {
        allergens.forEach((allergen) => {
            const potentialAllergens = gathered.get(allergen);

            if (potentialAllergens === undefined) {
                gathered.set(allergen, ingredients);
                return;
            }

            gathered.set(
                allergen,
                potentialAllergens.filter((pA) => {
                    return ingredients.includes(pA);
                })
            );
        });

        return gathered;
    }, new Map());
};
