import { describe, it, expect } from "vitest";
import { input, startingNumbers } from "./testInput.js";
import getStartingNumbers from "../getStartingNumbers.js";

describe("getStartingNumbers", () => {
    it("gets the starting numbers for the game", () => {
        expect(getStartingNumbers(input)).toEqual(startingNumbers);
    });
});
