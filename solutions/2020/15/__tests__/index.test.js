import { describe, it, expect } from "vitest";
import { testCases } from "./testInput.js";
import { part1, part2 } from "../index.js";

describe("2020, day 15", () => {
    it("solves for part 1", () => {
        testCases.forEach(({ input, part1: expected }) => {
            expect(part1(input)).toEqual(expected);
        });
    });

    it("solves for part 2", () => {
        testCases.forEach(({ input, part2: expected }) => {
            expect(part2(input)).toEqual(expected);
        });
    });
});
