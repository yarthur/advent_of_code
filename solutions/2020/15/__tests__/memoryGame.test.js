import { describe, it, expect } from "vitest";
import { startingNumbers } from "./testInput.js";
import memoryGame from "../memoryGame.js";

describe("memoryGame", () => {
    it("plays the game correctly", () => {
        expect(memoryGame(startingNumbers, 10)).toEqual(0);
    });
});
