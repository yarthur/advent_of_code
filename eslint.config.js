import js from "@eslint/js";
import jsdoc from "eslint-plugin-jsdoc";
import globals from "globals";
// import prettier from "eslint-plugin-prettier";

export default [
    js.configs.recommended,

    jsdoc.configs["flat/recommended"],

    // prettier.configs["recommended"],

    {
        ignores: ["docs/*"],
    },

    {
        linterOptions: {
            reportUnusedDisableDirectives: true,
        },
        languageOptions: {
            globals: {
                ...globals.worker,
            },
        },
        plugins: {
            jsdoc,
            // prettier,
        },
        rules: {
            "jsdoc/no-defaults": [
                "error"|"warn",
                {
                    noOptionalParamNames: false,
                }
            ]
        }
    },

    {
        files: ["bin/**/*.js", "lib/**/*.js"],
        languageOptions: {
            globals: {
                ...globals.node,
            },
        },
    },
];
