# Advent of Code _(advent_of_code)_

My solutions for the [Advent of Code](https://adventofcode.com) excercises.

I've been using this as a way to get better with Typescript, Pure Functions, and
my programming and problem-solving skills. It's a lot of fun, and I suggest you
give it a thow, as well!

## Install

Clone the repo:

```sh
git clone https://gitlab.com/yarthur/advent_of_code.git
```

Install the dependencies:

```sh
npm install
```

Off you go!

## Usage

Wanna see _all_ the solutions?

```sh
node lib/runPuzzles.js
```

Optionally, you can specify a year, or a year and a day.

```sh
// runs all solutions from 2020
node lib/runPuzzles 2020

// runs the solution for 2022, day 06
node lib/runPuzzles 2022, 06
```

## The Solutions

-   [2017](/solutions/2017) — ⭐️: 24
-   [2018](/solutions/2018) — ⭐️: 12
-   [2020](/solutions/2020) — ⭐️: 41
-   [2021](/solutions/2021) — ⭐️: 14
-   [2022](/solutions/2022) — ⭐️: 20
-   [2023](/solutions/2023) — ⭐️: 18
-   [2024](/solutions/2024) — ⭐️: 4
