---
"advent-of-code": minor
---

Solve for [2023, day 5](https://adventofcode.com/2023/day/5), part 1.
