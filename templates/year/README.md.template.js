export default ({ year }) => {
    return `# ${year} — ⭐️: 0

My solutions for the [${year} edition of Advent of Code](https://adventofcode.com/${year}/)

## Solutions`;
};
