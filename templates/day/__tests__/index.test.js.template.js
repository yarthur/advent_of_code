export default ({ year, day }) => {
    return `import { describe, it, expect } from "vitest";
import { part1, part2 } from "../index.js";
import { input } from "./testInput.js";

describe("${year}, day ${parseInt(day, 10)}", () => {
    it("solves for part 1", () => {
        expect(part1(input)).toBe(input);
    });

    it.todo("solves for part 2", () => {
        expect(part2(input)).toBe(input);
    });
});
`;
};
