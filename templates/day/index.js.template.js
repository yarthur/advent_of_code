export default ({ year, day, summary }) => {
    const dayInt = parseInt(day, 10);
    return `/**
 * My solutions for the
 * [${year} edition of Advent of Code, day ${dayInt}](https://adventofcode.com/${year}/day/${dayInt}/).
 * @namespace ${day}
 * @summary ${summary}
 * @memberof ${year}
 */

/**
 * @function part1
 * @summary Solves part 1 of the day's puzzle.
 * @memberof ${year}.${day}
 * @param { string } input The puzzle's input string.
 * @returns { number }
 */
export const part1 = (input) => {
    return input;
};

/**
 * @function part2
 * @summary Solves part 2 of the day's puzzle.
 * @memberof ${year}.${day}
 * @param { string } input The puzzle's input string.
 * @returns { number }
 */
export const part2 = (input) => {
    return input === "";
};`;
};
