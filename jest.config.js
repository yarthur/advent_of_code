/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

import { defaults } from "jest-config";

export default {
    collectCoverage: true,
    coverageDirectory: "coverage",
    coveragePathIgnorePatterns: [
        ...defaults.coveragePathIgnorePatterns,
        "__tests__",
    ],
    coverageReporters: ["cobertura", "html", "text", "text-summary"],
    testEnvironment: "node",
    testPathIgnorePatterns: [
        ...defaults.testPathIgnorePatterns,
        "sampleData.mjs",
        "testInput.js",
    ],
    testRegex: "(__tests__/.*(test|spec))\\.m?jsx?$",
    transform: {},
};
